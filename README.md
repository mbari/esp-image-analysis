# ESP Image Analysis Applications

## Documentation
1. [ESP Image Analysis](https://oceana.mbari.org/confluence/display/ESPAPP/ESP+Image+Analysis+Application)
2. [ESP Automated Image Analysis](https://oceana.mbari.org/confluence/display/ESPAPP/ESP+Image+Analysis+Automation)

## Developer Docs

### To Build
`mvn install -Dmaven.test.skip=true`

## To Generate Documentation
`mvn site`
