lazy val bioformatsVersion = "5.7.1"
lazy val configVersion = "1.3.2"
lazy val eventbusVersion = "1.4"
lazy val guavaVersion = "23.0"
lazy val guiceVersion = "4.1.0"
lazy val ijVersion = "1.51r"
lazy val imglibAlgoVersion = "0.8.1"
lazy val imglibGplVersion = "0.2.1"
lazy val imglibIjVersion = "2.0.0-beta-38"
lazy val imglibTransformVersion = "2.0.0-beta-38"
lazy val imglibVersion = "4.5.0"
lazy val jaiVersion = "1.1.3"
lazy val jamaVersion = "1.0.2"
lazy val jfreechartVersion = "1.0.13"
lazy val junitVersion = "4.12"
lazy val jxlayerVersion = "3.0.4.1"
lazy val logbackVersion = "1.2.3"
lazy val mbarix4jVersion = "1.10.0"
lazy val scalatestVersion = "3.0.4"
lazy val scifioVersion = "0.32.0"
lazy val slf4jVersion = "1.7.25"
lazy val swingLayoutVersion = "1.0.3"
lazy val swingxVersion = "1.6.5-1"

lazy val buildSettings = Seq(
  organization := "org.mbari.esp",
  scalaVersion := "2.12.4",
  crossScalaVersions := Seq("2.12.4"),
  licenses := Seq(("MIT", url("http://opensource.org/licenses/MIT")))
)

lazy val consoleSettings = Seq(
  shellPrompt := { state =>
    val user = System.getProperty("user.name")
    user + "@" + Project.extract(state).currentRef.project + ":sbt> "
  },
  initialCommands in console :=
    """
      |import java.time.Instant
      |import java.util.UUID
    """.stripMargin
)

lazy val dependencySettings = Seq(
  libraryDependencies ++= {
    Seq(
      "ch.qos.logback" % "logback-classic" % logbackVersion,
      "ch.qos.logback" % "logback-core" % logbackVersion,
      "com.typesafe" % "config" % configVersion,
      "junit" % "junit" % junitVersion % "test",
      "org.scalatest" %% "scalatest" % scalatestVersion % "test",
      "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
      "org.slf4j" % "slf4j-api" % slf4jVersion)
  },
  resolvers ++= Seq(
    Resolver.mavenLocal,
    Resolver.sonatypeRepo("releases"),
    "bioformats" at "http://artifacts.openmicroscopy.org/artifactory/maven/",
    "hohonuuli-bintray" at "http://dl.bintray.com/hohonuuli/maven",
    "imagej" at "http://maven.imagej.net/content/repositories/releases",
    "springsource" at "http://repository.springsource.com/maven/bundles/external",
    "unidata" at "https://artifacts.unidata.ucar.edu/content/repositories/unidata-releases"
  )
)

lazy val optionSettings = Seq(
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding",
    "UTF-8", // yes, this is 2 args
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-unchecked",
    "-Xlint",
    "-Yno-adapted-args",
    "-Ywarn-value-discard",
    "-Xfuture"),
  javacOptions ++= Seq("-target", "1.8", "-source", "1.8"),
  updateOptions := updateOptions.value.withCachedResolution(true)
)

// --- Aliases
addCommandAlias("cleanall", ";clean;clean-files")

// --- Modules
lazy val appSettings = buildSettings ++ consoleSettings ++ dependencySettings ++
    optionSettings

lazy val `esp-image-analysis` = (project in file("."))
    .settings(appSettings)
    .settings(
        name := "esp-image-analysis",
        version := "1.3.3-SNAPSHOT",
        fork := true,
        libraryDependencies ++= Seq(
            "ch.qos.logback" % "logback-classic" % logbackVersion,
            "com.google.guava" % "guava" % guavaVersion,
            "com.google.inject" % "guice" % guiceVersion,
            "io.scif" % "scifio" % scifioVersion,
            "jama" % "jama" % jamaVersion,
            "javax.media.jai" % "com.springsource.javax.media.jai.codec" % jaiVersion,
            "javax.media.jai" % "com.springsource.javax.media.jai.core" % jaiVersion,
            "jfree" % "jfreechart" % jfreechartVersion,
            "junit" % "junit" % junitVersion % "test",
            // "loci" % "bioformats" % bioformatsVersion,
            // "loci" % "ome_plugins" % bioformatsVersion,
            // "loci" % "ome-io" % bioformatsVersion,
            "net.imagej" % "ij" % ijVersion,
            "net.imglib2" % "imglib2-algorithm-gpl" % imglibGplVersion,
            "net.imglib2" % "imglib2-algorithm" % imglibAlgoVersion,
            "net.imglib2" % "imglib2-ij" % imglibIjVersion,
            "net.imglib2" % "imglib2-realtransform" % imglibIjVersion,
            "net.imglib2" % "imglib2" % imglibVersion,
            "net.java.dev.swing-layout" % "swing-layout" % swingLayoutVersion,
            "ome" % "bio-formats-tools" % bioformatsVersion,
            "ome" % "formats-gpl" % bioformatsVersion,
            "org.bushe" % "eventbus" % eventbusVersion,
            "org.mbari" % "mbarix4j" % mbarix4jVersion,
            "org.scalatest" %% "scalatest" % scalatestVersion % "test",
            "org.slf4j" % "slf4j-api" % slf4jVersion,
            "org.swinglabs.swingx" % "swingx-all" % swingxVersion,
            "org.swinglabs" % "jxlayer" % jxlayerVersion
        ),
      publishMavenStyle := true
    )
.settings(
  mainClass in assembly := Some("org.mbari.esp.imaging.ui.ImageMeasurementApp"),
  assemblyMergeStrategy in assembly := {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case _ => MergeStrategy.first
  },
  assemblyJarName := name.value + "-app-" + version.value
)
