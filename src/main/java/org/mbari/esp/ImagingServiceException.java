package org.mbari.esp;

/**
 *
 * @author brian
 */
public class ImagingServiceException extends RuntimeException {

    public ImagingServiceException(Throwable cause) {
        super(cause);
    }

    public ImagingServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImagingServiceException(String message) {
        super(message);
    }

    public ImagingServiceException() {
    }

}
