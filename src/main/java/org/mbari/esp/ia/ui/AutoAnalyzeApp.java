/*
 * @(#)AutoAnalyzeApp.java   2012.05.25 at 09:19:28 PDT
 *
 * Copyright 2009 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.esp.ia.ui;

import com.google.inject.Injector;
import org.mbari.esp.ia.ga.impl3.AutoAnalyzeFn$;
import org.mbari.esp.ia.ga.impl3.AutoAnalyzeParameters;
import org.mbari.esp.ia.services.ToolBox$;
import org.mbari.swing.LabeledSpinningDialWaitIndicator;
import org.mbari.util.SystemUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

/**
 * The AutoAnalysis application UI.
 *
 * @author Brian Schlining
 * @since 2011-04-11
 */
public class AutoAnalyzeApp {

    private static final Logger log = LoggerFactory.getLogger(AutoAnalyzeApp.class);
    private AutoAnalyzePanel autoAnalyzePanel;
    private JPanel buttonPanel;
    private JFrame frame;
    private JButton goButton;
    private final Injector injector;

    /**
     * Constructs ... Initialize Guice injector and start UI on EDT
     */
    public AutoAnalyzeApp() {
        log.debug("Starting ESP image analysis application using " + getClass().getName());

        // Use the injector in the Scala ToolBox. Appologies for the name mangling.
        injector = ToolBox$.MODULE$.injector();

        initialize();
    }

    /**
     * @return The UI that allows the user to set analysis parameters
     */
    public AutoAnalyzePanel getAutoAnalyzePanel() {
        if (autoAnalyzePanel == null) {
            autoAnalyzePanel = injector.getInstance(AutoAnalyzePanel.class);
            autoAnalyzePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        }

        return autoAnalyzePanel;
    }

    /**
     * @return The button panel on the bottom of the UI.
     */
    public JPanel getButtonPanel() {
        if (buttonPanel == null) {
            buttonPanel = new JPanel();
            buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
            buttonPanel.add(Box.createHorizontalGlue());
            buttonPanel.add(getGoButton());
            buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 10));
        }

        return buttonPanel;
    }

    /**
     * @return This is the application frame
     */
    public JFrame getFrame() {
        if (frame == null) {
            frame = new JFrame("ESP - Automated Image Analyzer");
            frame.setLayout(new BorderLayout());
            frame.add(getAutoAnalyzePanel(), BorderLayout.CENTER);
            frame.add(getButtonPanel(), BorderLayout.SOUTH);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
        }

        return frame;
    }

    /**
     * @return The button that starts an analysis run
     */
    public JButton getGoButton() {
        if (goButton == null) {
            goButton = new JButton("Analyze");
            goButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    final List<AutoAnalyzeParameters> params = getAutoAnalyzePanel().getAutoAnalyzeParameters();
                    log.debug("Processing " + params.size() + " images");
                    final LabeledSpinningDialWaitIndicator waitIndicator = new LabeledSpinningDialWaitIndicator(
                        getFrame());

                    SwingWorker<Void, Object> swingWorker = new SwingWorker<Void, Object>() {

                        @Override
                        protected Void doInBackground() throws Exception {
                            for (AutoAnalyzeParameters p : params) {
                                String name = p.imageFile().getName();
                                waitIndicator.setLabel("Processing " + name);
                                try {
                                    // Call a Scala Function
                                    AutoAnalyzeFn$.MODULE$.apply(p);
                                }
                                catch (Exception e) {
                                    log.warn("Unable to process " + p.imageFile().getAbsolutePath());
                                    File target = new File(p.targetDirectory(), name + "-README.txt");
                                    Writer out = new BufferedWriter(new FileWriter(target));
                                    out.write("Failed to process the file " + name +
                                              ". The following exception was thrown:\n\n");
                                    e.printStackTrace(new PrintWriter(out));
                                    out.close();
                                }
                            }

                            return null;
                        }

                        @Override
                        protected void done() {
                            waitIndicator.dispose();
                        }
                    };
                    swingWorker.execute();

                }

            });

        }

        return goButton;
    }

    private void initialize() {
        getFrame().setVisible(true);
    }

    /**
     * Main entry, The arguments are not used.
     * @param args Not used
     */
    public static void main(String[] args) {

        /*
         * Make it pretty on Macs
         */
        if (SystemUtilities.isMacOS()) {
            SystemUtilities.configureMacOSApplication("ESP Auto-image Analysis", "/images/esp_2G_dwg_sm.png");

        }

        if (log.isInfoEnabled()) {
            final Date date = new Date();
            log.info("This application was launched at " + date.toString());
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (final Exception e) {
            log.info("Unable to set look and feel", e);
        }

        try {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    AutoAnalyzeApp app = new AutoAnalyzeApp();
                    ImageIcon icon = new ImageIcon(app.getClass().getResource("/images/esp_2G_dwg_sm.png"));
                    app.getFrame().setIconImage(icon.getImage());
                }

            });

        }
        catch (final Throwable e) {
            log.error("Error occured on startup", e);
        }
    }
}
