/*
 * @(#)AutoAnalyzePanel.java   2011.04.13 at 10:58:00 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.inject.Inject;
import org.mbari.esp.ia.ga.impl3.AutoAnalyzeParameters;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.services.GeneArrayIOService;
import org.mbari.esp.ia.services.ImageIOService;
import scala.collection.JavaConversions$;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  UI panel for setting the {@link AutoAnalyzeParameters}
 *
 * @version        Enter version here..., 2011.04.13 at 10:58:00 PDT
 * @author         Brian Schlining [brian@mbari.org]    
 */
public class AutoAnalyzePanel extends JPanel {

    private int VERTICAL_SPACE = 10;
    private BlockSizePanel blockSizePanel;
    private FiducialKeyPanel fiducialKeyPanel;
    private FileSelectionPanel galFilePanel;
    private final GeneArrayIOService geneArrayIOService;
    private FilesSelectionPanel imageSelectionPanel;
    private SpotRadiusPanel spotRadiusPanel;
    private FileSelectionPanel targetDirPanel;
    private final AutoAnalyzePanelController controller;

    /**
     * Create the panel.
     *
     * @param geneArrayIOService
     */
    @Inject
    public AutoAnalyzePanel(GeneArrayIOService geneArrayIOService,
                            ImageIOService imageIOService) {
        this.geneArrayIOService = geneArrayIOService;
        this.controller = new AutoAnalyzePanelController(this, imageIOService);
        initialize();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
            }

        }));
    }

    /**
     * @return
     */
    public int getBlockSize() {
        return getBlockSizePanel().getBlockSize();
    }

    private BlockSizePanel getBlockSizePanel() {
        if (blockSizePanel == null) {
            blockSizePanel = new BlockSizePanel();
            blockSizePanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        }

        return blockSizePanel;
    }

    /**
     *
     * @return The string ID of the fiducial key to use select fiducial spots
     */
    public String getFiducialKey() {
        return getFiducialKeyPanel().getSelectedFiducialKey();
    }

    private FiducialKeyPanel getFiducialKeyPanel() {
        if (fiducialKeyPanel == null) {
            fiducialKeyPanel = new FiducialKeyPanel();
            fiducialKeyPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
                    "Select Fiducial Key", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        }

        return fiducialKeyPanel;
    }

    /**
     * @return
     */
    public File getGalFile() {
        return new File(getGalFilePanel().getTextField().getText());
    }

    private FileSelectionPanel getGalFilePanel() {
        if (galFilePanel == null) {
            galFilePanel = new FileSelectionPanel(getClass().getSimpleName() + "-gal-selection");
            galFilePanel.setBorder(new TitledBorder(null, "Select Gene Array Layout File", TitledBorder.LEADING,
                    TitledBorder.TOP, null, null));
            JTextField textField = galFilePanel.getTextField();
            textField.getDocument().addDocumentListener(new DocumentListener() {

                private final Function<GALPoint, String> toSrcWellFn = new ToSourceWellFn();
                private final Set<String> srcWells = new HashSet<String>();

                @Override
                public void insertUpdate(DocumentEvent e) {
                    doUpdate();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    doUpdate();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    doUpdate();
                }

                private void doUpdate() {
                    srcWells.clear();
                    try {
                        File galFile = getGalFile();
                        List<GALPoint> galPoints = JavaConversions$.MODULE$.seqAsJavaList(geneArrayIOService.read(galFile.toURI().toURL()));
                        srcWells.addAll(Collections2.transform(galPoints, toSrcWellFn));
                    } catch (Exception e) {
                        // Do Nothing
                    }

                    if (srcWells.size() > 0) {
                        List<String> fiducialKeys = new ArrayList<String>(srcWells);
                        Collections.sort(fiducialKeys);
                        getFiducialKeyPanel().setFiducialKeys(fiducialKeys);
                    }
                    getFiducialKeyPanel().repaint();

                }
            });
        }

        return galFilePanel;
    }

    /**
     * @return
     */
    public List<File> getImageFiles() {
        DefaultListModel model = (DefaultListModel) getImageSelectionPanel().getList().getModel();
        List<File> imageFiles = new ArrayList<File>();
        for (int i = 0; i < model.getSize(); i++) {
            imageFiles.add((File) model.get(i));
        }

        return imageFiles;
    }

    /**
     * Once all the fields in the UI are populated call this method to get the
     * parameters needed to run the GA.
     *
     * @return A parameter object for each file listed in the image files JList.
     */
    public List<AutoAnalyzeParameters> getAutoAnalyzeParameters() {
        return new ArrayList<AutoAnalyzeParameters>(Collections2.transform(getImageFiles(), new ToParamsFn()));
    }

    private FilesSelectionPanel getImageSelectionPanel() {
        if (imageSelectionPanel == null) {
            imageSelectionPanel = new FilesSelectionPanel(new ImageSelector(this));
            imageSelectionPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null),
                    "Select ESP Images", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
        }

        return imageSelectionPanel;
    }

    /**
     *
     * @return The size of the spot to use in the analysis. -1 is returned if the software
     *  should auto-calculate spot size based on image properties.
     */
    public int getSpotRadius() {
        return getSpotRadiusPanel().getSpotRadius();
    }

    private SpotRadiusPanel getSpotRadiusPanel() {
        if (spotRadiusPanel == null) {
            spotRadiusPanel = new SpotRadiusPanel();
            spotRadiusPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        }

        return spotRadiusPanel;
    }

    private FileSelectionPanel getTargetDirPanel() {
        if (targetDirPanel == null) {
            targetDirPanel = new FileSelectionPanel(getClass().getSimpleName() + "-target-directory");
            targetDirPanel.getFileChooser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            targetDirPanel.setBorder(new TitledBorder(null, "Select Directory to Save Products to",
                    TitledBorder.LEADING, TitledBorder.TOP, null, null));
        }

        return targetDirPanel;
    }

    /**
     * @return
     */
    public File getTargetDirectory() {
        return getTargetDirPanel().getFile();
    }

    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(getGalFilePanel());
        add(Box.createVerticalStrut(VERTICAL_SPACE));
        add(getImageSelectionPanel());
        add(Box.createVerticalStrut(VERTICAL_SPACE * 2));
        add(getSpotRadiusPanel());

        //add(Box.createVerticalStrut(VERTICAL_SPACE));
        add(getBlockSizePanel());
        add(Box.createVerticalStrut(VERTICAL_SPACE * 2));
        add(getFiducialKeyPanel());
        add(Box.createVerticalStrut(VERTICAL_SPACE));
        add(getTargetDirPanel());
        add(Box.createVerticalStrut(VERTICAL_SPACE * 2));
    }

    /**
     * Converter function: (File) => AutoAnalyzeParameters
     */
    private class ToParamsFn implements Function<File, AutoAnalyzeParameters> {
        @Override
        public AutoAnalyzeParameters apply(File input) {
            return controller.getAutoAnalyzeParameters(input);
        }
    }

}
