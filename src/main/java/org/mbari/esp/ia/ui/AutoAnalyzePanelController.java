/*
 * @(#)AutoAnalyzePanelController.java   2011.04.13 at 03:31:24 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.mbari.esp.ia.awt.ShortRegion;
import org.mbari.esp.ia.ga.impl3.AutoAnalyzeParameters;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.geometry.LabeledDoublePoint2D;
import org.mbari.esp.ia.geometry.Points$;
import org.mbari.esp.ia.imglib.FloodFillFn$;
import org.mbari.esp.ia.imglib.SpotRegionsFn$;
import org.mbari.esp.ia.imglib.To8BitGrayFn$;
import org.mbari.esp.ia.services.GeneArrayAnalysisService$;
import org.mbari.esp.ia.services.GeneArrayIOService;
import org.mbari.esp.ia.services.ImageIOService;
import org.mbari.math.DoubleMath;
import scala.collection.*;
import scala.collection.Iterable;
import scala.collection.mutable.ArrayBuffer;


/**
 * This controller provides the hooks into Scala services. Lots of funny name mangling in the
 * source here ... sorry. That's the price of calling scala for java.
 *
 * @author Brian Schlining
 * @since 2011-04-13
 */
public class AutoAnalyzePanelController {

    private final AutoAnalyzePanel autoAnalyzePanel;
    private final ImageIOService imageIOService;

    public AutoAnalyzePanelController(AutoAnalyzePanel autoAnalyzePanel, ImageIOService imageIOService) {
        this.autoAnalyzePanel = autoAnalyzePanel;
        this.imageIOService = imageIOService;
    }

    /**
     *
     * @param imageFile
     * @return
     */
    public AutoAnalyzeParameters getAutoAnalyzeParameters(File imageFile) {

        // --- Determine Spot Radius
        int spotRadius = autoAnalyzePanel.getSpotRadius();
        if (spotRadius < 1) {
            spotRadius = estimateSpotRadius(imageFile);
        }

        return new AutoAnalyzeParameters(autoAnalyzePanel.getGalFile(),
                autoAnalyzePanel.getFiducialKey(),
                imageFile,
                autoAnalyzePanel.getBlockSize(),
                spotRadius,
                autoAnalyzePanel.getTargetDirectory());

    }

    public int estimateSpotRadius(File imageFile) {
        // -- Read image, extract point features
        ImagePlus imagePlus = null;
        try {
            imagePlus = imageIOService.read(imageFile.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to read " + imageFile, e);
        }

        // Calling Scala objects ... sorry about name mangling
        ImageProcessor floodedProcessor = FloodFillFn$.MODULE$.apply(To8BitGrayFn$.MODULE$.apply(imagePlus.getProcessor()));
        Iterable<ShortRegion> regions = SpotRegionsFn$.MODULE$.apply(imagePlus.getProcessor(), floodedProcessor);
        ArrayBuffer<LabeledDoublePoint2D> spots = new ArrayBuffer<LabeledDoublePoint2D>();
        Iterator<ShortRegion> iterator = regions.iterator();
        while (iterator.hasNext()) {
            ShortRegion shortRegion = iterator.next();
            spots.$plus$eq(shortRegion.centroid());
        }
        return (int) Math.floor(Points$.MODULE$.bestDistance(spots, 1) / 2D);

    }

}
