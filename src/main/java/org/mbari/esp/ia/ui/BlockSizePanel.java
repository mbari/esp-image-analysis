/*
 * @(#)BlockSizePanel.java   2011.04.11 at 11:40:11 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import java.util.prefs.Preferences;

/**
 *
 *
 * @version        Enter version here..., 2011.04.11 at 11:40:11 PDT
 * @author         Brian Schlining [brian@mbari.org]    
 */
public class BlockSizePanel extends JPanel {

    private final String BLOCK_SIZE_PREFERENCE = "block-size";
    private final Preferences preferences = Preferences.userNodeForPackage(getClass());
    private JLabel label;
    private JSpinner spinner;

    /**
     * Create the panel.
     */
    public BlockSizePanel() {
        int blockSize = preferences.getInt(BLOCK_SIZE_PREFERENCE, 5);
        getSpinner().setValue(blockSize);
        initialize();

        /**
         * Save preference settings on shutdown.
         */
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                preferences.putInt(BLOCK_SIZE_PREFERENCE, (Integer) getSpinner().getValue());
            }

        }));
    }

    /**
     * @return
     */
    public int getBlockSize() {
        return (Integer) getSpinner().getValue();
    }

    private JLabel getLabel() {
        if (label == null) {
            label = new JLabel("Use n x n block to extract spot intensity, where n = ");
        }

        return label;
    }

    private JSpinner getSpinner() {
        if (spinner == null) {
            spinner = new JSpinner();
        }

        return spinner;
    }

    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(getLabel());
        add(getSpinner());
    }
}
