/*
 * @(#)FiducialKeyPanel.java   2011.04.11 at 10:52:05 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 *
 * @version        Enter version here..., 2011.04.11 at 10:52:05 PDT
 * @author         Brian Schlining [brian@mbari.org]    
 */
public class FiducialKeyPanel extends JPanel {

    private JComboBox comboBox;
    private JRadioButton manualSelectRB;
    private JButton selectButton;
    private JRadioButton autoSelectRB;

    /**
     * Create the panel.
     */
    public FiducialKeyPanel() {
        initialize();
    }

    public void setFiducialKeys(List<String> fiducialKeys) {
        List<String> keys = new ArrayList<String>(fiducialKeys);
        Collections.sort(keys);
        DefaultComboBoxModel model = (DefaultComboBoxModel) getComboBox().getModel();
        model.removeAllElements();
        for (String key  : keys) {
            model.addElement(key);
        }
        if (keys.size() > 0) {
            model.setSelectedItem(keys.get(0));
        }
    }

    public String getSelectedFiducialKey() {
        return (String) getComboBox().getSelectedItem();
    }

    /**
     * A combobox model that contains strings using a {@link DefaultComboBoxModel}
     * @return
     */
    private JComboBox getComboBox() {
        if (comboBox == null) {
            comboBox = new JComboBox();
            comboBox.setModel(new DefaultComboBoxModel());
        }

        return comboBox;
    }

    private JRadioButton getManualSelectRB() {
        if (manualSelectRB == null) {
            manualSelectRB = new JRadioButton("Manually Select Fiducial Spots");
            manualSelectRB.setEnabled(false); // TODO disabled until implemented
        }

        return manualSelectRB;
    }

    private JButton getSelectButton() {
        if (selectButton == null) {
            selectButton = new JButton("Select");
            selectButton.setEnabled(false); // TODO disabled until implemented
        }

        return selectButton;
    }

    private JRadioButton getAutoSelectRB() {
        if (autoSelectRB == null) {
            autoSelectRB = new JRadioButton("Source Well for Fiducial Spots");
            autoSelectRB.setSelected(true);
            autoSelectRB.setEnabled(false); // TODO disabled until implemented
        }

        return autoSelectRB;
    }

    private void initialize() {
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(getAutoSelectRB())
                                        .addComponent(getManualSelectRB()))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(getComboBox(), 0, 211, Short.MAX_VALUE)
                                        .addComponent(getSelectButton()))
                                .addContainerGap())
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.LEADING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(getAutoSelectRB())
                                        .addComponent(getComboBox(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        .addComponent(getManualSelectRB())
                                        .addComponent(getSelectButton()))
                                .addContainerGap(10, Short.MAX_VALUE))
        );
        setLayout(groupLayout);
    }
}
// 211 232