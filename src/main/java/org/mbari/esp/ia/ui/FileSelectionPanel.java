/*
 * @(#)FileSelectionPanel.java   2011.04.12 at 09:34:10 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

/**
 *
 *
 * @version        Enter version here..., 2011.04.12 at 09:34:10 PDT
 * @author         Brian Schlining [brian@mbari.org]    
 */
public class FileSelectionPanel extends JPanel {

    private final String ID_PREFERENCES = "id";
    private JFileChooser fileChooser;
    private final Preferences preferences = Preferences.userNodeForPackage(getClass());
    private JButton button;
    private final String id;
    private JScrollPane scrollPane;
    private JTextField textField;

    /**
     * Constructs ...
     */
    public FileSelectionPanel() {
        this(null);
    }

    /**
     * Create the panel.
     *
     * @param id
     */
    public FileSelectionPanel(final String id) {
        this.id = id;
        initialize();
        if ((id != null) && (id.length() > 0)) {

            // Save current directory on shutdown
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        String path = getFileChooser().getCurrentDirectory().getCanonicalPath();
                        if ((path != null) && (path.length() > 0)) {
                            preferences.put(ID_PREFERENCES + ": " + id, path);
                        }
                    }
                    catch (Exception e) {
                        // Do nothing
                    }
                }
            }));
        }
    }

    /**
     * @return
     */
    public JButton getButton() {
        if (button == null) {
            button = new JButton("Select");
            button.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int v = getFileChooser().showOpenDialog(FileSelectionPanel.this);
                    if (v == JFileChooser.APPROVE_OPTION) {
                        File file = getFileChooser().getSelectedFile();
                        try {
                            getTextField().setText(file.getCanonicalPath());
                        } catch (IOException ex) {

                            // Do nothing
                        }
                    }
                }

            });
        }

        return button;
    }

    /**
     * @return
     */
    public File getFile() {
        return new File(getTextField().getText());
    }

    /**
     * @return
     */
    public JFileChooser getFileChooser() {
        if (fileChooser == null) {
            fileChooser = new JFileChooser();
            // Load current directory from preferences
            if ((id != null) && (id.length() > 0)) {
                String path = preferences.get(ID_PREFERENCES + ": " + id, System.getProperty("user.home"));
                fileChooser.setCurrentDirectory(new File(path));
            }
        }

        return fileChooser;
    }

    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
            scrollPane.setViewportView(getTextField());
        }

        return scrollPane;
    }

    /**
     * @return
     */
    public JTextField getTextField() {
        if (textField == null) {
            textField = new JTextField();
            textField.setColumns(40);
        }

        return textField;
    }

    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(getScrollPane());
        add(getButton());
    }
}
