/*
 * @(#)FilesSelectionController.java   2011.04.11 at 10:08:13 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import java.io.File;
import java.util.List;

/**
 * @author Brian Schlining
 * @since 2011-04-11
 */
public interface FileSelector {

    /**
     *
     * @return A list containing zero to many files
     */
    List<File> addManyFiles();



}
