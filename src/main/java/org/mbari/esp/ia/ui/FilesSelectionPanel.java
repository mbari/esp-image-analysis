/*
 * @(#)ImagePanel.java   2011.04.07 at 01:59:30 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;

/**
 *
 *
 * @version        Enter version here..., 2011.04.07 at 01:59:30 PDT
 * @author         Brian Schlining [brian@mbari.org]
 */
public class FilesSelectionPanel extends JPanel {

    private JButton addManyButton;
    private JList list;
    private JPanel panel;
    private JButton removeButton;
    private JScrollPane scrollPane;
    private final FileSelector controller;

    /**
     * Create the panel.
     */
    public FilesSelectionPanel(FileSelector controller) {
        this.controller = controller;
        initialize();
    }



    private JButton getAddManyButton() {
        if (addManyButton == null) {
            addManyButton = new JButton("+");
            addManyButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    List<File> files = controller.addManyFiles();
                    DefaultListModel model = (DefaultListModel) getList().getModel();
                    for (File f  : files) {
                        model.addElement(f);
                    }
                }
            });
        }

        return addManyButton;
    }

    /**
     * @return A collection of files listed in this panel
     */
    public Collection<File> getSelectedFiles() {
        Collection<File> imageFiles = new ArrayList<File>();
        ListModel model = getList().getModel();
        int n = model.getSize();
        for (int i = 0; i < n; i++) {
            imageFiles.add((File) model.getElementAt(i));
        }

        return imageFiles;
    }

    /**
     * Stores paths as Files. Uses a {@link DefaultListModel} as the
     * underlying model.
     *
     * @return
     */
    public JList getList() {
        if (list == null) {
            list = new JList();
            list.setModel(new DefaultListModel());
        }

        return list;
    }

    private JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(getAddManyButton());
            panel.add(getRemoveButton());
        }

        return panel;
    }

    private JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton = new JButton("-");
            removeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Object[] selectedFiles = getList().getSelectedValues();
                    DefaultListModel model = (DefaultListModel) getList().getModel();
                    for (Object f: selectedFiles) {
                        model.removeElement(f);
                    }
                }
            });
        }

        return removeButton;
    }

    private JScrollPane getScrollPane() {
        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            scrollPane.setViewportView(getList());
        }

        return scrollPane;
    }

    private void initialize() {
        setBorder(new TitledBorder(null, "Images", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        setLayout(new BorderLayout(0, 0));
        add(getScrollPane(), BorderLayout.CENTER);
        add(getPanel(), BorderLayout.EAST);
    }
}
