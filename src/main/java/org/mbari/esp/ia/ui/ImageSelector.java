/*
 * @(#)ImageSelector.java   2011.04.11 at 10:42:50 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Controller that allows the UI to select images
 *
 * @author Brian Schlining
 * @since 2011-04-11
 */
public class ImageSelector implements FileSelector {

    private final String IMAGE_DIR_PREFERENCE = "image-directory";
    private final Preferences preferences = Preferences.userNodeForPackage(ImageSelector.class);
    private Logger log = LoggerFactory.getLogger(getClass());
    private JFileChooser fileChooser;
    private final Component parentComponent;

    /**
     * Constructs ...
     *
     * @param parentComponent Used as the parent of dialogs that are created by this controller
     */
    public ImageSelector(Component parentComponent) {
        this.parentComponent = parentComponent;
    }

    /**
     * @return
     */
    @Override
    public List<File> addManyFiles() {
        return selectFiles(true);
    }


    private JFileChooser getFileChooser() {
        if (fileChooser == null) {
            String openDirectory = preferences.get(IMAGE_DIR_PREFERENCE, System.getProperty("user.home"));
            fileChooser = new JFileChooser(openDirectory);
            fileChooser.setFileFilter(new ImageFilter());
        }

        return fileChooser;
    }

    private List<File> selectFiles(boolean selectMany) {
        List<File> manyFiles = new ArrayList<File>();
        JFileChooser chooser = getFileChooser();
        chooser.setMultiSelectionEnabled(selectMany);
        int status = chooser.showOpenDialog(parentComponent);
        if (status == JFileChooser.APPROVE_OPTION) {
            File[] allFiles = chooser.getSelectedFiles();
            manyFiles.addAll(Arrays.asList(allFiles));
            if (manyFiles.size() > 0) {
                try {
                    File f = manyFiles.get(0);
                    String parent = f.getParentFile().getCanonicalPath();
                    preferences.put(IMAGE_DIR_PREFERENCE, parent);
                }
                catch (Exception e) {
                    log.debug("Failed to save selected image directory to preferences", e);
                }
            }
        }

        return manyFiles;
    }

    private class ImageFilter extends FileFilter {

        /**
         *
         * @param pathname
         * @return
         */
        @Override
        public boolean accept(File pathname) {
            String p = pathname.getAbsolutePath();

            return p.endsWith(".png") || p.endsWith(".jpg") || p.endsWith(".gif") || p.endsWith(".tif") ||
                   p.endsWith(".tiff");
        }

        /**
         * @return
         */
        @Override
        public String getDescription() {
            return "Image Files";
        }
    }
}
