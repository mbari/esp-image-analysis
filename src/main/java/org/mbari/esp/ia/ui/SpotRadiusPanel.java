/*
 * @(#)SpotSizePanel.java   2011.04.11 at 11:28:49 PDT
 *
 * Copyright 2011 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.ia.ui;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.prefs.Preferences;

/**
 *
 *
 * @version        Enter version here..., 2011.04.11 at 11:28:49 PDT
 * @author         Brian Schlining [brian@mbari.org]    
 */
public class SpotRadiusPanel extends JPanel {

    private final String SPOT_SIZE_PREFERENCE = "spot-size";
    private final String CALCULATE_PREFERENCE = "calculate-spot-size";
    private final Preferences preferences = Preferences.userNodeForPackage(SpotRadiusPanel.class);
    private JCheckBox checkBox;
    private JLabel label;
    private JSpinner spinner;

    /**
     * Create the panel.
     */
    public SpotRadiusPanel() {
        int spotSize = preferences.getInt(SPOT_SIZE_PREFERENCE, 8);
        getSpinner().setValue(spotSize);
        boolean s = preferences.getBoolean(CALCULATE_PREFERENCE, true);
        getCheckBox().setSelected(s);
        initialize();
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                preferences.putInt(SPOT_SIZE_PREFERENCE, (Integer) getSpinner().getValue());
                preferences.putBoolean(CALCULATE_PREFERENCE, getCheckBox().isSelected());
            }
        }));
    }

    private JCheckBox getCheckBox() {
        if (checkBox == null) {
            checkBox = new JCheckBox("Calculate Spot Radius");
            checkBox.setToolTipText("If checked, the software will estimate the size of the " +
                    "spots.\nIf not checked, specify the spot size to use.\n\n");
            checkBox.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    getSpinner().setEnabled(!checkBox.isSelected());
                }
            });
        }

        return checkBox;
    }

    private JLabel getLabel() {
        if (label == null) {
            label = new JLabel(" ... or use this Spot Radius");
            label.setToolTipText(
                "If 'Calculate Spot Radius' is not checked. The software will use the spot size that you specify");
        }

        return label;
    }

    private JSpinner getSpinner() {
        if (spinner == null) {
            spinner = new JSpinner();
        }

        return spinner;
    }

    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(getCheckBox());
        add(Box.createHorizontalGlue());
        add(getLabel());
        add(getSpinner());
    }

    /**
     *
     * @return The spot size to use. -1 is returned if the 'Calculate Spot Size' checkbox is checked.
     *  In that case the software should estimate spot size
     */
    public int getSpotRadius() {
        int spotSize = -1;
        if (!getCheckBox().isSelected()) {
            spotSize = (Integer) getSpinner().getValue();
        }
        return spotSize;
    }
}
