package org.mbari.esp.ia.ui;

import com.google.common.base.Function;
import org.mbari.esp.ia.geometry.GALPoint;

/**
 *
 * @author Brian Schlining
 * @since 2012-05-25
 */

class ToSourceWellFn implements Function<GALPoint, String> {

    @Override
    public String apply(GALPoint input) {
        return input.sourceWell();
    }
}