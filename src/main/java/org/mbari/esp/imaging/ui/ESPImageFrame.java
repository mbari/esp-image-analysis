package org.mbari.esp.imaging.ui;


import java.awt.Dimension;
import java.awt.Insets;

import org.mbari.esp.ia.services.ImageIOService;
import org.mbari.swingworker.SwingWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.swing.JComponent;

/**
 * A class for displaying an image (gif, jpg or png) in it's own frame. Usage:
 * <pre>
 * ESPImageFrame f = new ESPImageFrame(imageIOService);
 * f.setVisible(true);
 * f.setUrl(new URL("file:/url/to/my/image.png"));
 * f.setUrl(new URL("http:/some/other/image.gif"));
 * </pre>
 *
 * This is essentially the same class as the {@link org.mbari.swing.JImageFrame} except that
 * is uses a custom {@link ImageIOService} since Java doens't handle TIFF's
 * 'out of the box' and most ESP images are TIFFs
 *
 * @author brian
 */
public class ESPImageFrame extends JFrame {

    private URL imageUrl;
    private BufferedImage image;
    private final JLabel label = new JLabel();
    private final JProgressBar progressBar = new JProgressBar() {{ setIndeterminate(true); setPreferredSize(new Dimension(200, 20));}};
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final ImageIOService imageIOService;
    public static final String PROP_IMAGE = "imageUrl";

    /**
     * Create the frame
     */
    @Inject
    public ESPImageFrame(ImageIOService imageIOService) {
        super();
        this.imageIOService = imageIOService;
        initialize();
    }

    /**
     * Sets the URL of the image to display. The images is fetched from the
     * location specified. <b>IMPORTANT!! This method avoids the cacheing that
     * swing and/or {@code Toolkit} normally uses. So your image will not be
     * cached in memory</b>
     *
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(URL imageUrl) {
        this.imageUrl = imageUrl;
        log.debug("setImageUrl( " + imageUrl + " )");

        if (imageUrl != null) {

            /*
             * Use swingutilities to invoke changes on the EventDisplatch thread.
             *
             * Remove label from view and add progress bar
             */
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    setResizable(true);
                    setTitle(getImageUrl().toExternalForm());
                    remove(label);
                    add(progressBar);
                    layoutFrame(progressBar);
                    (new ImageLoader(getImageUrl())).execute();
                }
            });

        }
        else {
            /*
             *  Clear out data if no value is set
             */
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    setResizable(true);
                    setTitle("");
                    image = null;
                    label.setText("No image available");
                    label.setIcon(null);
                    add(label);
                    layoutFrame(label);
                    setResizable(false);
                }
            });
        }
    }


    /**
     * We're managing the layout ourselves since we're using abolute layout.
     * We're doing this because calls to <i>pack</i> is causing clipping
     * problems when the image is larger than the computers screen
     *
     * @param component The component displayed in the frame. We're only using
     * 1 at a time. If you try to add other componenets you'll mess everything up.
     */
    private void layoutFrame(JComponent component) {
        Dimension dimension = component.getPreferredSize();
        Insets insets = getInsets();
        component.setBounds(insets.left, 0, dimension.width, dimension.height);
        setSize(dimension.width + insets.left + insets.right, dimension.height + insets.bottom);
    }

    /**
     * The image is stored internally as a {@code BufferedImage}. This
     * call returns the underlying image
     * @return The image fetched in
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * @return the imageUrl
     */
    public URL getImageUrl() {
        return imageUrl;
    }

    private void initialize() {
        setLayout(null);
        setImageUrl(null);
    }

    /**
     * @return
     */
    private class ImageLoader extends SwingWorker<BufferedImage, Object> {

        final URL url;
        final BufferedImage oldImage;

        public ImageLoader(final URL url) {
            this.url = url;
            this.oldImage = getImage();
        }

        protected BufferedImage doInBackground() throws Exception {
            log.debug("Reading image from " + url);
            return imageIOService.readAsBufferedImage(url);
        }

        @Override
        protected void done() {

            getContentPane().remove(progressBar);

            try {
                image = get();
                log.debug("Image " + url + " [" + image.getWidth() + " x " + image.getHeight() + " pixels] has been loaded");
                label.setText(null);
                label.setIcon(new ImageIcon(image));
            }
            catch (Exception e) {
                log.debug("Failed to read image", e);
                label.setText("Failed to fetch image from " + url.toExternalForm());
                label.setIcon(null);
            }

            getContentPane().add(label);
            layoutFrame(label);
            setResizable(false);
            ESPImageFrame.this.firePropertyChange(PROP_IMAGE, oldImage, image);
        }
    }
}
