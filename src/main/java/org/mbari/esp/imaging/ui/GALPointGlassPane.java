package org.mbari.esp.imaging.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;



import org.mbari.esp.ia.geometry.DoublePoint2D;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.geometry.Points;
import org.mbari.geometry.Point2D;
import org.mbari.geometry.PointUtilities;
import org.mbari.math.DoubleMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author brian
 * @since Apr 21, 2009 11:31:50 AM
 */
public class GALPointGlassPane extends JComponent {

    private double rotation = 0;
    private double scale = 1;

    // adjust display scaling for resizable images
    private double displayScale = 1;
    private Point2D<Integer> translatedCentroid;
    private final List<GALPoint> transformedPoints = new ArrayList<GALPoint>();
    private int diameter;
    private List<GALPoint> points;
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final MouseTranslationListener mouseListener = new MouseTranslationListener();
    public static final String PROP_DIAMETER = "diameter";
    public static final String PROP_POINTS = "points";
    public static final String PROP_ROTATION = "rotation";
    public static final String PROP_SCALE = "scale";
    public static final String PROP_TRANSLATION = "translation";
    private static final String ACTION_KEY_MOVE_X_LEFT = "-X";
    private static final String ACTION_KEY_MOVE_X_RIGHT = "+X";
    private static final String ACTION_KEY_MOVE_Y_UP = "-Y";
    private static final String ACTION_KEY_MOVE_Y_DOWN = "+Y";


    public GALPointGlassPane() {
        addMouseMotionListener(mouseListener);
        addMouseListener(mouseListener);

        InputMap inputMap = getInputMap();
        ActionMap actionMap = getActionMap();

        /*
         * Listen for arrow keys to move the points around. We can't add
         * rotation and scale here because we don't know the increment that
         * will be used by external UI componenets. Those listeners will
         * have to be added by parent UI compoenent.
         */
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), ACTION_KEY_MOVE_Y_UP);
        actionMap.put(ACTION_KEY_MOVE_Y_UP, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                translate(translatedCentroid.getX(), translatedCentroid.getY() - 1);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), ACTION_KEY_MOVE_Y_DOWN);
        actionMap.put(ACTION_KEY_MOVE_Y_DOWN, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                translate(translatedCentroid.getX(), translatedCentroid.getY() + 1);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), ACTION_KEY_MOVE_X_LEFT);
        actionMap.put(ACTION_KEY_MOVE_X_LEFT, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                translate(translatedCentroid.getX() - 1, translatedCentroid.getY());
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), ACTION_KEY_MOVE_X_RIGHT);
        actionMap.put(ACTION_KEY_MOVE_X_RIGHT, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                translate(translatedCentroid.getX() + 1, translatedCentroid.getY());
            }
        });

    }


    public int getDiameter() {
        return diameter;
    }

    public List<GALPoint> getPoints() {
        return points;
    }

    public List<GALPoint> getTransformedPoints() {
        return transformedPoints;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setPaint(Color.RED);

        // Draw points over top of image
        if (transformedPoints.size() > 0) {
            for (GALPoint p : transformedPoints) {
                int x0 = (int) Math.round(p.x() - diameter / 2);
                int y0 = (int) Math.round(p.y() - diameter / 2);

                g2.drawOval(x0, y0, diameter, diameter);
            }
        }
    }

    /**
     * Set the rotation. Any value is acceptable but it will be adjusted to
     * be between 0 and 2 * PI
     */
    public void rotate(double angleRadians) {

        angleRadians = DoubleMath.normalizeRadianAngle(angleRadians);

        double oldAngleRadians = this.rotation;
        this.rotation = angleRadians;
        log.debug("Rotation = "  + rotation);
        transform();
        firePropertyChange(PROP_ROTATION, oldAngleRadians, angleRadians);
    }

    public void scale(double scale) {
        double oldScale = this.scale;
        this.scale = scale;
        log.debug("Scale = "  + scale);
        transform();
        firePropertyChange(PROP_SCALE, oldScale, scale);
    }

    /**
     * Set the diameter of the spot sixe
     * @param diameter
     */
    public void setDiameter(int diameter) {
        int oldDiameter = this.diameter;
        this.diameter = diameter;
        transform();
        firePropertyChange(PROP_DIAMETER, oldDiameter, diameter);
    }

    public void setPoints(List<GALPoint> points) {

        final List<GALPoint> oldPoints = this.points;

        this.points = new ArrayList<GALPoint>(points);
        transformedPoints.clear();

        // Translate points so that centroid is 0,0
        List<Point2D<Double>> newPoints = galToGeometryPoint(points);
        DoublePoint2D centroid = Points.centroid(points);
        List<Point2D<Double>> adjustedPoints = PointUtilities.translate(newPoints, centroid.x(), centroid.y());
        for (int i = 0; i < this.points.size(); i++) {
            Point2D<Double> tp = adjustedPoints.get(i);
            GALPoint src = this.points.get(i);
            transformedPoints.add(new GALPoint(src, tp.getX(), tp.getY()));
        }

        repaint();

        firePropertyChange(PROP_POINTS, oldPoints, points);
    }

    private void transform() {

        if (points == null || points.size() == 0) {
            repaint();
            return;
        }

        synchronized (transformedPoints) {
            transformedPoints.clear();

            List<Point2D<Double>> newPoints = galToGeometryPoint(points);

            newPoints = PointUtilities.rotate(newPoints, rotation, PointUtilities.centroid(newPoints));

            Point2D<Double> centroid = PointUtilities.centroid(newPoints);
            newPoints = PointUtilities.scale(newPoints, scale, centroid);
            newPoints = PointUtilities.translate(newPoints, translatedCentroid.getX(), translatedCentroid.getY());

            for (int i = 0; i < points.size(); i++) {
                Point2D<Double> tp = newPoints.get(i);
                GALPoint src = points.get(i);

                transformedPoints.add(new GALPoint(src, tp.getX(), tp.getY()));
            }

            repaint();
        }
    }

    private List<Point2D<Double>> galToGeometryPoint(List<GALPoint> ps) {
        List<Point2D<Double>> newPoints = new ArrayList<Point2D<Double>>();
        for (GALPoint p : points) {
            newPoints.add(new Point2D<Double>(p.x(), p.y()));
        }
        return newPoints;
    }

    public void translate(int tx, int ty) {
        final Point2D<Integer> oldCentroid = translatedCentroid;
        this.translatedCentroid = new Point2D<Integer>(tx, ty);
        transform();
        firePropertyChange(PROP_TRANSLATION, oldCentroid, translatedCentroid);
    }

    public double getRotation() {

        return rotation;
    }

    public double getScale() {
        return scale;
    }

    public Point2D<Integer> getTranslation() {
        return translatedCentroid;
    }

    /**
     * Does nothing yet
     * @return
     */
    public double getDisplayScale() {
        return displayScale;
    }

    /**
     * Setting the display scale does nothing. This method was added for develpoment
     * of ESPImageFrame2
     * @return
     */
    public void setDisplayScale(double displayScale) {
        this.displayScale = displayScale;
    }



    /**
     * Listen to dragging and allow the points to be dragged around.
     */
    class MouseTranslationListener implements MouseMotionListener, MouseListener {

        Point2D<Integer> startMousePoint;
        Point2D<Integer> originalCentroid;


        /**
         * Apply translation to the glasspane
         * @param e
         */
        public void mouseDragged(MouseEvent e) {
            int dx = originalCentroid.getX() + e.getX() - startMousePoint.getX();
            int dy = originalCentroid.getY() + e.getY() - startMousePoint.getY();
            translate(dx, dy);
        }

        public void mouseMoved(MouseEvent e) {
            // DO Nothing
        }


        public void mouseClicked(MouseEvent e) {
            // Do Nothing
        }

        /**
         * Store the starting point of the click
         * @param e
         */
        public void mousePressed(MouseEvent e) {
            startMousePoint = new Point2D<Integer>(e.getX(), e.getY());
            originalCentroid = translatedCentroid;
        }

        public void mouseReleased(MouseEvent e) {
            // Do Nothing
        }

        public void mouseEntered(MouseEvent e) {
            // Do Nothing
        }

        public void mouseExited(MouseEvent e) {
            // Do Nothing
        }

    }
}
