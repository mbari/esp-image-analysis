package org.mbari.esp.imaging.ui;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.*;

import org.mbari.esp.ia.awt.SourceWell;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.mbari.awt.image.ImageUtilities;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.services.GeneArrayIOService;

/**
 * A Frame for displayinga Map of {@link GALPoint}s. Allows for:
 * <ul>
 * <li>Display of Map</li>
 * <li>Editing colors and Labels of Spots</li>
 * <li>Selecting and exporting control points on the map</li>
 * <li>Saving the map image to a file</li>
 * </ul>
 */
public class GALPointViewer extends JFrame {

    private ButtonGroup buttonGroup = new ButtonGroup();
    private JLabel backgroundLabel;
    private JRadioButton blackRadioButton;
    private JButton cancelButton;
    private final GALPointViewerController controller;
    private JButton exportControlsButton;
    private JLabel imageLabel;
    private JButton okButton;
    private JPanel okCancelPanel;
    private final List<GALPoint> points;
    private final int scale;
    private final List<SourceWell> sourceWells;
    private JPanel srcWellEditorPanel;
    private JRadioButton whiteRadioButton;

    /**
     * Create the frame
     *
     * @param points
     * @param scale
     * @param geneArrayIOService
     */
    public GALPointViewer(List<GALPoint> points, int scale, GeneArrayIOService geneArrayIOService) {
        super();
        this.controller = new GALPointViewerController(this, geneArrayIOService);
        this.points = points;
        this.scale = scale;

        //this.sourceWells = null;
        this.sourceWells = controller.toSourceWells(points);
        Collections.sort(sourceWells, new SourceWellComparator());

        /*
         * Bind to color property. Update image if color is changed
         */
        final PropertyChangeListener listener = new SourceWellChangeListener();

        for (SourceWell s : sourceWells) {
            s.propertyChangeSupport().addPropertyChangeListener("color", listener);
        }

        setBounds(100, 100, 556, 375);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        try {
            initialize();
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        //
    }

    /**
     * @return
     */
    protected JLabel getBackgroundLabel() {
        if (backgroundLabel == null) {
            backgroundLabel = new JLabel();
            backgroundLabel.setText("Background:");
        }

        return backgroundLabel;
    }

    /**
     * @return
     */
    protected JRadioButton getBlackRadioButton() {
        if (blackRadioButton == null) {
            blackRadioButton = new JRadioButton();
            buttonGroup.add(blackRadioButton);
            blackRadioButton.setText("Black");
            blackRadioButton.setEnabled(false);
            blackRadioButton.setSelected(true);
        }

        return blackRadioButton;
    }

    /**
     * @return
     */
    protected JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton();
            cancelButton.setText("Close");
            cancelButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    GALPointViewer.this.dispose();
                }

            });
        }

        return cancelButton;
    }

    /**
     * @return
     */
    protected JButton getExportControlsButton() {
        if (exportControlsButton == null) {
            exportControlsButton = new JButton();
            exportControlsButton.setText("Export Selected");
            exportControlsButton.setToolTipText("Export the selected points to a definition file.");
            exportControlsButton.setEnabled(true);
            exportControlsButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        controller.generateControlFile(points, sourceWells);
                    }
                    catch (Exception e1) {
                        ErrorInfo info = new ErrorInfo("Error: " + e1.getClass().getName(),
                                                       "Unable to export point definitions to a file. Reason: " +
                                                       e1.getMessage(), null, "GALPointViewer", e1, Level.WARNING,
                                                           null);

                        JXErrorPane.showDialog(GALPointViewer.this, info);
                    }
                }

            });
        }

        return exportControlsButton;
    }

    /**
     * @return
     */
    protected JLabel getImageLabel() {
        if (imageLabel == null) {
            imageLabel = new JLabel();
            imageLabel.setText("");

            final BufferedImage bi = controller.toBufferedImage(points, false, scale, null);
            final Image image = ImageUtilities.toImage(bi);
            final Icon icon = new ImageIcon(image);

            imageLabel.setIcon(icon);
        }

        return imageLabel;
    }

    /**
     * @return
     */
    protected JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton();
            okButton.setText("Save Image");
            okButton.setToolTipText("Save the image with a legend to a file");
            okButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        controller.generateImage(points, sourceWells, scale);
                    }
                    catch (Exception e1) {
                        ErrorInfo info = new ErrorInfo("Error: " + e1.getClass().getName(),
                                                       "Unable to generate a layout image. Reason: " + e1.getMessage(),
                                                       null, "GALPointViewer", e1, Level.WARNING, null);

                        JXErrorPane.showDialog(GALPointViewer.this, info);
                    }
                }

            });
        }

        return okButton;
    }

    /**
     * @return
     */
    protected JPanel getOkCancelPanel() {
        if (okCancelPanel == null) {
            okCancelPanel = new JPanel();

            final GroupLayout groupLayout = new GroupLayout((JComponent) okCancelPanel);

            groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.TRAILING).add(
                    groupLayout.createSequentialGroup().addContainerGap().add(getBackgroundLabel()).addPreferredGap(
                        LayoutStyle.RELATED).add(getBlackRadioButton()).addPreferredGap(LayoutStyle.RELATED).add(
                        getWhiteRadioButton()).addPreferredGap(
                        LayoutStyle.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(
                        getCancelButton()).addPreferredGap(LayoutStyle.RELATED).add(
                        getExportControlsButton()).addPreferredGap(LayoutStyle.RELATED).add(
                        getOkButton()).addContainerGap()));
            groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.LEADING).add(
                    groupLayout.createSequentialGroup().add(
                        groupLayout.createParallelGroup(GroupLayout.BASELINE).add(getOkButton()).add(
                            getCancelButton()).add(getBackgroundLabel()).add(getBlackRadioButton()).add(
                            getExportControlsButton()).add(getWhiteRadioButton())).addContainerGap(
                                GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
            okCancelPanel.setLayout(groupLayout);
        }

        return okCancelPanel;
    }

    /**
     * @return
     */
    protected JPanel getSrcWellEditorPanel() {
        if (srcWellEditorPanel == null) {
            srcWellEditorPanel = new JPanel();
            srcWellEditorPanel.setLayout(new BoxLayout(srcWellEditorPanel, BoxLayout.Y_AXIS));

            for (SourceWell s : sourceWells) {
                SourceWellPanel p = new SourceWellPanel(s);

                srcWellEditorPanel.add(p);
            }

            srcWellEditorPanel.add(Box.createVerticalGlue());
        }

        return srcWellEditorPanel;
    }

    /**
     * @return
     */
    protected JRadioButton getWhiteRadioButton() {
        if (whiteRadioButton == null) {
            whiteRadioButton = new JRadioButton();
            buttonGroup.add(whiteRadioButton);
            whiteRadioButton.setText("White");
            whiteRadioButton.setEnabled(false);
        }

        return whiteRadioButton;
    }

    private void initialize() throws Exception {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(getImageLabel(), BorderLayout.CENTER);
        JScrollPane scrollPane = new JScrollPane(getSrcWellEditorPanel());
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        getContentPane().add(scrollPane, BorderLayout.EAST);
        getContentPane().add(getOkCancelPanel(), BorderLayout.SOUTH);
    }

    /**
         * Launch the application
         * @param args
         */
    public static void main(String args[]) {

        /*
         * Lookup Guice module name then activate dependency injection. We'll set the
         * injector in the dispatcher so that other components can access it.
         */
        ResourceBundle bundle = ResourceBundle.getBundle("imaging");
        final String moduleName = bundle.getString("guice.module");
        Module module = null;

        try {
            module = (Module) Class.forName(moduleName).newInstance();
        }
        catch (Exception ex) {
            System.err.println("Failed to instantiate Guice module for dependency injection");

            return;
        }

        final Injector injector = Guice.createInjector(Stage.PRODUCTION, module);
        final GeneArrayIOService geneArrayIOService = injector.getInstance(GeneArrayIOService.class);

        try {

            //String resource = "/imaging/A1-RESULT-bac08mar2408h100ml160s.png";
            //URL url = resource.getClass().getResource(resource);
            URL url = new URL(
                "file:///Users/brian/workspace/esp/esp-imaging/src/test/resources/imaging/A1-TEMPLATE-2_8_08 10 probebac_2008-03-05_151722_Run.txt");
            List<GALPoint> values = geneArrayIOService.readAsJava(url);

            //List<GALPoint> values = (List<GALPoint>) PiezzoUtilities.readArrayDescription(url);
            GALPointViewer frame = new GALPointViewer(values, 100, geneArrayIOService);

            frame.pack();
            frame.setVisible(true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SourceWellChangeListener implements PropertyChangeListener {

        /**
         * This method gets called when a color property is changed.
         *
         * @param evt A PropertyChangeEvent object describing the event source
         *            and the property that has changed.
         */

        public void propertyChange(PropertyChangeEvent evt) {

            /*
             * Redraw the image
             */
            final BufferedImage bi = controller.toBufferedImage(points, false, scale, sourceWells);
            final Image image = ImageUtilities.toImage(bi);
            final Icon icon = new ImageIcon(image);

            getImageLabel().setIcon(icon);

            //GALPointViewer.this.pack();
        }
    }


    private class SourceWellComparator implements Comparator<SourceWell> {

        public int compare(SourceWell o1, SourceWell o2) {
            return o1.name().compareTo(o1.name());
        }
    }
}
