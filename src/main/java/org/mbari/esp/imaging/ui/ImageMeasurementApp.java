package org.mbari.esp.imaging.ui;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;


import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventTopicSubscriber;
import org.mbari.esp.ia.services.ToolBox;
import org.mbari.util.SystemUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author brian
 */
public class ImageMeasurementApp {

    private static final Logger log = LoggerFactory.getLogger(ImageMeasurementApp.class);
    private ImageMeasurementFrame frame;
    private final Injector injector;

    /**
     * The ImageMeasurementApp gets garbage collected shortly after startup. To
     * hang on to the EventTopicSubscribers we store them in a static list. This
     * is very important, otherwise they get gc'd too.
     */
    private static final List<EventTopicSubscriber> gcPreventionOfSubscribers = new Vector<EventTopicSubscriber>();

    /**
     * Constructs ...
     */
    public ImageMeasurementApp() {

        log.debug("Starting ESP image analysis application using " + getClass().getName());

        injector = ToolBox.injector();
        Lookup.currentGuiceInjector().setValueObject(injector);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                initialize();
            }

        });

    }


    /**
     * @return
     */
    public ImageMeasurementFrame getFrame() {
        if (frame == null) {
            frame = injector.getInstance(ImageMeasurementFrame.class);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            Lookup.applicationFrameDispatcher().setValueObject(frame);
        }

        return frame;
    }

    protected void initialize() {

        // Configure EventBus
        EventTopicSubscriber fatalExceptionSubscriber = new FatalExceptionSubscriber(getFrame());
        EventTopicSubscriber nonFatalExceptionSubscriber = new NonFatalExceptionSubscriber(getFrame());
        gcPreventionOfSubscribers.add(fatalExceptionSubscriber);
        gcPreventionOfSubscribers.add(nonFatalExceptionSubscriber);
        EventBus.subscribe(Lookup.FATAL_ERROR_TOPIC, fatalExceptionSubscriber);
        EventBus.subscribe(Lookup.NONFATAL_ERROR_TOPIC, nonFatalExceptionSubscriber);

        // Setup frame
        getFrame().setVisible(true);
        getFrame().getPointGlassPane().setVisible(true);
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        /*
         * Make it pretty on Macs
         */
        if (SystemUtilities.isMacOS()) {
            SystemUtilities.configureMacOSApplication("ESP Image Measurement", "/images/esp_2G_dwg_sm.png");

        }


        if (log.isInfoEnabled()) {
            final Date date = new Date();
            log.info("This application was launched at " + date.toString());
        }

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (final Exception e) {
            log.info("Unable to set look and feel", e);
        }

        try {
            ImageMeasurementApp app = new ImageMeasurementApp();
            ImageIcon icon = new ImageIcon(app.getClass().getResource("/images/esp_2G_dwg_sm.png"));
            app.getFrame().setIconImage(icon.getImage());
        }
        catch (final Throwable e) {
            log.error("Error occured on startup", e);
        }
    }

}
