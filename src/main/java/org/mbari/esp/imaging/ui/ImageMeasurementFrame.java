package org.mbari.esp.imaging.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import javax.inject.Inject;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.bushe.swing.event.EventBus;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;
import org.mbari.awt.image.ImageUtilities;
import org.mbari.esp.ia.services.GeneArrayAnalysisService;
import org.mbari.esp.ia.services.GeneArrayIOService;
import org.mbari.esp.ia.services.ImageIOService;
import org.mbari.geometry.Point2D;
import org.mbari.swing.JFancyButton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @version    2009.04.22 at 02:23:51 PDT
 * @author     Brian Schlining [brian@mbari.org]
 */
public class ImageMeasurementFrame extends JFrame {

    private JButton saveDirectoryButton;
    private JButton button;
    private JSpinner blockSizeSpinner;
    private JLabel blockSizeLabel;
    private JSpinner spotSizeSpinner;
    private JLabel spotSizeLabel;
    private final Logger log = LoggerFactory.getLogger(getClass());
    private JButton acceptButton;
    private JButton addGalButton;
    private JButton removeGalButton;
    private JButton addManyButton;
    private JButton addOneButton;
    private JButton removeManyButton;
    private final ImageMeasurementFrameController controller;
    private JPanel galButtonPanel;
    private JList galList;
    private DefaultListModel galListModel;
    private JPanel galPanel;
    private JPanel imageButtonPanel;
    private ESPImageFrame imageFrame;
    private JList imageList;
    private DefaultListModel imageListModel;
    private JPanel imagePanel;
    private JPanel mainPanel;
    private JLabel moveXLabel;
    private JSlider moveXSlider;
    private JLabel moveYLabel;
    private JSlider moveYSlider;
    private JPanel panel;
    private GALPointGlassPane pointGlassPane;
    private JLabel rotateLabel;
    private JSlider rotationSlider;
    private JLabel scaleLabel;
    private JSlider scaleSlider;
    private JToolBar toolBar;
    private JButton layoutViewerButton;
    private static final int DEFAULT_DIAMETER = 10;

    /** 1 tick on the slider equals this in scale change */
    private final double realScaleIncrement;

    /** 1 tock on the angle slider equals this angle change */
    private final double realRadianIncrement;

    /**
     * Create the frame
     *
     * @param geneArrayIOService
     * @param imageIOService
     */
    @Inject
    public ImageMeasurementFrame(final GeneArrayIOService geneArrayIOService, final ImageIOService imageIOService) {
        super();
        controller = new ImageMeasurementFrameController(this, geneArrayIOService,
                imageIOService);
        realScaleIncrement = controller.uiScaleToRealScale(1);
        realRadianIncrement = Math.toRadians(1D / 2D);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initialize();


    }

    /**
     * @return
     */
    public JButton getAcceptButton() {
        if (acceptButton == null) {
            acceptButton = new JFancyButton();
            acceptButton.setText("");
            acceptButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/gear_play.png")));
            acceptButton.setToolTipText("Analyze Image");
            acceptButton.setEnabled(false);
            acceptButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        controller.accept(getImageFrame().getImageUrl(),
                                ImageUtilities.toBufferedImage((Image) getImageFrame().getImage()),
                                getPointGlassPane().getTransformedPoints(),
                                (Integer) getSpotSizeSpinner().getValue(),
                                (Integer) getBlockSizeSpinner().getValue());
                    }
                    catch (Exception ex) {
                        log.error("Failed to analyze GAL file", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }
                }
            });
        }

        return acceptButton;
    }

    /**
     * @return
     */
    protected JButton getAddGalButton() {
        if (addGalButton == null) {
            addGalButton = new JFancyButton();
            addGalButton.setText("");
            addGalButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/gal_add.png")));
            addGalButton.setToolTipText("Add a gene-array layout file");
            addGalButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    try {
                        controller.addGalFile();
                    }
                    catch (Exception ex) {
                        log.error("Failed to add GAL file", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }

                }
            });
        }

        return addGalButton;
    }

    protected JButton getRemoveGalButton() {
        if (removeGalButton == null) {
            removeGalButton = new JFancyButton();
            removeGalButton.setText("");
            removeGalButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/gal_delete.png")));
            removeGalButton.setToolTipText("Remove a gene-array layout file");
            removeGalButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    try {
                        controller.removeGalFiles();
                    }
                    catch (Exception ex) {
                        log.error("Failed to add GAL file", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }

                }
            });
        }

        return removeGalButton;
    }

    /**
     * @return
     */
    protected JButton getAddManyButton() {
        if (addManyButton == null) {
            addManyButton = new JFancyButton();
            addManyButton.setText("");
            addManyButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/folder_blue_add.png")));
            addManyButton.setToolTipText("Add all images in a directory");
            addManyButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        controller.addManyImages();
                    }
                    catch (Exception ex) {
                        log.error("Failed to add many images", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }
                }

            });
        }

        return addManyButton;
    }

    /**
     * @return
     */
    protected JButton getAddOneButton() {
        if (addOneButton == null) {
            addOneButton = new JFancyButton();
            addOneButton.setText("");
            addOneButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/filter_circle_add.png")));
            addOneButton.setToolTipText("Add an Image");
            addOneButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    try {
                        controller.addOneImage();
                    }
                    catch (Exception ex) {
                        log.error("Failed to add one image", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }

                }
            });
        }

        return addOneButton;
    }

/**
     * @return
     */
    protected JButton getRemoveManyButton() {
        if (removeManyButton == null) {
            removeManyButton = new JFancyButton();
            removeManyButton.setText("");
            removeManyButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/filter_circle_delete.png")));
            removeManyButton.setToolTipText("Remove Selected Images");
            removeManyButton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        controller.removeImageFiles();
                    }
                    catch (Exception ex) {
                        log.error("Failed to remove images", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }
                }

            });
        }

        return removeManyButton;
    }


    /**
     * @return
     */
    protected JPanel getGalButtonPanel() {
        if (galButtonPanel == null) {
            galButtonPanel = new JPanel();
            galButtonPanel.setLayout(new BoxLayout(galButtonPanel, BoxLayout.X_AXIS));
            galButtonPanel.add(getAddGalButton());
            galButtonPanel.add(getRemoveGalButton());
        }

        return galButtonPanel;
    }

    /**
     * @return
     */
    public JList getGalList() {
        if (galList == null) {
            galList = new JList(getGalListModel());
            galList.setCellRenderer(new URLCellRenderer());
            galList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            galList.addListSelectionListener(new GalListSelectionListener());
            galList.setEnabled(false);
            galList.addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        boolean enabled = (getImageList().getSelectedIndex() > -1) &&
                                (galList.getSelectedIndex() > -1);
                        getAcceptButton().setEnabled(enabled);
                    }
                }
            });

        }

        return galList;
    }

    public DefaultListModel<URL> getGalListModel() {
        if (galListModel == null) {
            galListModel = new DefaultListModel<URL>();
        }

        return galListModel;
    }

    /**
     * @return
     */
    protected JPanel getGalPanel() {
        if (galPanel == null) {
            galPanel = new JPanel();
            galPanel.setLayout(new BorderLayout());
            galPanel.setBorder(new TitledBorder("Gene Array Layout Files"));
            JScrollPane scrollPane = new JScrollPane(getGalList());
            scrollPane.setMinimumSize(new Dimension(200, 200));
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            galPanel.add(scrollPane, BorderLayout.CENTER);
            galPanel.add(getGalButtonPanel(), BorderLayout.SOUTH);
        }

        return galPanel;
    }

    /**
     * @return
     */
    protected JPanel getImageButtonPanel() {
        if (imageButtonPanel == null) {
            imageButtonPanel = new JPanel();
            imageButtonPanel.setLayout(new BoxLayout(imageButtonPanel, BoxLayout.X_AXIS));
            imageButtonPanel.add(getAddOneButton());
            imageButtonPanel.add(getAddManyButton());
            imageButtonPanel.add(getRemoveManyButton());
        }

        return imageButtonPanel;
    }

    public ESPImageFrame getImageFrame() {
        if (imageFrame == null) {
            imageFrame = new ESPImageFrame(controller.imageIOService()) {

                @Override
                public void setVisible(boolean b) {
                    super.setVisible(b);
                    getPointGlassPane().setVisible(b);
                }


            };
            imageFrame.setLocationRelativeTo(null);
            imageFrame.setGlassPane(getPointGlassPane());

            /*
             * When the image is changed the GAL needs to be redrawn to scale on the new image.
             */
            imageFrame.addPropertyChangeListener(ESPImageFrame.PROP_IMAGE, new PropertyChangeListener() {

                public void propertyChange(final PropertyChangeEvent evt) {

                    // Redraw GAL on new Image
                    try {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                controller.updateGalOnNewImage((BufferedImage) evt.getOldValue(),
                                                               (BufferedImage) evt.getNewValue());
                            }

                        });
                    }
                    catch (Exception ex) {
                        log.info("Could not relocate GAL on new image: ", ex);
                        EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                    }
                }

            });

//            imageFrame.addComponentListener(new ComponentAdapter() {
//
//                @Override
//                public void componentResized(ComponentEvent e) {
//                    getPointGlassPane().setDisplayScale(imageFrame.getImageScale());
//                }
//            });


        }

        return imageFrame;
    }

    /**
     * @return
     */
    public JList getImageList() {
        if (imageList == null) {
            imageList = new JList(getImageListModel());
            imageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            imageList.setCellRenderer(new URLCellRenderer());

//            imageList.setCellRenderer(getVideoTileCellRenderer());
            imageList.addListSelectionListener(new ImageListSelectionListener());
            imageList.addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        boolean enabled = (getGalList().getSelectedIndex() > -1) &&
                                (imageList.getSelectedIndex() > -1);
                        getAcceptButton().setEnabled(enabled);

                        getGalList().setEnabled(getImageList().getSelectedIndex() > -1);

                    }
                }
            });
        }

        return imageList;
    }

    public DefaultListModel<URL> getImageListModel() {
        if (imageListModel == null) {
            imageListModel = new DefaultListModel<URL>();
        }

        return imageListModel;
    }

    /**
     * @return
     */
    protected JPanel getImagePanel() {
        if (imagePanel == null) {
            imagePanel = new JPanel();
            imagePanel.setLayout(new BorderLayout());
            imagePanel.setBorder(new TitledBorder("Filter Images"));
            JScrollPane scrollPane = new JScrollPane(getImageList());
            scrollPane.setMinimumSize(new Dimension(200, 200));
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            imagePanel.add(scrollPane, BorderLayout.CENTER);
            imagePanel.add(getImageButtonPanel(), BorderLayout.SOUTH);
        }

        return imagePanel;
    }

    /**
     * @return
     */
    protected JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel();
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
            mainPanel.add(getImagePanel());
            mainPanel.add(getGalPanel());
        }

        return mainPanel;
    }

    /**
     * @return
     */
    protected JLabel getMoveXLabel() {
        if (moveXLabel == null) {
            moveXLabel = new JLabel();
            moveXLabel.setText("Move X:");
        }

        return moveXLabel;
    }

    /**
     * @return
     */
    public JSlider getMoveXSlider() {
        if (moveXSlider == null) {
            moveXSlider = new JSlider();
            moveXSlider.setPaintTicks(true);
            moveXSlider.setMinorTickSpacing(50);
            moveXSlider.setMajorTickSpacing(100);
            moveXSlider.addChangeListener(new ChangeListener() {

                public void stateChanged(ChangeEvent e) {
                    final GALPointGlassPane gp = getPointGlassPane();
                    final int x = moveXSlider.getValue();
                    final int y = getMoveYSlider().getValue();

                    gp.translate(x, y);
                }

            });

        }

        return moveXSlider;
    }

    /**
     * @return
     */
    protected JLabel getMoveYLabel() {
        if (moveYLabel == null) {
            moveYLabel = new JLabel();
            moveYLabel.setText("Move Y:");
        }

        return moveYLabel;
    }

    /**
     * @return
     */
    public JSlider getMoveYSlider() {
        if (moveYSlider == null) {
            moveYSlider = new JSlider();
            moveYSlider.setPaintTicks(true);
            moveYSlider.setMajorTickSpacing(100);
            moveYSlider.setMinorTickSpacing(50);
            moveYSlider.addChangeListener(new ChangeListener() {

                public void stateChanged(ChangeEvent e) {
                    final GALPointGlassPane gp = getPointGlassPane();
                    final int y = moveYSlider.getValue();
                    final int x = getMoveXSlider().getValue();

                    gp.translate(x, y);
                }

            });
        }

        return moveYSlider;
    }

    /**
 * @return
 */
    protected JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel();

            final GroupLayout groupLayout = new GroupLayout((JComponent) panel);
            groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.LEADING)
                    .add(groupLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(groupLayout.createParallelGroup(GroupLayout.LEADING)
                            .add(groupLayout.createSequentialGroup()
                                .add(groupLayout.createParallelGroup(GroupLayout.LEADING)
                                    .add(getRotateLabel())
                                    .add(getScaleLabel()))
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(groupLayout.createParallelGroup(GroupLayout.LEADING)
                                    .add(getScaleSlider(), GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
                                    .add(getRotationSlider(), GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)))
                            .add(groupLayout.createSequentialGroup()
                                .add(getMoveXLabel())
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(getMoveXSlider(), GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE))
                            .add(groupLayout.createSequentialGroup()
                                .add(getMoveYLabel())
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(getMoveYSlider(), GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE))
                            .add(groupLayout.createSequentialGroup()
                                .add(groupLayout.createParallelGroup(GroupLayout.LEADING)
                                    .add(getSpotSizeLabel())
                                    .add(getBlockSizeLabel()))
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(groupLayout.createParallelGroup(GroupLayout.LEADING, false)
                                    .add(getBlockSizeSpinner(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .add(getSpotSizeSpinner(), GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))))
                        .addContainerGap())
            );
            groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.LEADING)
                    .add(groupLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(groupLayout.createParallelGroup(GroupLayout.LEADING)
                            .add(getRotationSlider(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .add(getRotateLabel()))
                        .addPreferredGap(LayoutStyle.RELATED)
                        .add(groupLayout.createParallelGroup(GroupLayout.TRAILING)
                            .add(getScaleLabel())
                            .add(getScaleSlider(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.RELATED)
                        .add(groupLayout.createParallelGroup(GroupLayout.TRAILING)
                            .add(getMoveXLabel())
                            .add(getMoveXSlider(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.RELATED)
                        .add(groupLayout.createParallelGroup(GroupLayout.TRAILING)
                            .add(getMoveYLabel())
                            .add(getMoveYSlider(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.RELATED)
                        .add(groupLayout.createParallelGroup(GroupLayout.BASELINE)
                            .add(getSpotSizeLabel())
                            .add(getSpotSizeSpinner(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.RELATED)
                        .add(groupLayout.createParallelGroup(GroupLayout.BASELINE)
                            .add(getBlockSizeLabel())
                            .add(getBlockSizeSpinner(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );

            panel.setLayout(groupLayout);
        }

        return panel;
    }

    /**
     * This pane draws the GAL points on top of the filter image and handles
     * transforms to the points.
     *
     * @return
     */
    public GALPointGlassPane getPointGlassPane() {
        if (pointGlassPane == null) {

            /**
             * This listener watches for property changes on the glasspane (i.e. scale,
             * translation, etc.) and updates this UI. This keeps changes made to values
             * on the glasspane, independent of this frame UI widget, in sync with the
             * UI controls.
             */
            class GlassPanePropChangeListener implements PropertyChangeListener {

                public void propertyChange(PropertyChangeEvent evt) {
                    if (evt.getPropertyName().equals(GALPointGlassPane.PROP_TRANSLATION)) {
                        Point2D<Integer> trans = (Point2D<Integer>) evt.getNewValue();
                        getMoveXSlider().setValue(trans.getX());
                        getMoveYSlider().setValue(trans.getY());
                    }
                    else if (evt.getPropertyName().equals(GALPointGlassPane.PROP_SCALE)) {
                        Double realScale = (Double) evt.getNewValue();
                        getScaleSlider().setValue(controller.realScaleToUiScale(realScale));
                    }
                    else if (evt.getPropertyName().equals(GALPointGlassPane.PROP_ROTATION)) {
                        Double angleRadians = (Double) evt.getNewValue();
                        getRotationSlider().setValue((int) Math.round(Math.toDegrees(angleRadians) * 2));
                    }
                }

            }

            pointGlassPane = new GALPointGlassPane();
            pointGlassPane.setDiameter(DEFAULT_DIAMETER);
            pointGlassPane.addPropertyChangeListener(new GlassPanePropChangeListener());

            /*
             * Add key listeners for rotation and scale here. Translaction keys are
             * already handled by the glasspane.
             */
            InputMap inputMap = pointGlassPane.getInputMap();
            ActionMap actionMap = pointGlassPane.getActionMap();

            final String ACTION_KEY_ROTATE_CW = "-theta";
            final String ACTION_KEY_ROTATE_CCW = "+theta";
            final String ACTION_KEY_SCALE_IN = "-scale";
            final String ACTION_KEY_SCALE_OUT = "+scale";


            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0), ACTION_KEY_ROTATE_CW);
            actionMap.put(ACTION_KEY_ROTATE_CW, new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    log.debug("CW Adjusting angle to " + (pointGlassPane.getRotation() - realRadianIncrement));
                    pointGlassPane.rotate(pointGlassPane.getRotation() - realRadianIncrement);
                }
            });

            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0), ACTION_KEY_ROTATE_CCW);
            actionMap.put(ACTION_KEY_ROTATE_CCW, new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    log.debug("CCW Adjusting angle to " + (pointGlassPane.getRotation() + realRadianIncrement));
                    pointGlassPane.rotate(pointGlassPane.getRotation() + realRadianIncrement);
                }
            });

            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, 0), ACTION_KEY_SCALE_IN);
            actionMap.put(ACTION_KEY_SCALE_IN, new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    pointGlassPane.scale(pointGlassPane.getScale() - realScaleIncrement);
                }
            });

            inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_X, 0), ACTION_KEY_SCALE_OUT);
            actionMap.put(ACTION_KEY_SCALE_OUT, new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    pointGlassPane.scale(pointGlassPane.getScale() + realScaleIncrement);
                }
            });
        }

        return pointGlassPane;
    }

    /**
     * @return
     */
    protected JLabel getRotateLabel() {
        if (rotateLabel == null) {
            rotateLabel = new JLabel();
            rotateLabel.setText("Rotate: ");
        }

        return rotateLabel;
    }

    /**
     * @return
     */
    protected JSlider getRotationSlider() {
        if (rotationSlider == null) {
            rotationSlider = new JSlider();
            rotationSlider.setValue(0);
            rotationSlider.setPaintTicks(true);
            rotationSlider.setMajorTickSpacing(45);
            rotationSlider.setMaximum(720);
            rotationSlider.addChangeListener(new ChangeListener() {

                public void stateChanged(ChangeEvent e) {

                    final GALPointGlassPane gp = getPointGlassPane();
                    final double angle = rotationSlider.getValue() * realRadianIncrement;
                    gp.rotate(angle);

                }

            });
            rotationSlider.add(getButton());
        }

        return rotationSlider;
    }

    /**
     * @return
     */
    protected JLabel getScaleLabel() {
        if (scaleLabel == null) {
            scaleLabel = new JLabel();
            scaleLabel.setText("Scale: ");
        }
        return scaleLabel;
    }

    /**
     * @return
     */
    public JSlider getScaleSlider() {
        if (scaleSlider == null) {
            scaleSlider = new JSlider();
            scaleSlider.setMaximum(controller.realScaleToUiScale(controller.REAL_SCALE_MAX()));
            scaleSlider.setMinimum(controller.realScaleToUiScale(controller.REAL_SCALE_MIN()));
            scaleSlider.setValue(1);
            scaleSlider.addChangeListener(new ChangeListener() {

                public void stateChanged(ChangeEvent e) {

                    final GALPointGlassPane gp = getPointGlassPane();
                    final double scale = controller.uiScaleToRealScale(scaleSlider.getValue());
                    gp.scale(scale);

                }

            });
        }

        return scaleSlider;
    }

    protected JButton getLayoutViewerButton() {
        if (layoutViewerButton == null) {
            layoutViewerButton = new JFancyButton();
            layoutViewerButton.setText("");
            layoutViewerButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/gal_edit.png")));
            layoutViewerButton.setToolTipText("Edit GAL file labels");
            layoutViewerButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent actionEvent) {
                    LayoutViewer layoutViewer = new LayoutViewer(controller.geneArrayIOService());
                    layoutViewer.setVisible(true);
                }
            });

        }
        return layoutViewerButton;

    }

    /**
     * @return
     */
    protected JToolBar getToolBar() {
        if (toolBar == null) {
            toolBar = new JToolBar();
            toolBar.add(getAcceptButton());
            toolBar.add(getSaveDirectoryButton());
            toolBar.add(getLayoutViewerButton());
        }

        return toolBar;
    }

    private void initialize() {
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(getToolBar(), BorderLayout.NORTH);
        getContentPane().add(getMainPanel(), BorderLayout.CENTER);
        getContentPane().add(getPanel(), BorderLayout.SOUTH);
    }

    private class GalListSelectionListener implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                URL galUrl = (URL) getGalList().getSelectedValue();

                // Draw Gal on glasspane
                try {
                    controller.renderGalFile(galUrl);
                }
                catch (Exception ex) {
                    log.info("Could not render GAL file: " + galUrl.toExternalForm(), ex);
                    EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                }

            }
        }
    }


    private class ImageListSelectionListener implements ListSelectionListener {


        public void valueChanged(ListSelectionEvent e) {

            if (!e.getValueIsAdjusting()) {
                URL imageUrl = (URL) getImageList().getSelectedValue();

                // Draw Image on imageFrame
                try {
                    controller.renderImageFile(imageUrl);
                }
                catch (Exception ex) {
                    log.info("Could not render image: " + imageUrl.toExternalForm(), ex);
                    EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex);
                }
            }
        }
    }
    /**
     * @return
     */
    protected JLabel getSpotSizeLabel() {
        if (spotSizeLabel == null) {
            spotSizeLabel = new JLabel();
            spotSizeLabel.setText("Spot Size:");
        }
        return spotSizeLabel;
    }
    /**
     * @return
     */
    /**
     * @return
     */
    public JSpinner getSpotSizeSpinner() {
        if (spotSizeSpinner == null) {
            spotSizeSpinner = new JSpinner();
            spotSizeSpinner.setValue(10);
            spotSizeSpinner.setToolTipText("Define diameter of GAL spots");
            spotSizeSpinner.addChangeListener(new ChangeListener() {

                public void stateChanged(ChangeEvent e) {
                    getPointGlassPane().setDiameter((Integer) spotSizeSpinner.getValue());
                }
            });
        }
        return spotSizeSpinner;

    }

    /**
     * @return
     */
    protected JLabel getBlockSizeLabel() {
        if (blockSizeLabel == null) {
            blockSizeLabel = new JLabel();
            blockSizeLabel.setText("Block Size:");
        }
        return blockSizeLabel;
    }

    /**
     * @return
     */
    public JSpinner getBlockSizeSpinner() {
        if (blockSizeSpinner == null) {
            blockSizeSpinner = new JSpinner();
            blockSizeSpinner.setValue(3);
            blockSizeSpinner.setToolTipText("Define intensity block size to average intensity across");
        }
        return blockSizeSpinner;
    }

    /**
     * @return
     */
    protected JButton getButton() {
        if (button == null) {
            button = new JButton();
            button.setText("New JButton");
            button.setBounds(0, 16197, 111, 25);
        }
        return button;
    }

    /**
     * @return
     */
    protected JButton getSaveDirectoryButton() {
        if (saveDirectoryButton == null) {
            saveDirectoryButton = new JFancyButton();
            saveDirectoryButton.setToolTipText("Select directory to save data into");
            saveDirectoryButton.setIcon(new ImageIcon(getClass().getResource("/images/48px/folder_into.png")));
            saveDirectoryButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    controller.selectSaveDirectory();
                }
            });
        }
        return saveDirectoryButton;
    }



}

