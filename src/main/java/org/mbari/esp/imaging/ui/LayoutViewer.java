package org.mbari.esp.imaging.ui;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import java.util.logging.Level;
import javax.swing.ButtonGroup;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.mbari.esp.ia.services.GeneArrayIOService;
import org.mbari.esp.ia.services.ToolBox;

public class LayoutViewer {

    private JLabel displayLabel;
    private ButtonGroup buttonGroup = new ButtonGroup();
    private JRadioButton singleRadioButton;
    private JRadioButton fullRadioButton;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField fileTextField;
    private JButton layoutFileButton;
    private JFrame frame;
    private JFileChooser fileChooser;
    private JFileChooser imageChooser;
    private final LayoutViewerController controller;
    private final Preferences preferences = Preferences.userNodeForPackage(getClass());
    public static final String GAL_DIR_PREFERENCE = "GAL file directory";

    /**
     * Launch the application
     * @param args
     */
    public static void main(String args[]) {
        try {

            final Injector injector = ToolBox.injector();
            final LayoutViewer layoutViewer = injector.getInstance(LayoutViewer.class);

            layoutViewer.frame.setVisible(true);
            layoutViewer.getCancelButton().addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                    System.exit(0);
                }
            });
            layoutViewer.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the application
     */
    @Inject
    public LayoutViewer(final GeneArrayIOService geneArrayIOService) {
        this.controller = new LayoutViewerController(geneArrayIOService);
        initialize();
    }

    /**
     * Initialize the contents of the frame
     */
    private void initialize() {
        frame = new JFrame();
        frame.setResizable(false);
        final GroupLayout groupLayout = new GroupLayout((JComponent) frame.getContentPane());
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.LEADING).add(groupLayout.createSequentialGroup().addContainerGap().add(groupLayout.createParallelGroup(GroupLayout.LEADING).add(GroupLayout.TRAILING, groupLayout.createSequentialGroup().add(getCancelButton()).addPreferredGap(LayoutStyle.RELATED).add(getOkButton())).add(groupLayout.createSequentialGroup().add(groupLayout.createParallelGroup(GroupLayout.TRAILING).add(getLayoutFileButton()).add(getDisplayLabel())).addPreferredGap(LayoutStyle.RELATED).add(groupLayout.createParallelGroup(GroupLayout.LEADING).add(groupLayout.createSequentialGroup().add(getFullRadioButton()).addPreferredGap(LayoutStyle.RELATED).add(getSingleRadioButton())).add(getFileTextField(), GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE)))).addContainerGap()));
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.LEADING).add(groupLayout.createSequentialGroup().addContainerGap().add(groupLayout.createParallelGroup(GroupLayout.BASELINE).add(getFileTextField(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).add(getLayoutFileButton())).addPreferredGap(LayoutStyle.RELATED).add(groupLayout.createParallelGroup(GroupLayout.BASELINE).add(getFullRadioButton()).add(getSingleRadioButton()).add(getDisplayLabel())).add(6, 6, 6).add(groupLayout.createParallelGroup(GroupLayout.BASELINE).add(getOkButton()).add(getCancelButton())).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        frame.getContentPane().setLayout(groupLayout);
        frame.setTitle("Layout Viewer");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
    }

    public void setVisible(boolean visible) {
        frame.setVisible(visible);
    }

    /**
     * @return
     */
    protected JButton getLayoutFileButton() {
        if (layoutFileButton == null) {
            layoutFileButton = new JButton();
            layoutFileButton.addActionListener(new LayoutFileButtonActionListener());
            layoutFileButton.setText("Select Layout Description");
            layoutFileButton.setToolTipText("Select the text file that describes the array layout");
        }
        return layoutFileButton;
    }

    protected JFileChooser getFileChooser() {
        if (fileChooser == null) {
            String openDirectory = preferences.get(GAL_DIR_PREFERENCE, System.getProperty("user.home"));
            fileChooser = new JFileChooser(openDirectory);
        }
        return fileChooser;
    }

    protected JFileChooser getImageChooser() {
        if (imageChooser == null) {
            String userHome = System.getProperty("user.home");
            imageChooser = new JFileChooser(userHome);
        }
        return imageChooser;
    }

    /**
     * @return
     */
    protected JTextField getFileTextField() {
        if (fileTextField == null) {
            fileTextField = new JTextField();
            fileTextField.getDocument().addDocumentListener(new MyDocumentListener());
        }
        return fileTextField;
    }

    /**
     * @return
     */
    protected JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton();
            okButton.addActionListener(new OkButtonActionListener());
            okButton.setText("Show Image");
            okButton.setEnabled(false);
        }
        return okButton;
    }

    /**
     * @return
     */
    protected JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton();
            cancelButton.setText("Quit");
            cancelButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    frame.dispose();
                }
            });
        }
        return cancelButton;
    }

    private class LayoutFileButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            final JFileChooser fc = getFileChooser();
            int returnCode = fc.showOpenDialog(frame);
            if (returnCode == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                getFileTextField().setText(file.getAbsolutePath());

                /*
                 * Read save directory from preferences
                 */
                String openDirectory = preferences.get(GAL_DIR_PREFERENCE, System.getProperty("user.home"));
                preferences.put(GAL_DIR_PREFERENCE, file.getParent());
            }
        }
    }

    private class OkButtonActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            File inputFile = new File(getFileTextField().getText());
            try {
                controller.showLayout(inputFile, getSingleRadioButton().isSelected());
            }
            catch (Exception ex) {
                ErrorInfo info = new ErrorInfo("Error: " + ex.getClass().getName(),
                        "Unable to display the file '" + inputFile.getAbsolutePath() + "'",
                        null,
                        "LayoutViewer",
                        ex,
                        Level.WARNING,
                        null);
                JXErrorPane.showDialog(frame, info);
            }

        }
    }

    private class MyDocumentListener implements DocumentListener {

        public void changedUpdate(DocumentEvent e) {
            validate();
        }

        public void insertUpdate(DocumentEvent e) {
            validate();
        }

        public void removeUpdate(DocumentEvent e) {
            validate();
        }

        private void validate() {
            final String[] text = new String[]{getFileTextField().getText()};
            boolean enable = false;
            for (String t : text) {
                // TODO Check that t is not all whitespace
                enable = t != null && t.length() > 0;
                if (!enable) {
                    break;
                }
            }

            getOkButton().setEnabled(enable);
        }
    }

    /**
     * @return
     */
    protected JRadioButton getFullRadioButton() {
        if (fullRadioButton == null) {
            fullRadioButton = new JRadioButton();
            buttonGroup.add(fullRadioButton);
            fullRadioButton.setText("Full Layout");
            fullRadioButton.setSelected(true);
        }
        return fullRadioButton;
    }

    /**
     * @return
     */
    protected JRadioButton getSingleRadioButton() {
        if (singleRadioButton == null) {
            singleRadioButton = new JRadioButton();
            buttonGroup.add(singleRadioButton);
            singleRadioButton.setText("Single Array");
        }
        return singleRadioButton;
    }

    /**
     * @return
     */
    protected JLabel getDisplayLabel() {
        if (displayLabel == null) {
            displayLabel = new JLabel();
            displayLabel.setText("Display:");
        }
        return displayLabel;
    }
}
