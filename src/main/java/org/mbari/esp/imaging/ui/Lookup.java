/*
 * @(#)Lookup.java   2013.04.30 at 02:25:17 PDT
 *
 * Copyright 2009 MBARI
 *
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



package org.mbari.esp.imaging.ui;

import com.google.inject.Injector;
import java.util.List;
import org.bushe.swing.event.EventBus;
import org.bushe.swing.event.EventTopicSubscriber;
import org.mbari.util.Dispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author brian
 */
public class Lookup {

    private static final String APP_FRAME_KEY = "org.mbari.esp.imageing.Lookup-Application Frame";

    /**
     * Subscribers to this topic will get and {@link Exception} as the data
     */
    public static final String FATAL_ERROR_TOPIC = "org.mbari.esp.imageing.Lookup-FATAL ERROR";

    /**
     * Subscribers to this topic will get a {@link String} as the data
     */
    public static final String NONFATAL_ERROR_TOPIC = "org.mbari.esp.imageing.Lookup-NON FATAL ERROR";
    private static final Logger log = LoggerFactory.getLogger(Lookup.class);

    /**
     * A subscriber thta logs all events on all topics on the {@link EventBus}
     */
    private static final EventTopicSubscriber LOGGING_SUBSCRIBER = new EventTopicSubscriber() {

        public void onEvent(String topic, Object data) {
            if (log.isDebugEnabled()) {
                log.debug("EventBus event published\n\tTOPIC: " + topic + "\n\tDATA: " + data);

                String[] topics = { Lookup.NONFATAL_ERROR_TOPIC, Lookup.FATAL_ERROR_TOPIC };
                StringBuilder sb = new StringBuilder("EventBus Subscriptions:");

                for (String t : topics) {
                    sb.append("\nTOPIC: ").append(t);

                    List<EventTopicSubscriber> nfs = EventBus.getSubscribersToTopic(t);

                    for (EventTopicSubscriber eventTopicSubscriber : nfs) {
                        sb.append("\n\t").append(eventTopicSubscriber);
                    }

                }

                log.debug(sb.toString());
            }
        }
    };

    /**  */
    public static final Object CURRENT_GUICE_INJECTOR = Injector.class;

    static {

        /*
        * Add logging to our topics so we know when they've been called
        */
        EventBus.subscribe(FATAL_ERROR_TOPIC, LOGGING_SUBSCRIBER);
        EventBus.subscribe(NONFATAL_ERROR_TOPIC, LOGGING_SUBSCRIBER);

    }

    /**
     * @return
     */
    public static Dispatcher applicationFrameDispatcher() {
        return Dispatcher.getDispatcher(APP_FRAME_KEY);
    }

    /**
     * @return
     */
    public static Dispatcher currentGuiceInjector() {
        return Dispatcher.getDispatcher(CURRENT_GUICE_INJECTOR);
    }
}
