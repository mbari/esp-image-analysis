package org.mbari.esp.imaging.ui;


import java.awt.Frame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.bushe.swing.event.EventTopicSubscriber;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.error.ErrorLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author brian
 */
public class NonFatalExceptionSubscriber implements EventTopicSubscriber {

    /**  */
    private final Frame parentFrame;
    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Constructs ...
     *
     * @param parent
     */
    public NonFatalExceptionSubscriber(Frame parent) {
        this.parentFrame = parent;
    }

    /**
     * Defines a custom format for the stack trace as String.
     */
    String formatStackTraceForDialogs(Throwable throwable, boolean isCause) {

        //add the class name and any message passed to constructor
        final StringBuilder result = new StringBuilder();

        result.append("<h3>");

        if (isCause) {
            result.append("Caused by: ");
        }

        result.append(throwable.toString()).append("</h3>");

        final String newLine = "<br/>";

        //add each element of the stack trace
        for (StackTraceElement element : throwable.getStackTrace()) {
            result.append(element);
            result.append(newLine);
        }

        final Throwable cause = throwable.getCause();

        if (cause != null) {
            result.append(formatStackTraceForDialogs(cause, true));
        }

        return result.toString();
    }

    /**
     *
     * @param topic
     * @param error
     */
    public void onEvent(String topic, Object error) {

        log.debug("Handling " + topic + ": " + error );

        String msg = null;
        String details = null;
        Throwable data = null;

        if (error instanceof Throwable) {
            data = (Throwable) error;
            msg = data.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                msg = "An error occurred. Refer to details for more information.";
            }
            details = formatStackTraceForDialogs(data, true);
        }
        else {
            msg = error.toString();
            details = error.toString();
        }


        /*
         * Create an error pane to display the error stuff
         */
        JXErrorPane errorPane = new JXErrorPane();
        Icon errorIcon = new ImageIcon(getClass().getResource("/images/yellow-smile.jpg"));
        ErrorInfo errorInfo = new ErrorInfo("ESP - Something exceptional occured (and we don't like that)", msg,
            details, null, data, ErrorLevel.WARNING, null);

        errorPane.setIcon(errorIcon);
        errorPane.setErrorInfo(errorInfo);
        JXErrorPane.showDialog(parentFrame, errorPane);
    }

    @Override
    protected void finalize() throws Throwable {
        log.debug("Finalize called");
        super.finalize();
    }


}

