package org.mbari.esp.imaging.ui;

import org.mbari.esp.Lists;
import org.mbari.esp.ia.awt.SourceWell;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.services.GeneArrayAnalysisService$;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.collection.immutable.Set;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Brian Schlining
 * @since 2013-05-01
 */
public class PiezzoUtilities {

    private static Logger log = LoggerFactory.getLogger(PiezzoUtilities.class);


    /**
     * method
     */
    public static int estimateGridSize(Collection<? extends GALPoint> points, int scale) {
        scala.Option<Set<SourceWell>> none = scala.Option.apply(null);
        BufferedImage image = GeneArrayAnalysisService$.MODULE$.toBufferedImage(
                Lists.toScala(new ArrayList<GALPoint>(points)), false, (double) scale, 1, none);
        return estimateGridSize(image);
    }

    public static int estimateGridSize(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        // Calculate distance between non-zero pixels We're assuming that the grid is relativley square
        // and so we're ignoring dy values.
        int thisX = 0;
        int lastX = 0;
        List<Integer> dx = new ArrayList<Integer>();
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w ++) {

                int z = image.getRGB(w, h);
                Color rgbColor = new Color(z);
                if (rgbColor.getRed() > 0 || rgbColor.getGreen() > 0 || rgbColor.getBlue() > 0) {
                    log.debug("I(" + w + ", " + h + ") = " + z + " : " + rgbColor);
                    lastX = thisX;
                    thisX = w;
                    if (lastX > 0) {
                        dx.add(thisX - lastX);
                    }
                }
            }
            lastX = 0;
            thisX = 0;
        }


        Map<Integer, Integer> distanceMap = new HashMap<Integer, Integer>();
        for (Integer i : dx) {

            if (!distanceMap.containsKey(i)) {
                distanceMap.put(i, 0);
            }
            int v = distanceMap.get(i);
            distanceMap.put(i, ++v);
        }

        Integer mostFrequentDx = Collections.max(distanceMap.values());
        Integer gridSize = 1;
        for (Integer key: distanceMap.keySet()) {
            Integer value = distanceMap.get(key);
            if (mostFrequentDx == value) {
                gridSize = key;
                break;
            }
        }
        gridSize -= 2;

        if (gridSize == null || gridSize <= 0) {
            gridSize = 1;
        }

        if (log.isDebugEnabled()) {
            log.debug("GridSize is " + gridSize);
        }

        return gridSize;
    }

}
