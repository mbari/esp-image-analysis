package org.mbari.esp.imaging.ui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;
import org.mbari.awt.image.ImageUtilities;
import org.mbari.esp.ia.awt.SourceWell;

public class SourceWellPanel extends JPanel {

	private JButton colorButton;
	private JCheckBox controlCheckBox;
	private JTextField textField;
	private JLabel label;
	private final SourceWell sourceWell;

	/**
	 * Create the panel
	 */
	public SourceWellPanel(SourceWell sourceWell) {
		super();
		if (sourceWell == null) {
			throw new NullPointerException("An attempt was made to create a SourceWellPanel" +
					" with a null SourceWell. This is not allowed!");
		}
		this.sourceWell = sourceWell;
		try {
			initialize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//
	}
	private void initialize() throws Exception {
		final GroupLayout groupLayout = new GroupLayout((JComponent) this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(GroupLayout.LEADING)
				.add(groupLayout.createSequentialGroup()
					.addContainerGap()
					.add(getLabel())
					.addPreferredGap(LayoutStyle.RELATED)
					.add(getTextField(), GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.RELATED)
					.add(getControlCheckBox())
					.addPreferredGap(LayoutStyle.RELATED)
					.add(getColorButton())
					.addContainerGap(14, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(GroupLayout.LEADING)
				.add(groupLayout.createSequentialGroup()
					.addContainerGap()
					.add(groupLayout.createParallelGroup(GroupLayout.BASELINE)
						.add(getLabel())
						.add(getTextField(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.add(getControlCheckBox())
						.add(getColorButton()))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}

	/**
	 * @return
	 */
	protected JLabel getLabel() {
		if (label == null) {
			label = new JLabel();
			label.setText(sourceWell.name());
		}
		return label;
	}
	/**
	 * @return
	 */
	protected JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			String description = sourceWell.description();
			description = description == null ? sourceWell.name() : description;
			textField.setText(description);
            textField.setToolTipText("Set the value of " + sourceWell.name() + " to appear in the legend");

            /*
			 * Update the sourceWell description as text is entered in the textField.
			 */
			textField.getDocument().addDocumentListener(new DocumentListener() {

                public void changedUpdate(DocumentEvent e) {
					updateSourceWell();
				}

				public void insertUpdate(DocumentEvent e) {
					updateSourceWell();
				}

				public void removeUpdate(DocumentEvent e) {
					updateSourceWell();
				}

				private void updateSourceWell() {
					sourceWell.description_$eq(textField.getText());
				}

			});
		}
		return textField;
	}

	/**
	 * @return
	 */
	protected JCheckBox getControlCheckBox() {
		if (controlCheckBox == null) {
			controlCheckBox = new JCheckBox();
			controlCheckBox.setText("");
			//controlCheckBox.setSelected(sourceWell.isControl());
            controlCheckBox.setSelected(true);

			/*
			 * Toggle the control state of the SourceWell when the radio button
			 * is checked or unchecked
			 */
			controlCheckBox.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					sourceWell.control_$eq(controlCheckBox.isSelected());
				}

			});
		}
		return controlCheckBox;
	}

	/**
	 * @return
	 */
	protected JButton getColorButton() {
		if (colorButton == null) {
			colorButton = new JButton();
            Color color = sourceWell.color();
            if (color == null) {
                colorButton.setText("Color");
                colorButton.setIcon(null);
            }
            else {
                colorButton.setText("");
                colorButton.setIcon(makeColorIcon(color));
            }
            colorButton.setToolTipText("Set the color of the spots for " + sourceWell.name());



            /*
			 * Allow user to select a color for the image color
			 */
			colorButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					// add action listener to display color picker
					Color newColor = JColorChooser.showDialog(
		                     SourceWellPanel.this,
		                     "Choose Background Color",
		                     sourceWell.color());

					if (newColor != null) {
						sourceWell.color_$eq(newColor);
                        colorButton.setText("");
                        colorButton.setIcon(makeColorIcon(newColor));
					}
                    else {
                        colorButton.setText("Color");
                        colorButton.setIcon(null);
                    }
                }

			});
			// TODO when color is selected change button color to match
		}
		return colorButton;
	}

	public SourceWell getSourceWell() {
		return sourceWell;
	}


    private Icon makeColorIcon(Color color) {
        int width = 10;
        int height = 10;
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        bi.createGraphics();
        Graphics graphics = bi.getGraphics();
        graphics.setColor(color);
        graphics.fillRect(0, 0, width, height);
        Image image = ImageUtilities.toImage(bi);
        ImageIcon icon = new ImageIcon(image);
        return icon;
    }

}
