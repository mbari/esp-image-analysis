package org.mbari.esp.imaging.ui;

import java.awt.Component;
import java.net.URL;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * Simplifies the display of the URL so that only the filename is displayed
 *
 * @author brian
 */
public class URLCellRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label =  (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        String[] parts = ((URL) value).getFile().split("/");
        label.setText(parts[parts.length - 1]);
        label.setToolTipText(((URL) value).toExternalForm());
        return label;
    }

}