package org.mbari.esp.imaging.ui.imageframe;

import org.mbari.awt.image.ImageUtilities;
import org.mbari.esp.ia.geometry.GALPoint;
import org.mbari.esp.ia.services.ImageIOService;
import org.mbari.swing.JImageCanvas;
import org.mbari.swing.JImageFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vars.annotation.ui.imagepanel.MultiLayerUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.List;

/**
 * @author Brian Schlining
 * @since 2015-02-18T14:54:00
 */
public class ESPImageFrame extends JImageFrame {

    private static final String ACTION_KEY_MOVE_X_LEFT = "-X";
    private static final String ACTION_KEY_MOVE_X_RIGHT = "+X";
    private static final String ACTION_KEY_MOVE_Y_UP = "-Y";
    private static final String ACTION_KEY_MOVE_Y_DOWN = "+Y";

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ImageIOService imageIOService;

    private final MultiLayerUI<JImageCanvas> layerUI = new MultiLayerUI<>();
    private final JXGALPointPainter<JImageCanvas> painter = new JXGALPointPainter<>();

    private URL imageUrl;

    private final JLabel label = new JLabel();
    private BufferedImage image;
    public static final String PROP_IMAGE = "imageUrl";

    /**
     *
     * @param imageIOService Service used to read images
     */
    public ESPImageFrame(ImageIOService imageIOService) {
        this.imageIOService = imageIOService;
        initialize();
    }

    protected void initialize() {
        JComponent imageDisplay = getImageDisplayComponent();

        layerUI.addPainter(painter);
        layerUI.installUI(imageDisplay);

        InputMap inputMap = imageDisplay.getInputMap();
        ActionMap actionMap = imageDisplay.getActionMap();

        /*
         * Listen for arrow keys to move the points around. We can't add
         * rotation and scale here because we don't know the increment that
         * will be used by external UI componenets. Those listeners will
         * have to be added by parent UI compoenent.
         */
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), ACTION_KEY_MOVE_Y_UP);
        actionMap.put(ACTION_KEY_MOVE_Y_UP, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                painter.translate(painter.getTranslatedCentroid().getX(),
                        painter.getTranslatedCentroid().getY() - 1);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), ACTION_KEY_MOVE_Y_DOWN);
        actionMap.put(ACTION_KEY_MOVE_Y_DOWN, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                painter.translate(painter.getTranslatedCentroid().getX(),
                        painter.getTranslatedCentroid().getY() + 1);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), ACTION_KEY_MOVE_X_LEFT);
        actionMap.put(ACTION_KEY_MOVE_X_LEFT, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                painter.translate(painter.getTranslatedCentroid().getX() - 1,
                        painter.getTranslatedCentroid().getY());
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), ACTION_KEY_MOVE_X_RIGHT);
        actionMap.put(ACTION_KEY_MOVE_X_RIGHT, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                painter.translate(painter.getTranslatedCentroid().getX() + 1,
                        painter.getTranslatedCentroid().getY());
            }
        });
    }

    public List<GALPoint> getPoints() {
        return painter.getPoints();
    }

    public List<GALPoint> getTransformedPoints() {
        return painter.getTransformedPoints();
    }

    public void rotate(double angleRadians) {
        painter.rotate(angleRadians);
    }

    public void scale(double scale) {
        painter.scale(scale);
    }

    public void setDiameter(int diameter) {
        painter.setDiameter(diameter);
    }

    public URL getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(URL imageUrl) {
        this.imageUrl = imageUrl;
        log.debug("setImageUrl( " + imageUrl + " )");

        if (imageUrl != null) {

            /*
             * Use swingutilities to invoke changes on the EventDisplatch thread.
             *
             * Remove label from view and add progress bar
             */
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    setResizable(true);
                    setTitle(getImageUrl().toExternalForm());
                    getImageDisplayComponent().remove(label);
                    remove(label);
                    (new ImageLoader(getImageUrl())).execute();
                }
            });

        }
        else {
            /*
             *  Clear out data if no value is set
             */
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    setResizable(true);
                    setTitle("");
                    image = null;
                    label.setText("No image available");
                    label.setIcon(null);
                    add(label);
                    setResizable(false);
                }
            });
        }
    }


    /**
     * @return
     */
    private class ImageLoader extends org.mbari.swingworker.SwingWorker<BufferedImage, Object> {

        final URL url;
        final BufferedImage oldImage;

        public ImageLoader(final URL url) {
            this.url = url;
            this.oldImage = ImageUtilities.toBufferedImage(getImage());
        }

        protected BufferedImage doInBackground() throws Exception {
            log.debug("Reading image from " + url);
            return imageIOService.readAsBufferedImage(url);
        }

        @Override
        protected void done() {


            try {
                image = get();
                log.debug("Image " + url + " [" + image.getWidth() + " x " + image.getHeight() + " pixels] has been loaded");
                label.setText(null);
                label.setIcon(new ImageIcon(image));
            }
            catch (Exception e) {
                log.debug("Failed to read image", e);
                label.setText("Failed to fetch image from " + url.toExternalForm());
                label.setIcon(null);
            }

            getContentPane().add(label);
            setResizable(false);
            ESPImageFrame.this.firePropertyChange(PROP_IMAGE, oldImage, image);
        }
    }

}
