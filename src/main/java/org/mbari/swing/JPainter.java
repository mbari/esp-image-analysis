package org.mbari.swing;


import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeSupport;

/**
 * @author Brian Schlining
 * @since 2015-02-13T16:06:00
 */
public interface JPainter<A extends JComponent> {

    PropertyChangeSupport getPropertyChangeSupport();

    boolean isDirty();

    void paintLayer(Graphics2D g2, JComponent c);

    void processMouseEvent(MouseEvent me, JLayer<? extends A> jxl);

    void processMouseMotionEvent(MouseEvent me, JLayer<? extends A> jxl);

    void setDirty(boolean dirty);
}
