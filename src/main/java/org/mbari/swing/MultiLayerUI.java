package org.mbari.swing;

import javax.swing.*;
import javax.swing.plaf.LayerUI;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Brian Schlining
 * @since 2015-02-13T16:08:00
 */
public class MultiLayerUI<A extends JComponent> extends LayerUI<A> {

    private Queue<JPainter<A>> painters = new LinkedBlockingQueue<JPainter<A>>();
    private final JLayer layer;

    public MultiLayerUI(JLayer layer) {
        this.layer = layer;
    }

    /**
     * Used to listen to the setDirty flag set by the painters
     */
    private final PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equalsIgnoreCase("dirty")) {
                setDirty(true);
            }
        }
    };

    public void addPainter(JPainter<A> painter) {
        painters.add(painter);
        painter.getPropertyChangeSupport().addPropertyChangeListener(propertyChangeListener);
        setDirty(true);
    }

    public void clearPainters() {
        for (JPainter painter : painters) {
            painter.getPropertyChangeSupport().removePropertyChangeListener(propertyChangeListener);
        }
        painters.clear();
        setDirty(true);
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        Graphics2D g2 = (Graphics2D) g;
        for (JPainter<A> painter : painters) {
            //log.debug("Painting: " + painter);
            painter.paintLayer(g2, c);
        }
    }


    protected void processMouseEvent(MouseEvent me, JLayer<? extends A> jxl) {

        //System.out.println(getClass().getSimpleName() + ".processMouseEvent -> MouseEvent: " + me.getID());
        super.processMouseEvent(me, jxl);
        boolean dirty = false;
        for (JPainter<A> painter : painters) {
            painter.processMouseEvent(me, jxl);
            if (painter.isDirty()) {
                dirty = true;
            }
        }
        setDirty(dirty);
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent me, JLayer<? extends A> jxl) {

        //System.out.println(getClass().getSimpleName() + ".processMouseMotionEvent -> MouseEvent: " + me.getID());
        super.processMouseMotionEvent(me, jxl);
        boolean dirty = false;
        for (JPainter<A> painter : painters) {
            painter.processMouseMotionEvent(me, jxl);
            if (painter.isDirty()) {
                dirty = true;
            }
        }
        setDirty(dirty);
    }

    /**
     *
     * @param painter
     */
    public void removePainter(JPainter<A> painter) {
        painters.remove(painter);
        painter.getPropertyChangeSupport().removePropertyChangeListener(propertyChangeListener);
        setDirty(true);
    }

    protected void setDirty(boolean dirty) {
        if (dirty) {
            layer.repaint();
        }
    }
}
