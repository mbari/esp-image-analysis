/**
 * These classes are borrowed from <a href="http://github.com/hohonuuli/vars">VARS</a>.
 *
 * They allow drawing on top of an image in a rezisable frame
 */
package vars.annotation.ui.imagepanel;

