package org.mbari.esp

import java.util.{List => JList}
import scala.collection.JavaConverters._


/**
 *
 * @author Brian Schlining
 * @since 2013-05-01
 */
object Lists {

  def toJava[A](list: List[A]): JList[A] = list.asJava

  def toScala[A](list: JList[A]): List[A] = list.asScala.toList

}
