package org.mbari.esp.ia

import java.util.Arrays
import org.mbari.math.{Statlib, Matlib}

/**
 * class that calculates the statistics of an array of double
 * @param pixels The array of interest
 *
 * @author Brian Schlining
 * @since 2012-04-11
 */
class ArrayStats(pixels: Array[Double]) {
    require(pixels != null, "Array can not be null")
    require(pixels.size >= 1, "Array can not be empty")

    private[this] val array = {
        val copy = Arrays.copyOf(pixels, pixels.size)
        Arrays.sort(copy)
        copy
    }

    lazy val min = array(0)

    lazy val max = array(array.size - 1)

    lazy val mean = Statlib.mean(array)

    lazy val median = Statlib.median(array)

    lazy val std = Statlib.standardDeviation(array)

    lazy val count = array.size

}
