package org.mbari.esp.ia

/**
 *
 * @author Brian Schlining
 * @since 2012-05-02
 */

object Math {

    /**
     * Computes factorial. We need this for imaging in order because the number of permutations
     * of ''n'' distinct objects is equal to the factorial of ''n''
     *
     * @param n The value for the factorial
     * @return
     */
    def factorial(n: BigInt, result: BigInt = 1): BigInt = if (n == 0) result
            else factorial(n - 1, n * result)

    /**
     * Computes the possible number of distinct combinations of n items into groups of c items
     * @param n The number of items
     * @param c The number of items in a set
     * @return The number of combinations of n items in c sets.
     */
    def combinations(n: BigInt, c: BigInt): BigInt = factorial(n) / (factorial(c) * factorial(n - c))

}
