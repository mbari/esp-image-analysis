package org.mbari.esp.ia

import Jama.Matrix
import scala.collection.mutable
import scala.math._
import org.mbari.esp.ia.geometry.{Point2D, LabeledDoublePoint2D}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
object Matrices {

    def affineTransform(t: Matrix, src: Seq[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] = {
        val b = toAffineMatrix(src)
        val a = t.times(b)
        val transformed = new mutable.ArrayBuffer[LabeledDoublePoint2D]
        for (c <- 0 until a.getColumnDimension) {
            transformed += new LabeledDoublePoint2D(a.get(0, c), a.get(0, c), src(c).label)
        }
        transformed
    }

    def inverseAffineTransform(t: Matrix, src: Seq[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] = {
        val a = toAffineMatrix(src)
        val b = t.inverse().times(a)
        val transformed = new mutable.ArrayBuffer[LabeledDoublePoint2D]
        for (c <- 0 until b.getColumnDimension) {
            transformed += new LabeledDoublePoint2D(b.get(0, c), b.get(1, c), src(c).label)
        }
        transformed
    }

    /**
     * Creates a 3xn Affine Matrix from a list of points where row 1 is the x-values,
     * row 2 is the y values and row 3 is filled with '1's.
     *
     * @param a The list of points to convert to a matrix
     * @return A matrix suitable for affine transforms
     */
    def toAffineMatrix[A: Numeric](a: Seq[Point2D[A]]): Matrix = {
        val d = Array.ofDim[Double](3, a.size)
        val numeric = implicitly[Numeric[A]]
        for (c <- 0 until a.size) {
            val p = a(c)
            d(0)(c) = numeric.toDouble(p.x)
            d(1)(c) = numeric.toDouble(p.y)
            d(2)(c) = 1
        }
        new Matrix(d)
    }

    /**
     * Creates a rotation matrix from an angle. Rotation is clockwise in image
     * coordinate space. (+Y down, +X right)
     *
     * @param clockwiseRotationInRadians The degrees (clockwise) to rotate
     * @return The rotation Matrix: [cos(d) sin(d);-sin(d) cos(d)]
     */
    def toRotationMatrix(clockwiseRotationInRadians: Double): Matrix = {
        val d = clockwiseRotationInRadians
        val rotation = Array(Array(cos(d), sin(d)), Array(-sin(d), cos(d)))
        new Matrix(rotation)
    }

    /**
     * Creates a scale matrix from a scale factor
     * @param scale THe scale factor
     * @return The scale matrix: [scale 0; 0 scale]
     */
    def toScaleMatrix(scale: Double): Matrix = new Matrix(Array(Array(scale, 0), Array(0, scale)))

    /**
     * Converts a list of points to a 2xn Matrix. Where the 1st row is x-values
     * and the 2nd row is y-values.
     *
     * @param a A List of points
     * @return A Matrix representing the point data
     */
    def toMatrix[A: Numeric](a: Seq[Point2D[A]]): Matrix = {
        val d = Array.ofDim[Double](2, a.size)
        val numeric = implicitly[Numeric[A]]
        for (c <- 0 until a.size) {
            val p = a(c)
            d(0)(c) = numeric.toDouble(p.x)
            d(1)(c) = numeric.toDouble(p.y)
        }
        new Matrix(d)
    }

    /**
     * Converts a MxN matrix to a list of points. The 1st row of the matrix
     * is converted to x values, the 2nd row to y values. The other rows are ignored.
     * Each point is labeled with the column number.
     *
     * @param matrix The matrix to convert to points
     * @return A List of points extracted from each column in the matrix
     */
    def toPoints(matrix: Matrix): Seq[LabeledDoublePoint2D] = {
        require(matrix.getRowDimension > 1, "Your matrix did not have 2 or more rows")
        val points = new mutable.ArrayBuffer[LabeledDoublePoint2D]
        for (c <- 0 until matrix.getColumnDimension) {
            points += new LabeledDoublePoint2D(matrix.get(0, c), matrix.get(1, c), c)
        }
        points
    }


}
