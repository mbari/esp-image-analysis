package org.mbari.esp.ia.awt

import ij.process.ImageProcessor
import java.awt.geom.Ellipse2D
import java.awt.image.BufferedImage
import java.awt.{Color, Graphics2D}
import java.util.{List => JList}
import org.mbari.esp.ia.geometry.{Point2D}
import org.mbari.esp.ia.imglib.extendImageProcessor
import org.mbari.esp.ia.imglib.ToRGBFn
import scala.collection.JavaConverters._

/**
 *
 * @author Brian Schlining
 * @since 2012-05-15
 */

object Drawing {

  def draw[A : Numeric](imageProcessor: ImageProcessor, points: Iterable[Point2D[A]]): BufferedImage = {
    val numeric = implicitly[Numeric[A]]
    val colorImage = ToRGBFn(imageProcessor).bufferedImage
    val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]
    val extractedPoints = points.map { p =>
      val x = numeric.toDouble(p.x)
      val y = numeric.toDouble(p.y)
      new Ellipse2D.Double(x - 2, y - 2, 4, 4)
    }
    g2.setPaint(Color.GREEN)
    extractedPoints.foreach(g2.draw(_))
    colorImage
  }


  def draw(width: Int, height: Int, spotDiameter: Int, points: JList[Point2D[Double]]): BufferedImage = {
    val image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_BINARY)
    val graphics = image.getGraphics.asInstanceOf[Graphics2D]
    for (p <- points.asScala) {
      graphics.fillOval(p.x.toInt, p.y.toInt, spotDiameter, spotDiameter)
    }
    image
  }

}
