package org.mbari.esp.ia.awt

import java.awt.{Rectangle, Shape, Color}
import scala.math._
import org.mbari.esp.ia.geometry.LabeledDoublePoint2D
import java.awt.geom.{Ellipse2D, Point2D}

/**
 * Bean class that holds information to be used to draw a region of an image.
 * @param id The String identifier for a RegionInfo
 * @param shape The shape to be draw
 * @param color The color to be used to draw the shape
 * @param text Descriptive text about the RegionInfo
 *
 * @author Brian Schlining
 * @since 2012-03-07
 */
case class RegionInfo(id: String, shape: Shape, color: Color, text: String) {

    def this(id: String, point: Point2D, blockSize: Int, color: Color, text: String) = this(id,
        new Rectangle(round(point.getX - blockSize / 2).toInt,
            round(point.getY - blockSize / 2).toInt,
            blockSize, blockSize),
        color, text)

}

object RegionInfo {
    /**
     * Convert labeled points to simple circular regions.
     *
     * @param points The points
     * @param color The color to be applied to the region
     * @param diameter
     * @return
     */
    def apply(points: Iterable[LabeledDoublePoint2D], color: Color, diameter: Int): Iterable[RegionInfo] = {
        points.map { p =>
            val id = p.label.toString
            val shape = new Ellipse2D.Double(p.x - (diameter / 2D), p.y - (diameter / 2D), diameter,
                diameter)
            RegionInfo(id, shape, color, id)
        }
    }
}
