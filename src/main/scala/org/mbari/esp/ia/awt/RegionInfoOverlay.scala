package org.mbari.esp.ia.awt

import ij.process.ImageProcessor
import java.awt.image.BufferedImage
import java.awt.{RenderingHints, Font, AlphaComposite, Graphics2D, Color}
import scala.math._

/**
 * Function for drawing [[RegionInfo]]s onto an
 * [[ij.process.ImageProcessor]]
 *
 * @param fillType How to draw the regions (ie. Outline or Fill)
 *
 * @author Brian Schlining
 * @since 2012-03-07
 */
class RegionInfoOverlay(fillType: RegionInfoOverlay.FillType)
        extends ((ImageProcessor, Iterable[RegionInfo]) => BufferedImage) {

    def apply(imageProcessor: ImageProcessor, regionInfos: Iterable[RegionInfo]) = {
        val bufferedImage = imageProcessor.duplicate().convertToRGB().getBufferedImage
        draw(bufferedImage, regionInfos)
        bufferedImage
    }

    private def draw(bufferedImage: BufferedImage, regionInfos: Iterable[RegionInfo]) {
        val g2 = bufferedImage.getGraphics.asInstanceOf[Graphics2D]
        val cTrans = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4F)
        val cOpaque = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0F)
        val font = g2.getFont
        val newFont = new Font(font.getName, Font.PLAIN, 9)
        g2.setFont(newFont)
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        val width = bufferedImage.getWidth
        val height = bufferedImage.getHeight

        var vi = 1
        // Draw stuff on image
        regionInfos.zipWithIndex.foreach( xx => {
            val ri = xx._1
            val n = xx._2
            val rect = ri.shape.getBounds2D
            val x = round(rect.getMaxX).toInt
            val y = round(rect.getMinY).toInt

            // Draw polygon that defines the region of interest and label with it's id
            g2.setColor(ri.color)
            g2.setComposite(cTrans)
            if (fillType == RegionInfoOverlay.Fill) {
                g2.fill(ri.shape)
            }
            g2.draw(ri.shape)
            g2.drawString(ri.id, x, y)

            // Write the legend
            val s = ri.id + ":" + ri.text
            val lineMetrics = g2.getFont.getLineMetrics(s, g2.getFontRenderContext)
            g2.setComposite(cOpaque)
            var u = 10F
            var v = (lineMetrics.getHeight + 1) * n + lineMetrics.getHeight
            if (v >= (height - lineMetrics.getHeight)) {
                v = (lineMetrics.getHeight + 1) * vi + lineMetrics.getHeight
                u = (width - (width * 0.2)).toFloat
                vi += 1
            }

            g2.drawString(s, u, v)

        })
    }

}

object RegionInfoOverlay {
    /**
     * A default seq of colors that can be used for drawing
     */
    val Colors = Seq(Color.RED,
        Color.ORANGE.darker(),
        Color.YELLOW,
        Color.GREEN,
        Color.CYAN,
        Color.BLUE,
        Color.MAGENTA,
        Color.PINK,
        Color.WHITE,
        Color.GRAY,
        Color.DARK_GRAY,
        Color.GREEN.darker().darker(),
        Color.BLUE.darker(),
        Color.MAGENTA.darker())


    /**
     * Enumeration of the types of Fills that a RegionInfo can be drawn with.
     */
//    object FillType extends Enumeration("Outline", "Fill") {
//        type FillType = Value
//        val Outline, Fill = Value
//    }

  case class FillType(name: String)
  val Outline = FillType("Outline")
  val Fill = FillType("Fill")

}


