package org.mbari.esp.ia.awt

import java.awt.Color
import java.beans.PropertyChangeSupport


/**
 * Stores information about a sourcewell needed for visual representation
 *
 * @author Brian Schlining
 * @since 2012-03-26
 */
class SourceWell(private var _color: Color, private var _control: Boolean, private var _name: String,
    private var _description: String) {

  val propertyChangeSupport = new PropertyChangeSupport(this)

  def color = _color
  def color_=(newColor: Color) {
    val oldColor = color
    _color = newColor
    propertyChangeSupport.firePropertyChange("color", oldColor, newColor)
  }

  def control = _control
  def control_=(newControl: Boolean){
    val oldControl = _control
    _control = newControl
    propertyChangeSupport.firePropertyChange("control", oldControl, newControl)
  }

  def name = _name
  def name_=(newName: String) {
    val oldName = _name
    _name = newName
    propertyChangeSupport.firePropertyChange("name", oldName, newName)
  }

  def description = _description
  def description_=(newDescription: String) {
    val oldDescription = _description
    _description = newDescription
    propertyChangeSupport.firePropertyChange("description", oldDescription, newDescription)
  }

  def id = if (description == null) name else description
}


object SourceWell {

    /**
     * A default color list
     * @return
     */
    lazy val Colors = List(Color.RED,
        Color.ORANGE.darker(),
        Color.YELLOW,
        Color.GREEN,
        Color.CYAN,
        Color.BLUE,
        Color.MAGENTA,
        Color.PINK,
        Color.WHITE,
        Color.GRAY,
        Color.DARK_GRAY,
        Color.GREEN.darker().darker(),
        Color.BLUE.darker(),
        Color.MAGENTA.darker());

}