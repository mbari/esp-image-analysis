package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.Point2D
import scala.math._
import scala.util.Random
import PointChromosomeExt.extendChromosome // implicit extension

/**
 * Adds a bit of ''noise'' to a randomly selected point in the chromosome.
 *
 * @param noiseRange Added noise will be within +/- noiseRange of the original value
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */
class AddNoiseMutation(noiseRange: Int) extends Mutation {

    private[this] val randomGenerator = new Random

    /**
     * Add nose to a chromosome
     * @param chromosome The chromosome to alter.
     * @return A new chromosome with some ''noise'' added.
     */
    def apply(chromosome: Chromosome): Chromosome = {
        val oldPoints = chromosome.points
        val idx = randomGenerator.nextInt(3)
        val newPoints = for (i <- 0 until oldPoints.size) yield {
            if (i == idx) addNoise(oldPoints(idx)) else oldPoints(i)
        }
        PointChromosomeExt.toChromosome(newPoints)
    }

    private def addNoise(oldPoint: Point2D[Int]) = {
        val xSign = signum(randomGenerator.nextDouble() - 0.5).toInt
        val ySign = signum(randomGenerator.nextDouble() - 0.5).toInt
        val xNoise = randomGenerator.nextInt(noiseRange + 1) * xSign
        val yNoise = randomGenerator.nextInt(noiseRange + 1) * ySign
        Point2D(oldPoint.x + xNoise, oldPoint.y + yNoise)
    }

}

