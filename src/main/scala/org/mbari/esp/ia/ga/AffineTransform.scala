package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.LabeledDoublePoint2D
import PointChromosomeExt.extendChromosome
import org.mbari.esp.ia.Matrices

/**
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
class AffineTransform extends Transform {

    def apply(gal: Chromosome, img: Chromosome, galPoints: Iterable[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] = {
        val a = Matrices.toAffineMatrix(gal.points)
        val b = Matrices.toAffineMatrix(img.points)
        val affine = a.times(b.inverse())
        Matrices.inverseAffineTransform(affine, galPoints.toSeq)
    }
}
