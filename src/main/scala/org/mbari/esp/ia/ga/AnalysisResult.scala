package org.mbari.esp.ia.ga

import ij.process.ImageProcessor
import org.mbari.esp.ia.geometry.{Point3D, GALPoint}
import java.net.URL

/**
 * Bean to hold the results of an image analysis
 *
 * @param galUrl The URL to the GAL file used for the analysis
 * @param imageUrl The URL to the image file that was analyzed
 * @param imageProcessor The image that was processed
 * @param intensities The results. key = GAL point, value = a point where x,y determine the upper
 *                    left corner of the block, z is the mean intensity of the block
 * @param spotRadius The spot radius used for the analysis
 * @param blockSize The size of the square block, in pixels, used to determine the intensity of
 *                  each spot.
 */
class AnalysisResult(val galUrl: URL,
        val imageUrl: URL,
        val imageProcessor: ImageProcessor,
        val intensities: Map[GALPoint, Point3D[Double]],
        val spotRadius: Float,
        val blockSize: Int)

