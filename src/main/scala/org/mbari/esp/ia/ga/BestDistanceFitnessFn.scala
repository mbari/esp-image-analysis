package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{PointDistancesFn, Points, LabeledDoublePoint2D}


/**
 * Evaluates 2 sets of points. Uses the distribution of interspot-distances in the target set to see if
 * the distances in the first set are realistic.
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */
class BestDistanceFitnessFn(val binSize: Int) extends FitnessFn {

    /**
     *
     * @param source Typically these are the points from the GAL file
     * @param target Typically these are the points extracted from the image
     * @return
     */
    def apply(source: Iterable[LabeledDoublePoint2D], target: Iterable[LabeledDoublePoint2D]): Double = {
        val bestDistance = Points.bestDistance(source, binSize)
        val distancePairs = PointDistancesFn(target)
        val distances = distancePairs.map(_.value).filter(_ > 0)
        val maxDistance = distances.max
        val minDistance = distances.min
        if (maxDistance < (bestDistance - binSize) ||
                minDistance > (bestDistance + binSize)) {
            0
        }
        else {
            1
        }
    }

}
