package org.mbari.esp.ia.ga


import scala.util.Random

/**
 * Simple mutation that flips a bit in the Chromosome
 *
 * @author Brian Schlining
 * @since 2012-03-20
 */
class BitMutation extends Mutation {

    private[this] val randomGenerator = new Random

    def apply(chromosome: Chromosome): Chromosome = {
        val idx = randomGenerator.nextInt(PointChromosomeExt.ChromosomeLength)
        chromosome.flip(idx)
    }
}
