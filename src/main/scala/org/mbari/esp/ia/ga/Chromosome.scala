package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.Point2D

/**
 * Interface for capturing the minimum need information to represent a chromosome.
 * The BigInteger methods of clearBit, setBit, flipBit and testBit are most useful
 * for mutation and querying.
 *
 * A BigInteger is used to represent the bits as it already has the needed
 * bit manipulation methods. However, alone it is not sufficient to represent
 * a chromosome as leading zeros, which are important information, are lost.
 * We deal with this by using a length method which specifies the number of individual
 * genes in a chromosome, including leading zeros
 *
 * @author Brian Schlining
 * @since 2012-03-20
 */
case class Chromosome(genes: BigInt, length: Int) {

    /**
     * A BigInteger is used to represent the bits as it already has the needed
     * bit manipulation methods. However, alone it is not sufficient to represent
     * a chromosome as leading zeros, which are important information, are lost.
     * We deal with this by using a length method which specifies the number of individual
     * genes in a chromosome, including leading zeros
     * @return The underliying BigInt representation
     */
    def clear(n: Int): Chromosome = new Chromosome(genes.clearBit(n), length)
    def set(n: Int): Chromosome = new Chromosome(genes.setBit(n), length)
    def flip(n: Int): Chromosome = new Chromosome(genes.flipBit(n), length)
    def test(n: Int): Boolean = genes.testBit(n)

    /**
     * Represents a BigInteger as a string of 1's and 0's padded with leading
     * zeros out to the specified length
     */
    override lazy val toString: String = {
        val base = genes.toString(2)
        val d = length - base.size
        "0" * d + base
    }
}
