package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{LabeledDoublePoint2D}

/**
 * Generates a set of chromosomes representing all possible combinations of a set of points.
 * Note that only the first two genes in each chromosome are used. The 3rd gene is just filled
 * in with a point that is not the same as either of the other genes.
 *
 * @author Brian Schlining
 * @since 2012-05-02
 */
object CreateChromosomesFn extends (Iterable[LabeledDoublePoint2D] => Iterable[Chromosome]) {

    def apply(points: Iterable[LabeledDoublePoint2D]): Iterable[Chromosome] = {
        val pointMap = points.map(p => p.label -> p).toMap
        for (key0 <- pointMap.keySet; key1 <- pointMap.keySet; if key0 != key1) yield {
            val p0 = pointMap(key0)
            val p1 = pointMap(key1)
            toChromosome(p0, p1, points)
        }
    }


    /**
     * Generates a chromosome from 2 points (and an arbitrarily selected point from that's not
     * the same as the other 2 points. This third point isn't used in rigid transforms)
     * @param p0 A point
     * @param p1 A different point
     * @param points a pool of points to draw a random unused point from.
     * @return
     */
    private def toChromosome(p0: LabeledDoublePoint2D, p1: LabeledDoublePoint2D,
            points: Iterable[LabeledDoublePoint2D]) = {

        val p2s = points.filter(pp => pp != p0 && pp != p1)
        val p2 = if (p2s.isEmpty) p1 else p2s.head

        PointChromosome(p0.toInt, p1.toInt, p2.toInt)

    }
}


