package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{PointDistancesFn, Points, LabeledDoublePoint2D}


/**
 * Does the same work as BestDistanceFitnessFn. But instead of calculating distance bounds on the
 * fly, you supply them. This is computational more efficient. Max and min distances can be
 * calculated as:
 * {{{
 *  val distancePairs = PointDistancesFn(imagePoints)
 *  val distances = distancePairs.map(_.value).filter(_ > 0)
 *  val maxDistance = distances.max
 *  val minDistance = distances.min
 * }}}
 * @author Brian Schlining
 * @since 2012-03-31
 */

class DistanceCutoffFitnessFn(minDistance: Double, maxDistance: Double, val binSize: Int) extends FitnessFn {

    /**
     *
     * @param source Typically these are the points from the GAL file
     * @param target Not used here
     * @return
     */
    def apply(source: Iterable[LabeledDoublePoint2D], target: Iterable[LabeledDoublePoint2D] = Nil): Double = {
        val bestDistance = Points.bestDistance(source, binSize)
        if (maxDistance < (bestDistance - binSize) ||
                minDistance > (bestDistance + binSize)) {
            0
        }
        else {
            1
        }
    }

}
