package org.mbari.esp.ia.ga

/**
 * Bean that holds a chromosome and it's corresponding fitness value.
 * @author Brian Schlining
 * @since 2012-03-20
 */
case class Fitness(val chromosome: Chromosome, val fitness: Double)
