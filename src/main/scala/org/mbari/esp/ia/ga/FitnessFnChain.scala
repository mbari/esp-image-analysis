package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.LabeledDoublePoint2D

/**
 * Chains [[org.mbari.esp.ia.ga.FitnessFn]]s together. If a FitnessFn returns 0 then subsequent
 * functions are not evaluated
 *
 * @param fns The fitness functions. They are evaluated in the order that they are given.
 * @author Brian Schlining
 * @since 2012-03-26
 */

class FitnessFnChain(fns: FitnessFn*) extends FitnessFn {

    private[this] val tolerance = 0.00000001

    def apply(v1: Iterable[LabeledDoublePoint2D], v2: Iterable[LabeledDoublePoint2D]): Double = {
        fns.foldLeft(1D) { (v, f) =>
            if (v < tolerance) {
                0D
            }
            else {
                f(v1, v2)
            }
        }
    }
}
