package org.mbari.esp.ia.ga


import org.mbari.esp.ia.geometry.{LabeledDoublePoint2D}

/**
 * This is the the base or seed data used by the ESP image analysis GA. It contains a chromosome
 * that represents the 3 fiducial points in the GAL, a collection of '''all''' original GAL points
 * and a collection of '''all''' original image points
 *
 * @param fiducialPoints The triplet of fiducial points in the GAL. They should be a subset of
 *  galPoints
 * @param galPoints The collection of GAL Points. They may have some initial transform performed on
 *  them so that they are scaled and aligned with the imagePoints. However this set will not be
 *  modified for the remainder
 * @param imagePoints The collection of image points
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
class GADatum(val fiducialPoints: Seq[LabeledDoublePoint2D], val galPoints: Seq[LabeledDoublePoint2D],
        val imagePoints: Seq[LabeledDoublePoint2D]) {

    require(fiducialPoints.size == 3, "Exactly 3 fiducial points are required")

    /**
     * A Chromosome representation of the fiducialPoints
     */
    lazy val galChromosome = PointChromosomeExt.toChromosome(fiducialPoints.map { _.toInt })
}
