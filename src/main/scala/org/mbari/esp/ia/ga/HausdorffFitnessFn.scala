package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{EuclideanDistanceFn, HausdorffDistanceFn, LabeledDoublePoint2D}



/**
 * Uses Hausdorff Distance as an estimate of fitness
 * @param partialDistanceA partial hausdorff distance of A->B
 * @param partialDistanceB partial hausdorff distance of B->A
 */
class HausdorffFitnessFn(partialDistanceA: Double, partialDistanceB: Double) extends FitnessFn {

    private[this] val fn = new HausdorffDistanceFn(new EuclideanDistanceFn, partialDistanceA, partialDistanceB)

    /**
     * Calculate fitness
     * @param v1 Point set A
     * @param v2 Point set B
     * @return
     */
    def apply(v1: Iterable[LabeledDoublePoint2D], v2: Iterable[LabeledDoublePoint2D]): Double = {
        val distance = fn(v1, v2)
        1 / (1 + distance.value)
    }
}
