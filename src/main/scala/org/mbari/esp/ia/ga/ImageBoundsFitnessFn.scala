package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{Point2D, LabeledDoublePoint2D}


/**
 * Evaluates the ''source'' collections of points to make sure that they fall within the bounds of
 * our image
 *
 * @param width The image width
 * @param height The image height
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */
class ImageBoundsFitnessFn(val width: Double, val height: Double) extends FitnessFn {

    def apply(source: Iterable[LabeledDoublePoint2D], target: Iterable[LabeledDoublePoint2D]): Double = {
        val maxPoint = ImageBoundsFitnessFn.maximumPoint(source)
        if (maxPoint.x > width ||
                maxPoint.y > height ||
                maxPoint.x < 0 ||
                maxPoint.y < 0) {
            0
        }
        else {
            val minPoint = ImageBoundsFitnessFn.minimumPoint(source)
            if (minPoint.x > width ||
                    minPoint.y > height ||
                    minPoint.x < 0 ||
                    minPoint.y < 0) {
                0
            }
            else {
                1
            }
        }
    }

}

object ImageBoundsFitnessFn {

    private[ga] def minimumPoint(points: Iterable[Point2D[Double]]) =
        Point2D(points.map(_.x).min,  points.map(_.y).min)


    private[ga] def maximumPoint(points: Iterable[Point2D[Double]]) =
        Point2D(points.map(_.x).max,  points.map(_.y).max)

}
