package org.mbari.esp.ia.ga

import java.lang.{Long => JLong}
import org.mbari.esp.ia.geometry.Point2D

/**
 * Extends Chromosome so that the internal chromosome can represent three
 * [[org.mbari.esp.ia.geometry.Point2D]] objects
 *
 * @author Brian Schlining
 * @since 2012-03-20
 */
class PointChromosomeExt(val chromosome: Chromosome) {

    lazy val points: Seq[Point2D[Int]] = {
        require(chromosome.length == PointChromosomeExt.ChromosomeLength,
            "A Chromosome must have " + PointChromosomeExt.ChromosomeLength + "bits. Your's had " +
            chromosome.length)

        val genes = chromosome.toString
        Seq(Point2D(PointChromosomeExt.geneToInt(genes, 0), PointChromosomeExt.geneToInt(genes, 1)),
            Point2D(PointChromosomeExt.geneToInt(genes, 2), PointChromosomeExt.geneToInt(genes, 3)),
            Point2D(PointChromosomeExt.geneToInt(genes, 4), PointChromosomeExt.geneToInt(genes, 5)))
    }
}

object PointChromosomeExt {

    /**
     * We need six 32-bit chromosomes to represent 3 points
     */
    val ChromosomeLength = 32 * 6

    /**
     * Factory method to create a [[org.mbari.esp.ia.ga.Chromosome]] from 3
     * [[org.mbari.esp.ia.geometry.Point2D]]s
     * @param p0 The first point
     * @param p1 The second point
     * @param p2 The third point
     * @return
     */
    def toChromosome(p0: Point2D[Int], p1: Point2D[Int], p2: Point2D[Int]): Chromosome = {
        Chromosome(BigInt(intToGene(p0.x) + intToGene(p0.y) +
                intToGene(p1.x) + intToGene(p1.y) +
                intToGene(p2.x) + intToGene(p2.y), 2),
            ChromosomeLength)
    }
    
    def toChromosome(points: Iterable[Point2D[Int]]): Chromosome = {
        require(points.size == 3, "A point chromosome needs 3 points, you supplied " + points.size)
        val it = points.iterator
        toChromosome(it.next(), it.next(), it.next())
    }

    /**
     * Convert an integer to a 32 character 'binary' string that can be used to build a chromosome
     * @param i
     * @return
     */
    def intToGene(i: Int): String = {
        val s = Integer.toBinaryString(i)
        val d = 32 - s.size
        "0" * d + s
    }

    def geneToInt(genes: String, geneNumber: Int): Int = {
        require(genes.size == ChromosomeLength, "The genes String must " + ChromosomeLength +
                            " characters long. Yours was " + genes.size + " characters long")
        val n0 = geneNumber * 32
        val n1 = n0 + 32
        // Integer.parseInt fails with negative ints. (Java BUG!) but the work around is to use
        // parseLong and cast to an int.
        JLong.parseLong(genes.substring(n0, n1), 2).toInt
    }

    implicit def extendChromosome(chromosome: Chromosome) = new PointChromosomeExt(chromosome)

}

/**
 * Factory for Chromsomes. Basically just calls PointChromosomeExt.toChromosome.
 */
object PointChromosome {
    def apply(p0: Point2D[Int], p1: Point2D[Int], p2: Point2D[Int]): Chromosome =
            PointChromosomeExt.toChromosome(p0, p1, p2)

    def apply(points: Iterable[Point2D[Int]]): Chromosome =
            PointChromosomeExt.toChromosome(points)
}
