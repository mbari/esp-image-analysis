package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.Point2D
import PointChromosomeExt.extendChromosome
import scala.util.Random

/**
 * Mutation where the mutant will have one of it's genes (that represent a 2-D point) swapped with
 * a point from the pool
 *
 * @author Brian Schlining
 * @since 2012-03-21
 *
 * @param pool The pool of points to use as potential genes. Typically for image analysis this pool
 *             will contain all the extracted image points.
 */

class PointPoolMutation(val pool: Seq[Point2D[Int]]) extends Mutation {

    private[this] val randomGenerator = new Random

    def apply(chromosome: Chromosome): Chromosome = {
        val oldPoints = chromosome.points
        val deadGeneIdx = randomGenerator.nextInt(3)
        val newPointIdx = randomGenerator.nextInt(pool.size)
        val newPoints = for (i <- 0 until oldPoints.size) yield {
            if (i == deadGeneIdx) pool(newPointIdx) else oldPoints(i)
        }
        PointChromosomeExt.toChromosome(newPoints)
    }
}
