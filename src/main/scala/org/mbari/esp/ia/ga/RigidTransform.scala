package org.mbari.esp.ia.ga

import org.mbari.esp.ia.Matrices
import org.mbari.esp.ia.geometry.{Point2D, LabeledDoublePoint2D}


/**
 * Apply a geometric transformation to a set of gal points
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
object RigidTransform extends Transform {

    /**
     * Rotates, Scales and translates the ''galPoints'' to an image. The transform is calculated by
     * aligning the gal chromsome to the img chromosome.
     *
     * @param gal A chromosome representing the 3 GAL (well, really just 2) points to use for the
     *            transform
     * @param img A chromosome representing the 3 image (well, really just 2) points to use for the
          *            transform
     * @param galPoints The gal points to transform
     * @return The galpoints aligned to the image.
     */
    def apply(gal: Chromosome, img: Chromosome, galPoints: Iterable[LabeledDoublePoint2D]):
            Seq[LabeledDoublePoint2D] = {

        import PointChromosomeExt.extendChromosome // implicit

        val gps = gal.points
        val g0 = gps(0)
        val g1 = gps(1)

        val ips = img.points
        val i0 = ips(0)
        val i1 = ips(1)

        // --- ROTATE
        val r0 = g0.direction(g1)
        val r1 = i0.direction(i1)
        val r = r0 - r1
        
        val rotationMatrix = Matrices.toRotationMatrix(r)
        // NOTE: Rotation is around origin ... Subtract g0 form all gals
        val galPointSeq = galPoints.map { gp =>
              Point2D(gp.x - g0.x, gp.y - g0.y, gp.label)
        }.toSeq
        
        // Rotate the GALs
        val a = Matrices.toMatrix(galPointSeq)
        val b = rotationMatrix.times(a)
        
        // --- SCALE - Scale GALS to match image
        val s0 = g0.distance(g1)
        val s1 = i0.distance(i1)
        val s = s1 / s0
        val scaleMatrix = Matrices.toScaleMatrix(s)
        val newB = scaleMatrix.times(b)
        
        // --- OFFSET - Add i0 to all gals
        val rtGalPoints = Matrices.toPoints(newB)
        for (i <- 0 until rtGalPoints.size) yield {
            val rtp = rtGalPoints(i)
            val p = galPointSeq(i)
            Point2D(rtp.x + i0.x, rtp.y + i0.y, p.label)
        }

    }
}

