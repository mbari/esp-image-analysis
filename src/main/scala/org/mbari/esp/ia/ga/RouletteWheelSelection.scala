package org.mbari.esp.ia.ga

import org.slf4j.LoggerFactory
import scala.collection.mutable
import scala.math._
import java.util.Arrays

/**
 * The selection probability of an individual is proportional to it's fitness value.
 * @param survivorCount The number of survivors that should be returned.
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */

class RouletteWheelSelection(val survivorCount: Int) extends Selection {

    private[this] val log = LoggerFactory.getLogger(getClass)

    private[this] val maxIterations = survivorCount * 5

    /**
     * Select the next generation
     *
     * @param oldGeneration The generation to select from
     * @return The new generation. It will not have more than ''survivorCount'' individuals,
     *         although it is possible that it will have fewer than that.
     */
    def apply(oldGeneration: Iterable[Fitness]): Iterable[Fitness] = {
        // Filter out dead (i.e. 0 fitness) individuals and order by fitness (descending)
        val notDead = oldGeneration.toSet.filter(_.fitness > 0).toSeq.sortBy(_.fitness).reverse
        if (notDead.size <= survivorCount) {
            log.debug("The number of individuals, " + notDead.size + ", is less than the " +
                    "survivorCount, " + survivorCount + ". Selecting all individuals.")
            notDead
        }
        else {
            // Generate a seq of cumulative fitness values
            val cumulativeFitness = notDead.map(_.fitness).scanLeft(0D)(_ + _).tail.toArray
            val totalFitness = cumulativeFitness.last
            val survivors = new mutable.HashSet[Fitness]
            var n = 0
            while (survivors.size < survivorCount && n < maxIterations) {
                survivors += selectSurvivor(notDead, cumulativeFitness, totalFitness)
                n += 1
            }
            survivors
        }
    }
    
    private def selectSurvivor(fitnesses: Seq[Fitness], cumulativeFitness: Array[Double], totalFitness: Double) = {
        val r = random * totalFitness
        val idx = Arrays.binarySearch(cumulativeFitness, r)
        val correctedIdx = if (idx < 0) -idx - 1 else idx
        fitnesses(correctedIdx)
    }
}
