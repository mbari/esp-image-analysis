package org.mbari.esp.ia.ga

/**
 * Trait for a Selection function
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
trait Selection extends (Iterable[Fitness] => Iterable[Fitness]) {

    /**
     *
     * @return The maximum number of individuals that will be selected for the next generation
     */
    def survivorCount: Int

}
