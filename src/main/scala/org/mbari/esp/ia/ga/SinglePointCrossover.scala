package org.mbari.esp.ia.ga

import PointChromosomeExt.extendChromosome
import scala.util.Random

/**
 * This is a single point crossover. That means we swap one point (i.e. x-y pair)
 * between the 2 parents.
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */

object SinglePointCrossover extends Crossover {

    private[this] val randomGenerator = new Random

    /**
     * Swap a point between the 2 parents to create a new Chromosome. The ''mother'' and ''father''
     * are randomly selected so the order of parents is not relevant to which parent provides the
     * single point and which provides 2 points
     * @param parent1
     * @param parent2
     * @return A new Chromosome with one point from one of the parents and 2 points from the other
     */
    def apply(parent1: Chromosome, parent2: Chromosome): Chromosome = {
        
        val (papa, mama) = if (randomGenerator.nextInt(2) == 0) (parent1.points, parent2.points)
            else (parent2.points, parent1.points)

        val maleSwapIdx = randomGenerator.nextInt(3)
        val femaleSwapIdx = randomGenerator.nextInt(3)
        val newPoints = for (i <- 0 until papa.length) yield {
            if (i == maleSwapIdx) mama(femaleSwapIdx) else papa(i)
        }
        PointChromosomeExt.toChromosome(newPoints)
    }
}

/**
 * Function can be used as a singleton
 */

