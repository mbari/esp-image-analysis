package org.mbari.esp.ia.ga

import org.slf4j.LoggerFactory
import scala.math._

/**
 * Selects the fittest individuals from a population
 *
 * @param survivorCount The number of individuals that will be selected from a generation
 *
 * @author Brian Schlining
 * @since 2012-03-21
 */
class SkimBestIndividualsSelection(val survivorCount: Int) extends Selection {

    private[this] val log = LoggerFactory.getLogger(getClass)

    def apply(oldGeneration: Iterable[Fitness]): Iterable[Fitness] = {
        // Filter out dead (i.e. 0 fitness) individuals and order by fitness (descending)
        val notDead = oldGeneration.filter(_.fitness > 0).toSeq.sortBy(_.fitness).reverse
        if (notDead.size < survivorCount) {
            log.info("The number of individuals, " + notDead.size + ", is less than the " +
                    "survivorCount, " + survivorCount + ". Selecting all individuals.")
            notDead
        }
        else {
            notDead.take(survivorCount)
        }
    }
}

object SkimBestIndividualsSelection {

    /**
     * Convenience method
     * @param survivorCount Number of survivors
     * @param oldGeneration Population to select from
     * @return A new Generation
     */
    def apply(survivorCount: Int,  oldGeneration: Iterable[Fitness]): Iterable[Fitness] =
        new SkimBestIndividualsSelection(survivorCount).apply(oldGeneration)

    /**
     * Convenience method
     * @param survivorPercentage The percent of the population that should survive
     * @param oldGeneration Population to select from
     * @return A new generation
     */
    def apply(survivorPercentage: Double,  oldGeneration: Iterable[Fitness]): Iterable[Fitness] = {
        require(survivorPercentage > 0 && survivorPercentage <= 1, "You must specify the percentage" +
                " as a 0 < value <= 1. You gave " + survivorPercentage)
        
        val survivorCount = round(oldGeneration.size * survivorPercentage).toInt
        apply(survivorCount, oldGeneration)
    }
}
