package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{EuclideanDistanceFn, HausdorffDistances, LabeledDoublePoint2D}


/**
 *
 * @author Brian Schlining
 * @since 2012-05-14
 */

class SumOfSquaresDistanceFitnessFn(distance: Double) extends FitnessFn {

    private[this] val distanceFn = new HausdorffDistances(new EuclideanDistanceFn)

    def apply(setA: Iterable[LabeledDoublePoint2D], setB: Iterable[LabeledDoublePoint2D]): Double = {

        val (_, nearestAtoB) = distanceFn(setA, setB)
        val f = nearestAtoB.foldLeft(0D) { (fi, pp) =>
            if (pp.value >= distance) {
                fi
            }
            else {
                fi + (pp.value * pp.value)
            }
        }
        1D / f

    }

}
