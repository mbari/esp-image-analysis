package org.mbari.esp.ia.ga

import org.mbari.esp.ia.geometry.{EuclideanDistanceFn, HausdorffDistances, LabeledDoublePoint2D}


/**
 * Weighs points by proximity to it's nearest neighbor. Internally, the distance from each point in
 * setA to each point in setB is calculated. The nearest A point to each B point is used to
 * calculate the distance.
 *
 * @param distance The cut off distance. 'A' points whose distance to the nearest 'B' point is
 *                 equal to or greater than this value are not used in the fitness calculation
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
class WeightedDistanceFitnessFn(distance: Double) extends FitnessFn {

    private[this] val distanceFn = new HausdorffDistances(new EuclideanDistanceFn)

    def apply(setA: Iterable[LabeledDoublePoint2D], setB: Iterable[LabeledDoublePoint2D]): Double = {

        val (_, nearestAtoB) = distanceFn(setA, setB)
        val totalWeight = setB.size * distance
        val f = nearestAtoB.foldLeft(0D) { (fi, pp) =>
            if (pp.value >= distance) {
                fi
            }
            else {
                fi + distance - pp.value
            }
        }

        f / totalWeight

    }

}
