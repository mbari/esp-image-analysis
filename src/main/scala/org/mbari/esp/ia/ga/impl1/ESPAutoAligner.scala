package org.mbari.esp.ia.ga.impl1

import org.slf4j.LoggerFactory
import org.mbari.esp.ia.ga.{Fitness, GADatum}
import org.mbari.esp.ia.geometry.{GALPoint, LabeledDoublePoint2D}

/**
 * Auto-aligns a GAL file with the points on an image.
 *
 * @param gaStrategy The strategey to use for the alignment
 * @param fiducialPoints A set of 3 fiducial points
 * @param galPoints All the GAL Points. This should also include the fiducial points
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
class ESPAutoAligner(val gaStrategy: GAStrategy, val fiducialPoints: Seq[LabeledDoublePoint2D],
        val galPoints: Seq[GALPoint]) {

    require(fiducialPoints.size == 3, "Exactly 3 fiducial points are required. You supplied " +
            fiducialPoints.size)
    require(galPoints.contains(fiducialPoints(0)) &&
            galPoints.contains(fiducialPoints(1)) &&
            galPoints.contains(fiducialPoints(2)),
        "The fiducial points were not found in the galPoints you supplied.")

    private[this] val log = LoggerFactory.getLogger(getClass)

    /**
     * Apply the autoaligner to a set of imagepoints
     *
     * @param imagePoints The points extracted from an ESP image.
     * @return The [[org.mbari.esp.ia.geometry.GALPoint]]s aligned to the imagePoints
     */
    def apply(imagePoints: Seq[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] = {

        // Build the datum that represents an image to run a GA on
        val datum = new GADatum(fiducialPoints, galPoints, imagePoints)

        var population = gaStrategy.intializePopulation(datum.imagePoints)
        var n = 0;
        while (n < gaStrategy.maxIterations) {
            val springGeneration = gaStrategy.springBloom(datum, population)
            val summerGeneration = population ++ springGeneration
            population = gaStrategy.winterDieOff(datum, summerGeneration)
            n += 1
            log.debug("Completed loop " + n + " of " + gaStrategy.maxIterations)
        }

        val populationFitnesses = population.map {
            ch =>
                val fitness = gaStrategy.calculateFitness(datum, ch, gaStrategy.survivorFitness)
                new Fitness(ch, fitness)
        }

        val winner = populationFitnesses.toSeq.sortBy(_.fitness).last
        log.info("Winner:\nChromosome: " + winner.chromosome + "\nFitness: " + winner.fitness)

        gaStrategy.transform(datum.galChromosome, winner.chromosome, galPoints)

    }


}
