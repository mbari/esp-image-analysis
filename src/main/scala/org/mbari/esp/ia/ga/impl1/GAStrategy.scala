package org.mbari.esp.ia.ga.impl1

import scala.collection.mutable

import org.mbari.esp.ia.ga._
import PointChromosomeExt.extendChromosome
import org.slf4j.LoggerFactory
import scala.util.Random
import org.mbari.esp.ia.geometry.LabeledDoublePoint2D

/**

 *
 * @author Brian Schlining
 * @since 2012-03-22
 */

/**
 * The strategy to use for the [[org.mbari.esp.ia.ga.impl1.ESPAutoAligner]]'s GA.
 * @param maxIterations The number of iterations of the genetic algorithm
 * @param reproductiveFitness The fitness function to apply to parents to determine the probability
 *                            that they will reproduce.
 * @param reproductiveSelection The selection function to apply to select parents for each child
 *                              chromosome
 * @param survivorFitness The fitness function that calculates the fitness of an individual to
 *                        determine if it will survive to the next generation
 * @param survivorSelection The selection function to determine if an individual will survive to the
  *                        next generation.
 * @param crossover The crossover function that combines to parents to produce a child
 * @param mutation The mutation function that alters an individuals chromosome
 * @param mutationProbability The probability that a child will be a mutant
 * @param transform The geometric transform used to map one set of points to another. Typically
 *                  we are using rigid geometric transforms but an affine transform could also be
 *                  used.
 * @param intializePopulation A function that creates the initial population of individuals.
 */
class GAStrategy(val maxIterations: Int,
        val reproductiveFitness: FitnessFn,
        val reproductiveSelection: Selection,
        val survivorFitness: FitnessFn,
        val survivorSelection: Selection,
        val crossover: Crossover,
        val mutation: Mutation,
        val mutationProbability: Double,
        val transform: Transform,
        val intializePopulation: Iterable[LabeledDoublePoint2D] => Iterable[Chromosome]) {

    require(mutationProbability >= 0 && mutationProbability <= 1, "mutationProbability must be " +
            "between 0 and 1. You supplied " + mutationProbability)
    require(reproductiveSelection.survivorCount == 2, "reproductiveSelection requires a survivor " +
            "count of 2 (a mother and a father). You supplied " + reproductiveSelection.survivorCount)

    private[this] val log = LoggerFactory.getLogger(getClass)

    private[this] val randomGenerator = new Random

    def springBloom(datum: GADatum, parentGeneration: Iterable[Chromosome]): Iterable[Chromosome] = {
        val populationSize = survivorSelection.survivorCount
        val childGeneration = new mutable.ArrayBuffer[Chromosome]
        val maxLoops = populationSize * 4
        var n = 0
        val parentGenerationFitnesses = parentGeneration.par.map(ch =>
            new Fitness(ch, calculateFitness(datum, ch, reproductiveFitness))).seq

        while (n < maxLoops && childGeneration.size < populationSize) {
            val parents = reproductiveSelection(parentGenerationFitnesses)
            if (parents.size == 2) {
                val zygote = crossover(parents.head.chromosome, parents.last.chromosome)
                val child = if (randomGenerator.nextDouble() <= mutationProbability)
                    mutation(zygote)
                else zygote

                childGeneration += child
                n += 1
            }


        }
        childGeneration
    }

    def winterDieOff(datum: GADatum, population: Iterable[Chromosome]): Iterable[Chromosome] = {

        val populationFitnesses = population.par.map(ch =>
            new Fitness(ch, calculateFitness(datum, ch, survivorFitness))).seq

        val survivors = survivorSelection(populationFitnesses)

        survivors.map(_.chromosome).toSet

    }

    def calculateFitness(datum: GADatum, imageChromosome: Chromosome, fitnessFn: FitnessFn): Double = {
        try {
            val imagePoints = imageChromosome.points.toSet
            if (imagePoints.size == 3) {
                val transformedGalPoints = transform(datum.galChromosome, imageChromosome, datum.galPoints)
                fitnessFn(transformedGalPoints, datum.imagePoints)
            }
            else {
                0
            }
        }
        catch {
            case e: Exception => {
                log.debug("Fitness calculation failed", e)
                0
            }
        }
    }

}
