package org.mbari.esp.ia.ga.impl1

import scala.util.Random
import scala.math._
import org.mbari.esp.ia.geometry.{LabeledIntPoint2D, LabeledDoublePoint2D}
import org.mbari.esp.ia.ga._

/**
 * Factory that wires together the parts of the [[org.mbari.esp.ia.ga.impl1.GAStrategy]]
 * @author Brian Schlining
 * @since 2012-03-26
 */
object RigidGAStrategyFactory {

    private[this] val randomGenerator = new Random

    /**
     *
     * @param maxIterations The number of iterations of the genetic algorithm
     * @param populationSize The population size for each generation
     * @param mutationProbability The probability that a child chromosome will be a mutant. Mutation
     *                            is important to prevent convergence on an incorrect solution.
     * @param imageWidth The width of the image in pixels
     * @param imageHeight The height of the image in pixels
     * @param imagePoints The ''spots'' extracted from the image
     * @return The GA strategy
     */
    def apply(maxIterations: Int, populationSize: Int, mutationProbability: Double,
            imageWidth: Int, imageHeight: Int,
            imagePoints: Seq[LabeledIntPoint2D]): GAStrategy = {

        //val reproductiveFitness = new WeightedDistanceFitnessFn(7)
        val reproductiveFitness = new FitnessFn {
            def apply(v1: Iterable[LabeledDoublePoint2D], v2: Iterable[LabeledDoublePoint2D]): Double = 1
        }

        // --- Reproductive Selection
        val reproductiveSelectionFn = new RouletteWheelSelection(2)

        // --- Survivor FitnessFn
        //   1) Are points in image bounds
        //   2) Do the distance between points seem reasonable
        //   3) Evaluate fitness
        val survivorFitness = new FitnessFnChain(
            new ImageBoundsFitnessFn(imageWidth, imageHeight),
            new BestDistanceFitnessFn(3),
            new WeightedDistanceFitnessFn(7))

        // --- Survivor Selection.
        //   1) Skim the fittest
        //   2) Roulette wheel select from the remainders
        val survivorSelectionFn = new Selection {

            val creamCount = round(populationSize * 0.25).toInt
            val theRestCount = populationSize - creamCount

            val creamSelection = new SkimBestIndividualsSelection(creamCount)
            val randomSelection = new RouletteWheelSelection(theRestCount)

            def apply(v1: Iterable[Fitness]): Iterable[Fitness] = {
                val cream = creamSelection(v1).toSeq
                val theRest = v1.toSeq.diff(cream)
                cream ++ randomSelection(theRest)
            }

            def survivorCount: Int = populationSize
        }

        // --- Mutation.
        //   We'll try a mix. 50% noise, 50% point pool
        val mutationFn = new Mutation {
            val addNoise = new AddNoiseMutation(10)
            val pointPool = new PointPoolMutation(imagePoints)

            def apply(v1: Chromosome): Chromosome = {
                val n = randomGenerator.nextInt(100)
                if (n < 50) {
                    addNoise(v1)
                }
                else {
                    pointPool(v1)
                }
            }
        }

        // --- Initialize population. Just a random selector here.
        def initPopulation(imgPts: Iterable[LabeledDoublePoint2D]): Iterable[Chromosome] = {
            val n = imagePoints.size
            val pts = imgPts.toSeq
            val buf = for (i <- 0 until populationSize) yield {
                PointChromosome(
                    pts(randomGenerator.nextInt(n)).toInt,
                    pts(randomGenerator.nextInt(n)).toInt,
                    pts(randomGenerator.nextInt(n)).toInt)
            }
            buf.toIterable
        }

        new GAStrategy(maxIterations,
            reproductiveFitness,
            reproductiveSelectionFn,
            survivorFitness,
            survivorSelectionFn,
            SinglePointCrossover,
            mutationFn,
            mutationProbability,
            RigidTransform,
            initPopulation)

    }

}


