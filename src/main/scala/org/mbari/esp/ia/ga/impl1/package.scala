package org.mbari.esp.ia.ga

/**
 * This is a basic implementation of the auto-aligning GA used for ESP image analysis. It can be
 * used as a basis for deriving other GA strategy's. This implementation is not particularly
 * efficient as it: a) recalculates fitness for all chromosomes on every iteration and b) has no
 * mechanism for exiting early (it only completes when all iterations are done)
 */
package object impl1 {

}