package org.mbari.esp.ia.ga.impl2

import java.net.URL
import org.mbari.esp.ia.services.{ToolBox, GeneArrayAnalysisService}
import java.io.{FileWriter, BufferedWriter, File}

/**
 *
 * @author Brian Schlining
 * @since 2012-05-10
 */

object ConsoleApp {

    def main(args: Array[String]) {

        // URL to GAL file
        val galUrl = {
            val name = args(0)
            if (name.startsWith("http")) {
                new URL(name)
            }
            else {
                new File(name).toURI.toURL
            }
        }

        // URL to image, name of image w/o extension
        val (imageUrl, filebase) = {
            val name = args(1)

            val url = if (name.startsWith("http")) {
                new URL(name)
            }
            else {
                new File(name).toURI.toURL
            }
            // Get the image name w/o an extension
            val filebase = url.getPath.split('/').last.split('.').head
            (url, filebase)
        }

        //  key to fiducial spots in GAL
        val fiducialKey = args(2)

        // Directory to write results to
        val targetDir = if (args.size < 4) {
            new File(System.getProperty("user.dir"))
        }
        else {
            val dir = new File(args(3))
            if (!dir.exists()) {
                dir.mkdirs()
            }
            if (!dir.isDirectory) {
                require(dir.isDirectory, dir.getCanonicalPath + " exists and is not a directory")
            }
            dir
        }

        val analyzer = new FastEspImageAnalyzer(20, 100, 0.25, 5, 3)

        val results = analyzer(galUrl, imageUrl, fiducialKey)
        val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
            results.intensities, results.blockSize)
        ToolBox.imageIOService.write(resultImage, new File(targetDir, filebase + "-test.png"))

        val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
            results.intensities, results.spotRadius, results.blockSize)
        val writer = new BufferedWriter(new FileWriter(new File(targetDir, filebase + "-test.txt")))
        writer.write(resultText)
        writer.close()

    }

}
