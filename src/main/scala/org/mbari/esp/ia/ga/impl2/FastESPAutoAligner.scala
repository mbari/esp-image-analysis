package org.mbari.esp.ia.ga.impl2

import org.slf4j.LoggerFactory
import org.mbari.esp.ia.geometry.{Point2D, LabeledDoublePoint2D}
import org.mbari.esp.ia.ga.{PointChromosome, PointChromosomeExt, Fitness, GADatum}


/**
 * Auto-aligns a GAL file with the points on an image.
 *
 * @param gaStrategy The strategey to use for the alignment
 * @param fiducialPoints A set of 3 fiducial points
 * @param galPoints All the GAL Points. This should also include the fiducial points
 * @param exitOnSeasonsSurvived The fast algorithm will exit early if the winning chromosome has
 *                              been the winner for this many iterations of the GA. Default = 10
 *
 * @author Brian Schlining
 * @since 2012-03-22
 */
class FastESPAutoAligner(val gaStrategy: FastGAStrategy, val fiducialPoints: Seq[LabeledDoublePoint2D],
        val galPoints: Seq[LabeledDoublePoint2D], val exitOnSeasonsSurvived: Int = 10) {

    require(fiducialPoints.size == 3, "Exactly 3 fiducial points are required. You supplied " +
            fiducialPoints.size)
    require(galPoints.contains(fiducialPoints(0)) &&
            galPoints.contains(fiducialPoints(1)) &&
            galPoints.contains(fiducialPoints(2)),
        "The fiducial points were not found in the galPoints you supplied.")

    private[this] val log = LoggerFactory.getLogger(getClass)

    /**
     * Apply the GA
     * @param imagePoints The ESP image points to align the GAL to
     * @return The aligned gal points.
     */
    def apply(imagePoints: Seq[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] = {

        // Build the datum that represents an image to run a GA on
        val datum = new GADatum(fiducialPoints, galPoints, imagePoints)

        var population = gaStrategy.intializePopulation(datum.imagePoints).map {
            individual =>
                new Fitness(individual, gaStrategy.calculateFitness(datum, individual, gaStrategy.survivorFitness))
        }

        var n = 0;
        val bogusPoint = Point2D(0, 0)
        var winner: Fitness = new Fitness(PointChromosome(bogusPoint, bogusPoint, bogusPoint), 0)
        var seasonsSurvived: Int = 0
        while (n < gaStrategy.maxIterations) {
            val springGeneration = gaStrategy.springBloom(datum, population)
            val summerGeneration = population ++ springGeneration
            population = gaStrategy.winterDieOff(datum, summerGeneration)
            n += 1
            log.debug("Completed loop " + n + " of " + gaStrategy.maxIterations)

            // -- Break out of loop is winner has survived for 5 seasons
            val newWinner = population.toSeq.sortBy(_.fitness).last
            val inc = if (newWinner.chromosome == winner.chromosome) 1 else 0
            seasonsSurvived += inc
            winner = newWinner
            if (seasonsSurvived > exitOnSeasonsSurvived) {
                n = gaStrategy.maxIterations
            }
        }

        //val winner = population.toSeq.sortBy(_.fitness).last
        log.info("Winner:\nChromosome: " + winner.chromosome + "\nFitness: " + winner.fitness)

        gaStrategy.transform(datum.galChromosome, winner.chromosome, galPoints)

    }


}


