package org.mbari.esp.ia.ga.impl2

import java.net.URL
import org.mbari.esp.ia.ga.AnalysisResult
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}
import org.mbari.esp.ia.geometry.GALPoint
import org.slf4j.LoggerFactory
import org.mbari.esp.ia.imglib.{MultiPassRegionsExtractorFn, SpotRegionsExtractorFn}


/**
 * Apply a genetic algorithm to align points form a GAL file with those on an image.
 *
 * @author Brian Schlining
 * @since 2012-04-12
 *
 * @param maxIterations The number of iterations of the genetic algorithm
 * @param populationSize The population size for each generation
 * @param mutationProbability The probability that a child chromosome will be a mutant. Mutation
 *                            is important to prevent convergence on an incorrect solution.
 * @param spotRadius Radius from the aligned gal points to search for the brightest block of pixels
 * @param blockSize The brightest blockSize by blockSize square withing ''spotRadiuse'' will be
 *                  extracted from the image for each aligned GAL point
 */
class FastEspImageAnalyzer(maxIterations: Int,
        populationSize: Int,
        mutationProbability: Double,
        spotRadius: Float,
        blockSize: Int) extends ((URL, URL, String) => AnalysisResult) {

    private[this] val log = LoggerFactory.getLogger(getClass)

    /**
     * Apply the image analyzer to an image
     * @param galUrl The URL to the GAL file to use for the analysis
     * @param imageUrl The image to analyze
     * @param fiducialKey The id of the ''sourceWell'' (e.g. "K-1") for the fiducial spots in
     *                    the GAL file
     * @return The winner of the analysis
     */
    def apply(galUrl: URL, imageUrl: URL, fiducialKey: String): AnalysisResult = {

        // --- 1) Extract GAL info
        val galIO = ToolBox.geneArrayIOService
        val galPoints = GeneArrayAnalysisService.filterSingleSpot(galIO.read(galUrl), 12500)
        val fiducialPoints = {
            val tmpPts = galPoints.filter(_.sourceWell == fiducialKey)
            if (tmpPts.size == 3) {
                tmpPts
            }
            else if (tmpPts.size > 3) {
                tmpPts.take(3)
            }
            else if (tmpPts.size == 2) {
                log.warn("Only 2 fiducial points were found in the GALFile with the id of " +
                    fiducialKey + ". We expected at least 3. We'll try to work with it though.")
                tmpPts :+ tmpPts.head // Reuse the 1st point so we have 3 points in our seq
            }
            else {
                throw new IllegalArgumentException("The GAL file at " + galUrl + " needs to have at" +
                        "least 2 points labeled '" + fiducialKey + "' but we found " + tmpPts.size)
            }
        }

        // --- 2) Read image
        val imageIO = ToolBox.imageIOService
        val imagePlus = imageIO.read(imageUrl)
        //val spotRegions = SpotRegionsExtractorFn(imagePlus.getProcessor).filter(_.eccentricity()._1 < 3)
        val spotRegions = MultiPassRegionsExtractorFn(imagePlus.getProcessor)
        val imagePoints = spotRegions.map(_.centroid().toInt).toSeq

        // --- 3) Set up GA and run it
        val gaStrategy = RigidFastGAStrategyFactory(maxIterations, populationSize, mutationProbability,
            imagePlus.getWidth, imagePlus.getHeight, imagePoints)
        val aligner = new FastESPAutoAligner(gaStrategy, fiducialPoints, galPoints)
        val alignedLabeledGalPoints = aligner(imagePoints.map(_.toDouble))

        // Convert the LabeledIntPoint2D back to the correct, but aligned, GALPoint
        val alignedGalPoints = alignedLabeledGalPoints.map { lp =>
            val gp = galPoints.filter(_.spotNumber == lp.label).head
            new GALPoint(gp, lp.x, lp.y)
        }

        // Extract the intensity information from the image
        val results = GeneArrayAnalysisService.extractIntensities(imagePlus.getProcessor, alignedGalPoints,
            spotRadius, blockSize)

        new AnalysisResult(galUrl, imageUrl, imagePlus.getProcessor, results, spotRadius, blockSize)

    }
}

