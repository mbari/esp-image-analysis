package org.mbari.esp.ia.ga.impl2

import scala.collection.mutable
import util.Random
import org.slf4j.LoggerFactory
import org.mbari.esp.ia.geometry.LabeledDoublePoint2D
import org.mbari.esp.ia.ga.PointChromosomeExt.extendChromosome
import org.mbari.esp.ia.ga._


/**
 * The strategy to use for the [[org.mbari.esp.ia.ga.impl1.ESPAutoAligner]]'s GA. This strategy
 * differs from the [[org.mbari.esp.ia.ga.impl1.GAStrategy]] in one way. In this strategy, the
 * ''survivorSelecction'' function is also used to determine reproductive fitness.
 *
 * @param maxIterations The number of iterations of the genetic algorithm
 * @param survivorFitness The fitness function that calculates the fitness of an individual to
 *                        determine if it will survive to the next generation
 * @param survivorSelection The selection function to determine if an individual will survive to the
  *                        next generation.
 * @param crossover The crossover function that combines to parents to produce a child
 * @param mutation The mutation function that alters an individuals chromosome
 * @param mutationProbability The probability that a child will be a mutant
 * @param transform The geometric transform used to map one set of points to another. Typically
 *                  we are using rigid geometric transforms but an affine transform could also be
 *                  used.
 * @param intializePopulation A function that creates the initial population of individuals.
 */
class FastGAStrategy(val maxIterations: Int,
        val survivorFitness: FitnessFn,
        val survivorSelection: Selection,
        val crossover: Crossover,
        val mutation: Mutation,
        val mutationProbability: Double,
        val transform: Transform,
        val intializePopulation: Iterable[LabeledDoublePoint2D] => Iterable[Chromosome]) {

    require(mutationProbability >= 0 && mutationProbability <= 1, "mutationProbability must be " +
            "between 0 and 1. You supplied " + mutationProbability)

    private[this] val log = LoggerFactory.getLogger(getClass)

    private[this] val randomGenerator = new Random

    // --- Reproductive Selection
    private[this] val reproductiveSelection = new RouletteWheelSelection(2)

    def springBloom(datum: GADatum, parentGeneration: Iterable[Fitness]): Iterable[Fitness] = {
        val populationSize = survivorSelection.survivorCount
        val childGeneration = new mutable.ArrayBuffer[Fitness]
        val maxLoops = populationSize * 4
        var n = 0

        while (n < maxLoops && childGeneration.size < populationSize) {
            val parents = reproductiveSelection(parentGeneration)
            if (parents.size == 2) {
                val zygote = crossover(parents.head.chromosome, parents.last.chromosome)
                val child = if (randomGenerator.nextDouble() <= mutationProbability)
                    mutation(zygote)
                else zygote

                childGeneration += new Fitness(child, calculateFitness(datum, child, survivorFitness))
                n += 1
            }


        }
        childGeneration
    }

    def winterDieOff(datum: GADatum, population: Iterable[Fitness]): Iterable[Fitness] =
        survivorSelection(population)


    def calculateFitness(datum: GADatum, imageChromosome: Chromosome, fitnessFn: FitnessFn): Double = {
        try {
            val imagePoints = imageChromosome.points.toSet
            if (imagePoints.size == 3) {
                val transformedGalPoints = transform(datum.galChromosome, imageChromosome, datum.galPoints)
                fitnessFn(transformedGalPoints, datum.imagePoints)
            }
            else {
                0
            }
        }
        catch {
            case e: Exception => {
                log.debug("Fitness calculation failed", e)
                0
            }
        }
    }

}

