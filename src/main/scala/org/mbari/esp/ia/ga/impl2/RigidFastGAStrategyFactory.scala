package org.mbari.esp.ia.ga.impl2

import scala.util.Random
import scala.math._
import org.mbari.esp.ia.ga._
import org.mbari.esp.ia.Math
import org.mbari.esp.ia.geometry.{LabeledDoublePoint2D, PointDistancesFn, LabeledIntPoint2D}

/**
 * Factory that wires together the [[org.mbari.esp.ia.ga.impl2.FastGAStrategy]]
 * @author Brian Schlining
 * @since 2012-03-31
 */
object RigidFastGAStrategyFactory {

    private[this] val randomGenerator = new Random

    /**
     *
     * @param maxIterations The number of iterations of the genetic algorithm
     * @param populationSize The population size for each generation
     * @param mutationProbability The probability that a child chromosome will be a mutant. Mutation
     *                            is important to prevent convergence on an incorrect solution.
     * @param imageWidth The width of the image in pixels
     * @param imageHeight The height of the image in pixels
     * @param imagePoints The ''spots'' extracted from the image
     * @return The GA strategy
     */
    def apply(maxIterations: Int, populationSize: Int, mutationProbability: Double,
            imageWidth: Int, imageHeight: Int,
            imagePoints: Seq[LabeledIntPoint2D]): FastGAStrategy = {

        val numIterations = {
            val n = imagePoints.size
            if(n < 2000 && Math.combinations(n, 2) < populationSize * maxIterations) 1
                else maxIterations
        }


        val survivorFitness = {
            val distancePairs = PointDistancesFn(imagePoints.map(_.toDouble))
            val distances = distancePairs.map(_.value).filter(_ > 0)
            val maxDistance = distances.max
            val minDistance = distances.min
            new FitnessFnChain(
                new ImageBoundsFitnessFn(imageWidth, imageHeight),
                new DistanceCutoffFitnessFn(minDistance, maxDistance, 3),
                new WeightedDistanceFitnessFn(7))
                //new SumOfSquaresDistanceFitnessFn(7))
        }

        val survivorSelection = new Selection {

            val creamCount = round(populationSize * 0.25).toInt
            val theRestCount = populationSize - creamCount

            val creamSelection = new SkimBestIndividualsSelection(creamCount)
            val randomSelection = new RouletteWheelSelection(theRestCount)

            def apply(v1: Iterable[Fitness]): Iterable[Fitness] = {
                val cream = creamSelection(v1).toSeq
                val theRest = v1.toSeq.diff(cream)
                cream ++ randomSelection(theRest)
            }

            def survivorCount: Int = populationSize
        }

        // --- Mutation.
        //   We'll try a mix. 50% noise, 50% point pool
        val mutation = new Mutation {
            val addNoise = new AddNoiseMutation(10)
            val pointPool = new PointPoolMutation(imagePoints)

            def apply(v1: Chromosome): Chromosome = {
                val n = randomGenerator.nextInt(100)
                if (n < 50) {
                    addNoise(v1)
                }
                else {
                    pointPool(v1)
                }
            }
        }

        var maxIterationsNew = 0

        def initPopulation(imgPts: Iterable[LabeledDoublePoint2D]): Iterable[Chromosome] = {

            // Shortcut: If the number of possible combinations is less than the total number
            // of calculations needed to run the GA, then we just create a pool of chromosomes
            // representing all possible combinations of points and run that once
            if(numIterations == 1) {
                CreateChromosomesFn(imgPts)
            }
            else {
                val n = imagePoints.size
                val pts = imgPts.toSeq
                val buf = for (i <- 0 until populationSize) yield {
                    PointChromosome(
                        pts(randomGenerator.nextInt(n)).toInt,
                        pts(randomGenerator.nextInt(n)).toInt,
                        pts(randomGenerator.nextInt(n)).toInt)
                }
                buf.toIterable
            }

        }


        new FastGAStrategy(numIterations,
            survivorFitness,
            survivorSelection,
            SinglePointCrossover,
            mutation,
            mutationProbability,
            RigidTransform,
            initPopulation)

    }
}
