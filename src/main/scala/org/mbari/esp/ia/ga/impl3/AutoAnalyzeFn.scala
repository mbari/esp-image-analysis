package org.mbari.esp.ia.ga.impl3


import java.util.Date
import java.text.SimpleDateFormat
import org.mbari.esp.ia.services.{ToolBox, GeneArrayAnalysisService}
import java.io.{FileWriter, BufferedWriter, File}


/**
 * Adapter function between the Scala stuff and the Java UI. Does the same work as
 * [[org.mbari.esp.ia.ga.impl3.ConsoleApp]]
 *
 * @author Brian Schlining
 * @since 2012-05-25
 */

object AutoAnalyzeFn {

    private[this] val df = new SimpleDateFormat("yyyyMMddHHmmss")

    def apply(params: AutoAnalyzeParameters) {
        // Extract Image Name
        val filename =  params.imageFile.getCanonicalPath.split(File.separatorChar).last.split('.').head +
                "-" + df.format(new Date())

        val analyzer = new ESPImageAnalyzer(params.spotRadius, params.blockSize)
        val results = analyzer(params.galFile.toURI.toURL,
            params.imageFile.toURI.toURL,
            params.fiducialKey)
        val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
            results.intensities, results.blockSize)
        ToolBox.imageIOService.write(resultImage, new File(params.targetDirectory, filename + ".png"))

        val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
            results.intensities, results.spotRadius, results.blockSize)
        val writer = new BufferedWriter(new FileWriter(new File(params.targetDirectory, filename + ".txt")))
        writer.write(resultText)
        writer.close()

    }

}
