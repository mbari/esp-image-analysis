package org.mbari.esp.ia.ga.impl3

import java.io.File

/**
 *
 * @author Brian Schlining
 * @since 2012-05-25
 */

case class AutoAnalyzeParameters(galFile: File, fiducialKey: String, imageFile: File,
        blockSize: Int, spotRadius: Int, targetDirectory: File)
