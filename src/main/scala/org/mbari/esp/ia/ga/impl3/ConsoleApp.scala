package org.mbari.esp.ia.ga.impl3

import java.io.{FileWriter, BufferedWriter, File}
import java.net.URL
import java.text.SimpleDateFormat
import java.util.Date
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}



class ConsoleApp(galUrl: URL, fiducialKey: String, imageUrl: URL, 
        targetDir: File) {

    private[this] val df = new SimpleDateFormat("yyyyMMddHHmmss")

    def run {
        // Extract Image Name
        val filename =  imageUrl.getPath.split('/').last.split('.').head + 
                "-" + df.format(new Date())

        val analyzer = new ESPImageAnalyzer(5, 3)
        val results = analyzer(galUrl, imageUrl, fiducialKey)
        val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
            results.intensities, results.blockSize)
        ToolBox.imageIOService.write(resultImage, new File(targetDir, filename + ".png"))

        val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
                results.intensities, results.spotRadius, results.blockSize)
        val writer = new BufferedWriter(new FileWriter(new File(targetDir, filename + ".txt")))
        writer.write(resultText)
        writer.close()
   }

}

object ConsoleApp {

    def main(args: Array[String]): Unit = {

        if (args.size != 4) {
            println("""
                | Usage:
                |   ConsoleApp <galfile> <fiducialkey> <imagefile> <targetdir>
                |
                | Inputs:
                |   galfile = The gal file to use for the analysis
                |   fiducialkey = the spot key for the fidudial spots (e.g. K-3)
                |   imagefile = The image file to analyze 
                |   targetdir = the directory to write results into
                """.stripMargin)
        }

        val galUrl = new File(args(0)).toURI.toURL
        val fiducialKey = args(1)
        val imageUrl = new File(args(2)).toURI.toURL
        val targetDir = new File(args(3))

        new ConsoleApp(galUrl, fiducialKey, imageUrl, targetDir).run      
    }
    
}

/*
// Sample: I ran this in the Console
import java.io.File
import org.mbari.esp.ia.ga.impl3._

val galFile = new File("/Users/brian/workspace/esp-image-analysis/src/site/samples/espneo/110220hab5wcomp_2012-02-17_105714_Run.txt")
val basedir = new File("/Volumes/ESP/station/ESPneo.dyndns.org/esp")
val imageNames = basedir.list.filter(s => s.startsWith("pcr") && s.endsWith(".tif"))
val targetDir= new File("/Users/brian/Desktop/temp/esp")

val galUrl = galFile.toURI.toURL

imageNames.map(new File(basedir, _).toURI.toURL).foreach(new ConsoleApp(galUrl, "K-1", _, targetDir).run)

*/