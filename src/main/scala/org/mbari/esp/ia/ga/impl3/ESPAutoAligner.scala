package org.mbari.esp.ia.ga.impl3

import org.slf4j.LoggerFactory
import org.mbari.esp.ia.geometry.{PointDistancesFn, LabeledDoublePoint2D}
import org.mbari.esp.ia.ga._
import org.mbari.esp.ia.ga.PointChromosomeExt.extendChromosome
import ij.process.ImageProcessor
import org.mbari.esp.ia.services.ToolBox
import java.awt.image.BufferedImage
import org.mbari.esp.ia.imglib.ToRGBFn
import java.awt.geom.Ellipse2D
import java.awt.{Color, Graphics2D}
import java.io.File

/**
 *
 * @author Brian Schlining
 * @since 2012-05-16
 */

class ESPAutoAligner(val fiducialPoints: Seq[LabeledDoublePoint2D],
                     val galPoints: Seq[LabeledDoublePoint2D],
                     val transform: Transform = RigidTransform) {

  require(fiducialPoints.size == 3, "Exactly 3 fiducial points are required. You supplied " +
      fiducialPoints.size)
  require(galPoints.contains(fiducialPoints(0)) &&
      galPoints.contains(fiducialPoints(1)) &&
      galPoints.contains(fiducialPoints(2)),
    "The fiducial points were not found in the galPoints you supplied.")

  private[this] val log = LoggerFactory.getLogger(getClass)

  def apply(imageWidth: Int,
            imageHeight: Int,
            fiducialImagePoints: Seq[LabeledDoublePoint2D],
            allImagePoints: Seq[LabeledDoublePoint2D],
            image: Option[ImageProcessor] = None):
  Seq[LabeledDoublePoint2D] = {

    val datum = new GADatum(fiducialPoints, galPoints, allImagePoints)

    // TODO: /test13 contains a Gal that defines points outside of the image bounds.
    // FIXME: I commented this out to handle that case. (brian 2015-08-06)
    //        val survivorFitness = new FitnessFnChain(
    //            new ImageBoundsFitnessFn(imageWidth, imageHeight),
    //            new WeightedDistanceFitnessFn(7))

    val survivorFitness = new WeightedDistanceFitnessFn(7)

    val chromosomes = CreateChromosomesFn(fiducialImagePoints)
    val fitnesses = chromosomes.map(c => new Fitness(c, calculateFitness(datum, c, survivorFitness, image)))

    val winner = fitnesses.toSeq.sortBy(_.fitness).last
    log.info("Winner:\nChromosome: " + winner.chromosome + "\nFitness: " + winner.fitness)

    transform(datum.galChromosome, winner.chromosome, galPoints)
  }

  private def calculateFitness(datum: GADatum, imageChromosome: Chromosome, fitnessFn: FitnessFn,
                               image: Option[ImageProcessor] = None): Double = {

    try {
      val imagePoints = imageChromosome.points.toSet
      if (imagePoints.size == 3) {
        val transformedGalPoints = transform(datum.galChromosome, imageChromosome, datum.galPoints)
        image.foreach { img =>
          val imageIO = ToolBox.imageIOService
          // --- Draw results
          val colorImage: BufferedImage = ToRGBFn(img).getBufferedImage
          val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]
          val results = transformedGalPoints.map(p =>
            new Ellipse2D.Double(p.x - 2, p.y - 2, 4, 4))
          g2.setPaint(Color.RED)
          results.foreach(g2.draw(_))
          imageIO.write(colorImage, new File("target", getClass.getSimpleName + "-" +
              System.currentTimeMillis() + ".png"))
        }
        fitnessFn(transformedGalPoints, datum.imagePoints)
      }
      else {
        0
      }
    }
    catch {
      case e: Exception => {
        log.debug("Fitness calculation failed", e)
        0
      }
    }
  }

}
