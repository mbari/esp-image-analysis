package org.mbari.esp.ia.ga.impl3

import org.mbari.esp.ia.ga.AnalysisResult
import java.net.URL
import org.slf4j.LoggerFactory
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}
import org.mbari.esp.ia.imglib.{MultiPassRegionsExtractorFn, BrightSpotExtractorFn}
import org.mbari.esp.ia.geometry.GALPoint

/**
 *
 * @author Brian Schlining
 * @since 2012-05-16
 */

class ESPImageAnalyzer(spotRadius: Float, blockSize: Int)
        extends ((URL, URL, String) => AnalysisResult) {

    private[this] val log = LoggerFactory.getLogger(getClass)

    def apply(galUrl: URL, imageUrl: URL, fiducialKey: String): AnalysisResult = {

        // --- 1) Extract GAL info
        val galIO = ToolBox.geneArrayIOService
        val galPoints = GeneArrayAnalysisService.filterSingleSpot(galIO.read(galUrl), 12500)
        val fiducialPoints = {
            val tmpPts = galPoints.filter(_.sourceWell == fiducialKey)
            if (tmpPts.size == 3) {
                tmpPts
            }
            else if (tmpPts.size > 3) {
                tmpPts.take(3)
            }
            else if (tmpPts.size == 2) {
                log.warn("Only 2 fiducial points were found in the GALFile with the id of " +
                        fiducialKey + ". We expected at least 3. We'll try to work with it though.")
                tmpPts :+ tmpPts.head // Reuse the 1st point so we have 3 points in our seq
            }
            else {
                throw new IllegalArgumentException("The GAL file at " + galUrl + " needs to have at" +
                        "least 2 points labeled '" + fiducialKey + "' but we found " + tmpPts.size)
            }
        }

        // --- 2) Read image
        val imageIO = ToolBox.imageIOService
        val imagePlus = imageIO.read(imageUrl)

        val fiducialImagePoints = BrightSpotExtractorFn(imagePlus.getProcessor).map(_.centroid()).toSeq
        val allImagePoints = MultiPassRegionsExtractorFn(imagePlus.getProcessor).map(_.centroid()).toSeq
        val aligner = new ESPAutoAligner(fiducialPoints, galPoints)
        val alignedLabeledGalPoints = aligner(imagePlus.getWidth, imagePlus.getHeight, fiducialImagePoints,
                allImagePoints)

        // Convert the LabeledIntPoint2D back to the correct, but aligned, GALPoint
        val alignedGalPoints = alignedLabeledGalPoints.map { lp =>
            val gp = galPoints.filter(_.spotNumber == lp.label).head
            new GALPoint(gp, lp.x, lp.y)
        }

        // Extract the intensity information from the image
        val results = GeneArrayAnalysisService.extractIntensities(imagePlus.getProcessor, alignedGalPoints,
            spotRadius, blockSize)

        new AnalysisResult(galUrl, imageUrl, imagePlus.getProcessor, results, spotRadius, blockSize)

    }


}
