package org.mbari.esp.ia

import geometry.{LabeledDoublePoint2D}


/**
 * This is the genetic algorithm (GA) package focused on aligning GAL points with points extracted
 * from an ESP image. Some classes in here are generic enough to be applied to other GA problems
 * (e.g. Chromosome).
 */
package object ga {

    /**
     * Alias for a function that takes 2 parents and produces a child chromosome
     */
    type Crossover = (Chromosome, Chromosome) => Chromosome

    /**
     * Alias for a funciton that take a chromsome and produces a new mutated chromosome
     */
    type Mutation = (Chromosome) => Chromosome

    /**
     * Evaluates the ''fitness'' of 2 sets of points. The first argument to this
     * function is the transformed galPoints, the 2nd is the actual image points. Fitness values
     * returned should be between 0 and 1. Higher fitness is better.
     */
    type FitnessFn = (Iterable[LabeledDoublePoint2D], Iterable[LabeledDoublePoint2D]) => Double

    /**
     * Calculate a spatial transform by fitting the galChromsome to the imgChromosome. Then apply the
     * transform to the list of galPoints
     *
     * (galChromsome, imgChromsome, galPoints)
     */
    type Transform = (Chromosome, Chromosome, Iterable[LabeledDoublePoint2D]) => Seq[LabeledDoublePoint2D]
}