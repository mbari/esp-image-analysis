package org.mbari.esp.ia.geometry

/**
 * Calculates the distance between 2 points
 *
 * @author Brian Schlining
 * @since 2012-03-20
 */
class EuclideanDistanceFn[A : Numeric] extends ((Point2D[A], Point2D[A]) => Double) {
    def apply(p0: Point2D[A], p1: Point2D[A]): Double = p0.distance(p1)
}
