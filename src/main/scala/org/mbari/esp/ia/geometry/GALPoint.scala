package org.mbari.esp.ia.geometry

import java.lang.{Double => JDouble}
import org.mbari.geometry.{Point2D => MPoint2D}


/**
 * Bean class that represents information about a single GAL (Gene Array Layout) point
 * @param x
 * @param y
 * @param sourceNumber
 * @param sourceWell
 * @param spotNumber
 * @param spotVolume
 * @param status
 * @param subNumber
 * @param tipNumber
 * @author Brian Schlining
 * @since 2012-03-06
 */
class GALPoint(x: Double, y: Double,
        val sourceNumber: Int,
        val sourceWell: String,
        val spotNumber: Int,
        val spotVolume: Double,
        val status: String,
        val subNumber: Int,
        val tipNumber: Int) extends LabeledDoublePoint2D(x, y, spotNumber) {

    /**
     * Copy constructor. Copies all values from the original GALPoint except for the x and y
     * coordinate
     *
     * @param p The original Point
     * @param x The new x coordinate
     * @param y The new y coordinate
     * @return A new GALPoint
     */
    def this(p: GALPoint, x: Double, y: Double) = this(x, y, p.sourceNumber, p.sourceWell,
        p.spotNumber, p.spotVolume, p.status, p.subNumber, p.tipNumber)


    /**
     * GALPoint represented as a simple point object. The points label is the galPoints
     * ''spotNumber''
     */
    @deprecated(" A GALPoint already is a LabeledDoublePoint2D, no conversion is needed")
    val toLabeledPoint2D: LabeledDoublePoint2D = this

    lazy val toMbariPoint2D: MPoint2D[JDouble] = new MPoint2D[JDouble](x, y)

}

object GALPoint {

  def apply(p: GALPoint, sourceWell: String) = {
    new GALPoint(p.x, p.y, p.sourceNumber, sourceWell, p.spotNumber, p.spotNumber, p.status,
      p.subNumber, p.tipNumber)
  }

}


