package org.mbari.esp.ia.geometry

import scala.math.ceil
import scala.math.Pi
import scala.math.cos
import scala.math.sin
import org.mbari.math.{Statlib, Matlib}
import Jama.Matrix
import java.util.Collection
import scala.collection.JavaConverters._
/**
 *
 * @author Brian Schlining
 * @since 2012-03-07
 */

object Points {

  /**
   * Get the probable most common distance between nearest neighbors.
   *
   * @param points The list of points
   * @param binSize the best distance is calculated via a histogram. The binSize indicates
   *  the resolution of the histogram. (i.e. the width of the bins used in the histogram)
   * @return The most frequent distance (using ''binSize'' pixel bins) between each point
   *  and it's nearest neighbor
   */
  def bestDistance(points: Iterable[LabeledDoublePoint2D], binSize: Double): Double = {
    // -- Calculate distances and get the max distance
    val distances = PointDistancesFn(points).filter(_.value > 0)
    val max = distances.maxBy(_.value).value

    // -- Get a set of the unique labels for the points
    val labels = (points.map(_.label)).toSet

    // -- For each label, get the nearest neightbor
    val nearestNeighbors = labels.flatMap { v =>
      val matching = distances.filter(p => (p.p0.label == v || p.p1.label == v) &&
          p.p0.label != p.p1.label)
      val min = matching.map(_.value).min
      matching.find(p => p.value == min)
    }
    val nearestValues = nearestNeighbors.map(pp => pp.value).toArray

    val maxBin = max - (max % binSize) + binSize
    val bins = Matlib.linspace(0, maxBin, ceil(maxBin / binSize).toInt)
    val hist = Statlib.histc(nearestValues, bins)
    bins(hist.indexOf(hist.max))
  }

  /**
   * Get the probable ''best'' orientation. Calculates directions between all points and selects
   * the most frequent orientation
   *
   * @param points
   * @return
   */
  def bestOrientation(points: Iterable[LabeledDoublePoint2D]): Double = {
    val directions = PointDirectionsFn(points).map { d =>
    // Map angles between 0 and 180 to eliminate bi-directionality
      val theta = d.value
      if (theta < 0) {theta + Pi}
      else {theta}
    } toArray
    val bins = Matlib.linspace(0, Pi, 90)
    val hist = Statlib.histc(bins, directions)
    hist.find(b => b == hist.max).get
  }

  def centroid[A: Numeric](points: Iterable[Point2D[A]]) = {
    val numeric = implicitly[Numeric[A]]
    val x = points.map(p => numeric.toDouble(p.x)).sum / points.size
    val y = points.map(p => numeric.toDouble(p.y)).sum / points.size
    Point2D(x, y)
  }

  def centroid[A <: Point2D[Double]](points: Collection[A]) = {
    val x = points.asScala.map(p => p.x).sum / points.size.toDouble
    val y = points.asScala.map(p => p.x).sum / points.size.toDouble
    Point2D(x, y)
  }

  /**
   * Transforms the given points.
   * @param points The points to transform
   * @param scaleFactor The scaling to use for the transform
   * @param rotationRadians The rotation to apply
   * @param pivot The point around which the rotation will be applied.
   * @return The transformed points (labels from the original
   *         [[org.mbari.esp.ia.geometry.LabeledDoublePoint2D]] are preserved)
   */
  def rotateAndScale(points: Iterable[LabeledDoublePoint2D], scaleFactor: Double,
      rotationRadians: Double, pivot: Point2D[Double]): Iterable[LabeledDoublePoint2D] = {

    val scale = new Matrix(Array(
      Array(scaleFactor, 0),
      Array(0, scaleFactor)))
    val rotation = new Matrix(Array(
      Array(cos(rotationRadians), sin(rotationRadians)),
      Array(sin(rotationRadians), cos(rotationRadians))))

    points.map { p =>
      val m = new Matrix(Array(
        Array(p.x - pivot.x),
        Array(p.y - pivot.y)))
      val r = rotation.times(m)
      val rs = scale.times(r)

      Point2D(rs.get(0, 0) + pivot.x, rs.get(1, 0) + pivot.y, p.label)
    }

  }

  def translateRelativeTo[A: Numeric](points: Iterable[LabeledDoublePoint2D],
      oldAnchor: Point2D[A], newAnchor: Point2D[A]): Iterable[LabeledDoublePoint2D] = {
    val numeric = implicitly[Numeric[A]]
    val dx = numeric.toDouble(newAnchor.x) - numeric.toDouble(oldAnchor.x)
    val dy = numeric.toDouble(newAnchor.y) - numeric.toDouble(oldAnchor.y)
    points.map { p =>
      Point2D(p.x + dx, p.y + dy, p.label)
    }
  }


  def maximum[A <: Point2D[Double]](points: Collection[A]) = {
    val x = points.asScala.map(_.x).max
    val y = points.asScala.map(_.y).max
    Point2D(x, y)
  }

  def minimum[A <: Point2D[Double]](points: Collection[A]) = {
    val x = points.asScala.map(_.x).min
    val y = points.asScala.map(_.y).min
    Point2D(x, y)
  }


}
