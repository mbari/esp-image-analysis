package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import org.mbari.esp.ia.awt.ShortRegion

/**
 *
 * @author Brian Schlining
 * @since 2012-05-16
 */

object BrightSpotExtractorFn extends (ImageProcessor => Iterable[ShortRegion]) {

    def apply(from: ImageProcessor): Iterable[ShortRegion] = {
        // Calculate local standard deviation for every 3x3 block of pixels
        val to = StdFn(from)

        val threshold = To8BitGrayFn andThen MaximumEntropyThresholdFn

        val floodFill = {
            val floodFillWMinFn = FloodFillWithMinimumFn(5, _: ImageProcessor)
            val removeFilterFn = RemoveNonFilterAreaFn(0.75, _: ImageProcessor)
            To16BitGrayFn andThen removeFilterFn andThen floodFillWMinFn
        }
        val fill = threshold andThen floodFill

        SpotRegionsFn(from, fill(to))

    }
}
