package org.mbari.esp.ia.imglib

import ij.process.{FloatProcessor, ImageProcessor}


/**
 *
 * @author Brian Schlining
 * @since 2012-05-14
 */

class CrossCorrelateFn extends ((ImageProcessor, ImageProcessor) => FloatProcessor) {

    def apply(i1: ImageProcessor, i2: ImageProcessor): FloatProcessor = {
        throw new UnsupportedOperationException
    }


}
