package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import scala.math._
import org.mbari.esp.ia.geometry.{Point2D, Points}


/**
 * Masks out areas of the image that are not to be used for background intensity calculation.
 * Background intensity is the non-spot areas of the image. The following are excluded:
 * - Areas outside of the disk circle. Calculated as a circle where the centroid is from the
 *   aligned gal spots, the radius of the circle is from the centroid to the center of the spot
 *   farthest from the centroid
 * - Pixels with in each spot (defined by gal point and spotRadius)
 * - remaining bright pixels are excluded using a maximum entropy threshold
 * @author Brian Schlining
 * @since 2012-04-11
 */
class MaskFilterForegroundFn(spotRadius: Double,
        backgroundColor: Int) extends ((Iterable[Point2D[Double]], ImageProcessor) => ImageProcessor) {



    def apply(points: Iterable[Point2D[Double]], imageProcessor: ImageProcessor): ImageProcessor = {

        val r = ceil(spotRadius).toInt
        val centroid = Points.centroid(points)
        val diskRadius = points.map(_.distance(centroid)).max + r + 1

        val copyProcessor = imageProcessor.duplicate()

        // --- 1) Set areas outside our spots to the background value
        for (i <- 0 until copyProcessor.width; j <- 0 until copyProcessor.height) {
            val p = Point2D(i, j).toDouble
            if (p.distance(centroid) > diskRadius) {
                copyProcessor(i, j) = backgroundColor
            }
        }

        // --- 2) Set pixels within our gal defined spots to background values
        points.foreach { p =>
            val x = round(p.x).toInt
            val y = round(p.y).toInt

            for ( i <- (x - r) to (x + r); j <- (y - r) to (y + r)) {
                val s = Point2D(i, j).toDouble
                if (s.distance(p) < r) {
                    copyProcessor(i, j) = backgroundColor
                }
            }
        }

       /*
        * --- 3) Apply a threshold to remove bright noise. Spots above the threshold are noise and
        * will be set to zero in the copy.
        *
        * NOTE!! We don't ever want to use a 16-bit hist. It takes WAY TOO LONG to run
        * the maximum entropy threshold on it. Instead we convert it to an
        * 8-bit hist.
        */
        val thresheldProcessor = MaximumEntropyThresholdFn(To8BitGrayFn(copyProcessor))
        for (i <- 0 until copyProcessor.width; j <- 0 until copyProcessor.height) {
            if (thresheldProcessor(i, j) != 0) {
                copyProcessor(i, j) = backgroundColor
            }
        }

        copyProcessor
    }
}
