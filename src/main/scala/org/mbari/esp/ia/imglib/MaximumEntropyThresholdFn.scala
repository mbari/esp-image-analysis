package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import org.mbari.esp.ia.Histograms


/**
 * Threshold an image using a maximum entropy threshold technique.
 * @author Brian Schlining
 * @since 2012-03-07
 */
object MaximumEntropyThresholdFn extends ProcessorTransform {
    def apply(from: ImageProcessor): ImageProcessor = {
        val to = from.duplicate()
        val threshold = Histograms.maximumEntropy(from.getHistogram)
        to.threshold(threshold)
        to
    }
}

