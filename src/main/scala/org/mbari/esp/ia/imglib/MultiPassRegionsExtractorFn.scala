package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import org.mbari.esp.ia.awt.ShortRegion
import scala.collection.mutable
import org.mbari.esp.ia.Histograms
import org.mbari.esp.ia.geometry.{PointDistancesFn, Point2D}

/**
 *
 * @author Brian Schlining
 * @since 2012-05-11
 */

object MultiPassRegionsExtractorFn extends (ImageProcessor => Iterable[ShortRegion]) {

    private[this] val bound = 25

    private[this] val floodFill = {
        val floodFillWMinFn = FloodFillWithMinimumFn(5, _: ImageProcessor)
        val removeFilterFn = RemoveNonFilterAreaFn(0.75, _: ImageProcessor)
        To16BitGrayFn andThen removeFilterFn andThen floodFillWMinFn
    }

    def apply(from: ImageProcessor): Iterable[ShortRegion] = {
        val copy = To8BitGrayFn(from.duplicate())
        val rawRegions = toRegions(copy)
        val stdRegions = toRegions(To8BitGrayFn(StdFn(copy)))

        // --- Make sure labels aren't duplicated and combine regions
        val maxLabel = rawRegions.map(_.label).max
        val stdRegions2 = stdRegions.map { r =>
            ShortRegion(r.label + maxLabel, r.points)
        }
        val combinedRegions = rawRegions ++ stdRegions2
        val regions = new mutable.ArrayBuffer[ShortRegion] ++= removeDuplicateCentroids(combinedRegions)

        // --- Remove duplicate points that are less than some given distance from each other.
        val distancePairs = PointDistancesFn(regions.map(_.centroid()))
        val regionsToDrop = distancePairs.map { pointPair =>
            if (pointPair.value <= 3) {
                regions.find(_.label == pointPair.p1.label)
            }
            else {
                None
            }

        }.flatten.distinct
        regions --= regionsToDrop

    }


    private def toRegions(from: ImageProcessor): Iterable[ShortRegion] = {
        //val split = Histograms.maximumEntropy(from.getHistogram)
        val split = Histograms.renyiEntropy(from.getHistogram)
        val minVal = if (split - bound < 0) split else split - bound
        val maxVal = if (split + bound > 255) split else split + bound

        // Loop over various thresholds extracting
        val regions = new mutable.ArrayBuffer[ShortRegion]
        for (i <-  minVal to maxVal by 5) {
            var buf = from.duplicate()
            buf.threshold(i)
            var localRegions = SpotRegionsFn(from, floodFill(buf)).filter(_.eccentricity()._1 < 3)

            buf = null
            // relabel regions so they do not have overlapping labels
            if (!regions.isEmpty) {
                val maxLabel = regions.map(_.label).max
                localRegions = localRegions.map { r =>
                    ShortRegion(r.label + maxLabel, r.points)
                }
            }

            regions ++= localRegions
        }

        // Filter Regions and remove those with duplicate centroids
        removeDuplicateCentroids(regions)
    }

    private def removeDuplicateCentroids(r1: Iterable[ShortRegion]): Iterable[ShortRegion] ={
        // Filter Regions and remove those with duplicate centroids
        val foundCentroids = new mutable.ArrayBuffer[Point2D[Int]]
        val filteredRegions = new mutable.ArrayBuffer[ShortRegion]
        r1.foreach { r =>
            val centroid = r.centroid.toInt
            if (!foundCentroids.contains(centroid)) {
                foundCentroids += centroid
                filteredRegions += r
            }
        }
        filteredRegions
    }
}


