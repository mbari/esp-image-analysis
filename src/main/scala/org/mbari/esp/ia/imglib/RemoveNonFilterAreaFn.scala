package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import scala.math._
import org.mbari.esp.ia.geometry.Point2D

/**
 * Mask out the portion of an image that's supposedly outside of the filter disk. We do this by
 * calculating the minimum radius from the center to an edge and using 75% of that.
 *
 * @author Brian Schlining
 * @since 2012-03-07
 */
class RemoveNonFilterAreaFn(val percentRadius: Double = 0.75) extends ProcessorTransform {
    require(percentRadius >= 0 && percentRadius <= 1,
        "percentRadius must be between 0 and 1, not " + percentRadius)

    def apply(from: ImageProcessor): ImageProcessor = {
        val to = from.duplicate()
        val w = from.getWidth
        val h = from.getHeight
        val backgroundColor = from.getMin.toInt
        val center = Point2D(w / 2D, h / 2D)
        val minRadius = from.minimumRadius(center) * percentRadius
        val radius = floor(minRadius).toInt

        for (i <- 0 until w; j <- 0 until h) {
            val p = Point2D(i.toDouble, j.toDouble)
            val r = p.distance(center)
            if (r > radius) {
                to.set(i, j, backgroundColor)
            }
        }

        to
    }
}

object RemoveNonFilterAreaFn {
    def apply(percentRadius: Double, from: ImageProcessor) = {
        val fn = new RemoveNonFilterAreaFn(percentRadius)
        fn(from)
    }
}



