package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import org.mbari.esp.ia.Histograms

/**
 *
 * @author Brian Schlining
 * @since 2012-05-11
 */

object RenyiEntropyThresholdFn extends ProcessorTransform {
    def apply(from: ImageProcessor): ImageProcessor = {
        val to = from.duplicate()
        val split = Histograms.renyiEntropy(from.getHistogram)
        to.threshold(split)
        to
    }
}


