package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import org.mbari.esp.ia.awt.ShortRegion


/**
 * Extracts the spots from an image and returns a collection of
 * [[org.mbari.esp.ia.awt.ShortRegion]]s
 *
 * @author Brian Schlining
 * @since 2012-03-13
 */
object SpotRegionsExtractorFn extends (ImageProcessor => Iterable[ShortRegion]) {


    /**
     * @param rawImage The image to extract the regions from
     * @return A collection of ShortRegions representing the spots
     */
    def apply(rawImage: ImageProcessor): Iterable[ShortRegion] = {
        // --- Reusable functions
        val threshold = To8BitGrayFn andThen MaximumEntropyThresholdFn
        val floodFill = {
            val floodFillWMinFn = FloodFillWithMinimumFn(5, _: ImageProcessor)
            val removeFilterFn = RemoveNonFilterAreaFn(0.75, _: ImageProcessor)
            To16BitGrayFn andThen removeFilterFn andThen floodFillWMinFn
        }

        // --- Extract Regions from the Raw Image
        val rawFloodFill = threshold andThen floodFill
        val rawRegions = SpotRegionsFn(rawImage, rawFloodFill(rawImage))

        // --- Extract Regions from AutoContrasted Image
        val acFloodFill = new AutoContrastFn(0.95) andThen threshold andThen floodFill
        val acRegions = SpotRegionsFn(rawImage, acFloodFill(rawImage))

        // relabel regions so they do not have overlapping labels
        val maxRawLabel = rawRegions.map(_.label).max
        val relabeledAcRegions = acRegions.map { r =>
            ShortRegion(r.label + maxRawLabel, r.points)
        }

        // --- Extract Regions from Dynamic Contrast Image
        val dynFloodFill = To16BitGrayFn andThen FuzzyContrastFn andThen threshold andThen floodFill
        val dynRegions = SpotRegionsFn(rawImage, dynFloodFill(rawImage))

        // relabel regions so they do not have overlapping labels
        val maxAcLabel = relabeledAcRegions.map(_.label).max
        val relabeledDynRegions = dynRegions.map { r =>
            ShortRegion(r.label + maxAcLabel, r.points)
        }

        rawRegions ++ relabeledAcRegions ++ relabeledDynRegions

    }
}


