package org.mbari.esp.ia.imglib

import scala.collection.mutable
import org.mbari.esp.ia.awt.ShortRegion
import ij.process.{ColorProcessor, ImageProcessor}

/**
 * Extract labeled regions from an image
 *
 * @author Brian Schlining
 * @since 2012-03-07
 */
object SpotRegionsFn extends ((ImageProcessor, ImageProcessor) => Iterable[ShortRegion]) {

    /**
     *
     * @param rawProcessor The original image
     * @param floodFilledProcessor The flood-filled version of the original image
     * @return A collection of ShortRegions, 1 for each flod-filled label, where the z pixel values
     *     are the intensities from the rawProcessor
     */
    def apply(rawProcessor: ImageProcessor, floodFilledProcessor: ImageProcessor): Iterable[ShortRegion] = {
        require(rawProcessor.getClass != classOf[ColorProcessor],
            "rawProcessor can not be a ColorProcessor")
        val map = new mutable.HashMap[Int, ShortRegion]
        val background = floodFilledProcessor.processorType.minValue.toInt

        for (i <- 0 until floodFilledProcessor.getWidth; j <- 0 until floodFilledProcessor.getHeight) {
            val z = floodFilledProcessor.get(i, j)
            if (z != background) {
                val k = rawProcessor.get(i, j)
                val region = map.getOrElseUpdate(z, ShortRegion(z))
                region.addPixel(i, j, k)
            }
        }
        map.values
    }
}

