package org.mbari.esp.ia.imglib

import ij.process.ImageProcessor
import java.io.File
import java.awt.geom.Ellipse2D
import java.awt.{Color, Graphics2D}
import org.mbari.esp.ia.services.ToolBox
import org.mbari.esp.ia.geometry.LabeledIntPoint2D
import org.mbari.esp.ia.awt.Drawing

/**
 *
 * @author Brian Schlining
 * @since 2012-05-11
 */

class SpotsApp(prep: ProcessorTransform, threshold: ProcessorTransform,
        floodFill: ProcessorTransform) {

    private[this] val processImage = threshold andThen floodFill

    def apply(image: ImageProcessor, target: File) {
        val preppedImage = prep(image)
        val floodfilledImage = processImage(preppedImage)
        val regions = SpotRegionsFn(image, floodfilledImage).filter(_.eccentricity()._1 < 3)
        val points = regions.map(_.centroid.toInt).toSeq
        SpotsApp.write(preppedImage, target, points)
    }


}


object SpotsApp {

    private val noopTransform = new ProcessorTransform {
        def apply(v1: ImageProcessor): ImageProcessor = v1
    }

    def main(args: Array[String]) {

        val imageFile = new File(args(0))
        val image = ToolBox.imageIOService.read(imageFile.toURI.toURL).getProcessor
        val baseName =  imageFile.getName.split('.').head


        val threshold = To8BitGrayFn andThen MaximumEntropyThresholdFn
        //val threshold = To8BitGrayFn andThen RenyiEntropyThresholdFn
        val floodFill = {
            val floodFillWMinFn = FloodFillWithMinimumFn(5, _: ImageProcessor)
            val removeFilterFn = RemoveNonFilterAreaFn(0.75, _: ImageProcessor)
            To16BitGrayFn andThen removeFilterFn andThen floodFillWMinFn
        }
        val prep1 = new AutoContrastFn(0.95)
        val prep2 = StdFn(_: ImageProcessor, 1)
        val prep3 = { from: ImageProcessor =>
            val to = StdFn(from)
            for (i <- 0 until to.width; j <- 0 until to.height) {
                val z = to(i, j)
                to(i, j) = z * z
            }
            to
        }

        val rawApp = new SpotsApp(noopTransform, threshold, floodFill)
        rawApp(image, new File(baseName + "-raw.png"))

        val acApp = new SpotsApp(prep1, threshold, floodFill)
        acApp(image, new File(baseName + "-autocontrast.png"))

        val dynApp = new SpotsApp(prep2, threshold, floodFill)
        dynApp(image, new File(baseName + "-std.png"))

        val varApp = new SpotsApp(prep3, threshold, floodFill)
        varApp(image, new File(baseName + "-var.png"))

        val points = SpotRegionsExtractorFn(image)
                .filter(_.eccentricity()._1 < 3)
                .map(_.centroid().toInt)
        write(image, new File(baseName + "-spotsregions.png"), points)

        val mPoints = MultiPassRegionsExtractorFn(image)
                .map(_.centroid().toInt)
        write(image, new File(baseName + "-multipass.png"), mPoints)

    }

    def write(image: ImageProcessor, target: File, points: Iterable[LabeledIntPoint2D]) {
        val colorImage = Drawing.draw(image, points)
        ToolBox.imageIOService.write(colorImage, target)
    }
}
