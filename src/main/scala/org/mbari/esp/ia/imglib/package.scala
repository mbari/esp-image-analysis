package org.mbari.esp.ia

import ij.process.ImageProcessor

/**
 * Image processing functions based on <a href="http://developer.imagej.net/imglib">imglib2<a>
 */
package object imglib {

    type ProcessorTransform = (ImageProcessor) => ImageProcessor

    /**
     * implicit conversion of [[ij.process.ImageProcessor]] to ImageProcessorExt
     * @param imageProcessor
     * @return
     */
    implicit def extendImageProcessor(imageProcessor: ImageProcessor) = new ImageProcessorExt(imageProcessor)

}