package org.mbari.esp.ia.services

import org.mbari.esp.ia.geometry.GALPoint
import java.util.{List => JList}
import java.net.URL
import java.io.{File, IOException, InputStream}


/**
 *
 * @author Brian Schlining
 * @since 2012-03-06
 */

trait GeneArrayIOService {

  /**
   * <p>Write a list of GALPoints out to a new File. The file won't contain all the headers one would normally
   * find in the layout description file. However, it doesn contain enough info to recreate a spot image the
   * is useful for image processing and analysis.</p>
   *
   * <p>This method is originally created for writing out the control points of any array to a file so that we can
   * use it for analysis</p>
   *
   * @param file The file to write to
   * @param points The list of GALPoints to write to the file
   * @throws IOException If any error occurs during I/O
   */
  def write(file: File, points: List[GALPoint]): Unit

  /**
   * Reads a file used by the piezo array printer and generates a list
   * of GeneArrayValue objects (with only the x and y locations filled in)
   *
   * @param url URL to read
   * @return a List<GALPoint>
   */
  def read(url: URL): Seq[GALPoint]

  def read(inputStream: InputStream): Seq[GALPoint]

  def read(lines: Iterable[String]): Seq[GALPoint]


  def readAsJava(url: URL): JList[GALPoint]


  def canParse(url: URL): Boolean

  def canParse(lines: Iterable[String]): Boolean

}
