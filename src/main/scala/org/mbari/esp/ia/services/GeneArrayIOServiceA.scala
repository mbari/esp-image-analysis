package org.mbari.esp.ia.services

import java.io._
import java.net.URL
import java.util
import org.mbari.esp.ia.geometry.{GALPoint, Point2D}
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.math._
import scala.util.Try
import scala.io.Source


/**
 *
 * @author Brian Schlining
 * @since 2012-03-06
 */

class GeneArrayIOServiceA extends GeneArrayIOService {


  /**
   *
   * @param file The file to write to
   * @param points The list of GALPoints to write to the file
   * @throws IOException
   */
  def write(file: File, points: List[GALPoint]) {
    val invertedPoints = GeneArrayAnalysisService.invertX(points)
    val sortedPoints = invertedPoints.sortBy(_.spotNumber)
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    try {
      writer.write(GeneArrayIOServiceA.LineBeginMapInfo + "\n")
      writer.write(GeneArrayIOServiceA.LineHeader + "\n")
      sortedPoints.foreach {
        p =>
          writer.write("%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%s\n".format(
            p.spotNumber, // Integer
            p.sourceWell, // String
            p.spotNumber, // Integer
            p.tipNumber, // Integer
            p.subNumber, // Integer
            round(p.x), // Double
            round(p.y), // Double
            round(p.spotVolume), // Double
            p.status));
      }
      writer.write(GeneArrayIOServiceA.LineEndMapInfo + "\n")
    }
    finally {
      writer.close()
    }
  }

  /**
   *
   * @param url URL to read
   * @return a List<GALPoint>
   * @throws IOException
   */
  def read(url: URL): Seq[GALPoint] = {
    val lines = Source.fromURL(url).getLines().toList
    read(lines)
  }



  override def read(inputStream: InputStream): Seq[GALPoint] = {
    val lines = Source.fromInputStream(inputStream).getLines().toList
    read(lines)
  }

  override def read(lines: Iterable[String]): Seq[GALPoint] = {
    val arrayValues = new mutable.HashMap[Point2D[Double], GALPoint]
    var startMapInfo = false
    var readHeader = false
    var lineSeq = lines.toList
    var lineOption: Option[String] = lineSeq.headOption

    while (lineOption.isDefined) {

      val line = lineOption.get

      if (line == GeneArrayIOServiceA.LineBeginMapInfo) {
        startMapInfo = true
      }

      if (line == GeneArrayIOServiceA.LineEndMapInfo) {
        lineOption = None
      }
      else {
        if (startMapInfo) {
          if (readHeader) {
            val values = line.split('\t')
            val x = values(5).toDouble
            val y = values(6).toDouble
            val point = Point2D(x, y)
            if (!arrayValues.contains(point)) {
              val spotNumber = values(0).toInt
              val sourceWell = values(1)
              val sourceNumber = values(2).toInt
              val tipNumber = values(3).toInt
              val subNumber = values(4).toInt
              val spotVolume = values(7).toDouble
              val status = values(8)
              arrayValues.put(point, new GALPoint(x, y, sourceNumber, sourceWell,
                spotNumber, spotVolume, status, subNumber, tipNumber))

            }
          }
          else {
            /*
             * The second line is a header. we need to skip it too.
             */
            val columns = GeneArrayIOServiceA.LineHeader.split('\t')

            if (line.startsWith(columns(0))) {
              readHeader = true
            }
          }


        }

        lineSeq = lineSeq.tail
        lineOption = lineSeq.headOption
      }
    }

    GeneArrayAnalysisService.invertX(arrayValues.values.toSeq)
  }

  def readAsJava(url: URL): util.List[GALPoint] = read(url).toList.asJava

  override def canParse(url: URL): Boolean = Try {
    val source = Source.fromURL(url)
    val b = canParse(source.getLines().toList)
    source.close()
    b
  } getOrElse false

  override def canParse(lines: Iterable[String]): Boolean = Try(
    lines.exists(_.startsWith(GeneArrayIOServiceA.LineBeginMapInfo))
    ).getOrElse(false)
}

object GeneArrayIOServiceA {
  val LineBeginMapInfo = "!!! Begin map Info"
  val LineEndMapInfo = "!!! End Map Info"
  val LineHeader = "SpotNum\tSrcWell\tSrcNum\tTipNum\tSubNum\tSpotX\tSpotY\tSpotVol\tStatus"

}
