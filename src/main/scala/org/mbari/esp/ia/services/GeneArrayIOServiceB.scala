package org.mbari.esp.ia.services

import java.io.{File, IOException, InputStream}
import java.net.URL
import java.util.{List => JList}

import org.mbari.esp.ia.geometry.GALPoint

import scala.collection.JavaConverters._
import scala.io.Source
import scala.util.Try

/**
 * Created by brian on 3/3/14.
 */
class GeneArrayIOServiceB extends GeneArrayIOService {

  override def readAsJava(url: URL): JList[GALPoint] = read(url).toList.asJava

  /**
   * Reads a file used by the new piezo array printer and generates a list
   * of GeneArrayValue objects (with only the x and y locations filled in)
   *
   * @param url URL to read
   * @return a List<GALPoint>
   */
  override def read(url: URL): Seq[GALPoint] = {
    val source = Source.fromURL(url)
    val galPoints = read(source.getLines().toList)
    source.close()
    galPoints
  }


  override def read(inputStream: InputStream): Seq[GALPoint]  = {
    val source = Source.fromInputStream(inputStream)
    val galPoints = read(source.getLines().toList)
    source.close()
    galPoints
  }

  override def read(lines: Iterable[String]): Seq[GALPoint] = {
    var isHeader = true
    var spotNumber = 0
    val pts = for (line <- lines) yield {
      if (isHeader) {
        if (line.equalsIgnoreCase("Block\tRow\tColumn\tID\tName")) {
          isHeader = false
        }
        None
      }
      else {
        val parts = line.split('\t')
        val name = parts(4)  // TODO this crashes if the GAL contains blank lines
        if (!name.equalsIgnoreCase("Empty")) {
          spotNumber = spotNumber + 1
          val x = parts(2).toInt
          val y = parts(1).toInt
          Try(new GALPoint(x, y, 0, name, spotNumber, 0, "", 0, 0)).toOption
        }
        else {
          None
        }
      }

    }
    pts.filter(_.isDefined).map(_.get).toSeq
  }

  /**
   * <p>Write a list of GALPoints out to a new File. The file won't contain all the headers one would normally
   * find in the layout description file. However, it doesn contain enough info to recreate a spot image the
   * is useful for image processing and analysis.</p>
   *
   * <p>This method is originally created for writing out the control points of any array to a file so that we can
   * use it for analysis</p>
   *
   * @param file The file to write to
   * @param points The list of GALPoints to write to the file
   * @throws IOException If any error occurs during I/O
   */
  @throws[IOException]
  override def write(file: File, points: List[GALPoint]): Unit = ???

  override def canParse(url: URL): Boolean = Try {
    val source = Source.fromURL(url)
    val b = canParse(Seq(source.getLines().toList.head))
    source.close()
    b
  } getOrElse false

  override def canParse(lines: Iterable[String]): Boolean = Try(
    lines.headOption
      .exists(_.startsWith(GeneArrayIOServiceB.FIRST_ROW_TAG))
  ).getOrElse(false)
}

object GeneArrayIOServiceB {

  val FIRST_ROW_TAG = "ATF"

}
