package org.mbari.esp.ia.services

import java.io.{File, InputStream}

import org.mbari.esp.ia.geometry.GALPoint
import java.net.URL
import java.util

import scala.io.Source
import scala.util.Try

/**
 * Created by brian on 3/3/14.
 */
class GeneArrayIOServiceImpl extends GeneArrayIOService {

  private[this] val services = List(new GeneArrayIOServiceA, new GeneArrayIOServiceB2)

  override def readAsJava(url: URL): util.List[GALPoint] = ???

  /**
   * Reads a file used by the piezo array printer and generates a list
   * of GeneArrayValue objects (with only the x and y locations filled in)
   *
   * @param url URL to read
   * @return a List<GALPoint>
   */
  override def read(url: URL): Seq[GALPoint] = {
    Try {
      val ioService = services.filter(_.canParse(url)).head
      ioService.read(url)
    } getOrElse Nil
  }


  override def read(inputStream: InputStream): Seq[GALPoint] = {
    Try {
      val lines = Source.fromInputStream(inputStream)
        .getLines()
        .toList
      read(lines)
    } getOrElse Nil
  }

  override def read(lines: Iterable[String]): Seq[GALPoint] = {
    Try {
      val ioService = services.filter(_.canParse(lines)).head
      ioService.read(lines)
    } getOrElse Nil
  }


  /**
   * <p>Write a list of GALPoints out to a new File. The file won't contain all the headers one would normally
   * find in the layout description file. However, it doesn contain enough info to recreate a spot image the
   * is useful for image processing and analysis.</p>
   *
   * <p>This method is originally created for writing out the control points of any array to a file so that we can
   * use it for analysis</p>
   *
   * @param file The file to write to
   * @param points The list of GALPoints to write to the file
   * @throws IOException If any error occurs during I/O
   */
  override def write(file: File, points: List[GALPoint]): Unit = new GeneArrayIOServiceA write(file, points)

  override def canParse(url: URL): Boolean = services.exists(_.canParse(url))

  override def canParse(lines: Iterable[String]): Boolean = {
    val ls = lines.toList // need to eagerly evaluate the lines
    services.exists(_.canParse(ls))
  }
}


class GeneArrayIOServiceB2 extends GeneArrayIOServiceB {
  /**
   * Reads a file used by the new piezo array printer and generates a list
   * of GeneArrayValue objects (with only the x and y locations filled in)
   *
   * This overridden version multiplies the x, y coordinates by 1000 to more closely scale the output to what
   * the original GAL file's x and y coordinates. It also removes any GALPoints with the name "Fiducial"
   *
   * @param url URL to read
   * @return a List<GALPoint>
   */
  override def read(url: URL): Seq[GALPoint] = super.read(url)
      .filter(_.sourceWell != "Fiducial").map(g => new GALPoint(g, g.x * 500, g.y * 500))
}