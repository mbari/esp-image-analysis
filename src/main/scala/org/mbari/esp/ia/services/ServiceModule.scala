package org.mbari.esp.ia.services

import com.google.inject.{Binder, Module}


/**
 * Guice module for ESP image processing
 * @author Brian Schlining
 * @since 2012-03-06
 */
class ServiceModule extends Module {

    def configure(binder: Binder) {
        binder.bind(classOf[GeneArrayIOService]).to(classOf[GeneArrayIOServiceImpl])
        binder.bind(classOf[ImageIOService]).to(classOf[ImageIOServiceB])
      //binder.bind(classOf[ImageIOService]).to(classOf[ImageIOServiceC])
    }
}
