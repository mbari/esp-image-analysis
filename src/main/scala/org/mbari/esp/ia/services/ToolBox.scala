package org.mbari.esp.ia.services

import com.google.inject.{Guice, Injector}

/**
 * Provides convenient access to services that are injected by Guice
 *
 * @author Brian Schlining
 * @since 2012-03-06
 */
object ToolBox {

  val injector: Injector = Guice.createInjector(new ServiceModule)

  lazy val imageIOService: ImageIOService = injector.getInstance(classOf[ImageIOService])

  lazy val geneArrayIOService: GeneArrayIOService = injector.getInstance(classOf[GeneArrayIOService])

}
