package org.mbari.esp.ia.tools

import java.io.File
import org.mbari.esp.ia.services.ToolBox

/**
 * 
 * @author Brian Schlining
 * @since 2013-01-31
 */
object ImageConverter extends App {

  val inputDir = new File(args(0))
  val outputDir = new File(args(1))
  val ext = args(2)

  val imageIOService = ToolBox.imageIOService

  inputDir.listFiles().foreach { f =>
    val s = f.getName.toUpperCase()
    if (s.endsWith(".PNG") || s.endsWith(".TIF") || s.endsWith(".TIFF") || s.endsWith(".JPG")) {
      val source = imageIOService.read(f.toURI.toURL)
      val name = f.getName
      val extIdx = name.lastIndexOf('.')
      val target = new File(outputDir, name.substring(0, extIdx) + ext)
      imageIOService.write(source.getBufferedImage, target)
    }
  }

}
