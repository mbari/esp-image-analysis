package org.mbari.esp.ia.ui

import org.slf4j.LoggerFactory

import com.google.inject.util.Modules
import org.mbari.esp.ia.services.ServiceModule
import com.google.inject.{Stage, Guice}

/**
 *
 * @author Brian Schlining
 * @since 2012-04-11
 */

object ImageMeasurementApp {

    private[this] val log = LoggerFactory.getLogger(this.getClass)

    private[this] val injector = {
        val module = Modules.combine(new UIModule, new ServiceModule)
        Guice.createInjector(Stage.PRODUCTION, module)
    }


    private[this] val frame = {}

    def main(args: Array[String]) {

    }

}
