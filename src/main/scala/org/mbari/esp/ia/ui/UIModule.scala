package org.mbari.esp.ia.ui

import com.google.common.eventbus.EventBus
import com.google.{inject => guice}
import com.google.inject.{Binder, Module}


/**
 *
 * @author Brian Schlining
 * @since 2012-04-11
 */

class UIModule extends Module {
    def configure(binder: Binder) {
        // Wire in a singleton eventbus for UI components to communicate with each other
        binder.bind(classOf[EventBus]).in(classOf[guice.Singleton])
    }
}
