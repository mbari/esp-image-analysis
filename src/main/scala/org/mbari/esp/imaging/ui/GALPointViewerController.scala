package org.mbari.esp.imaging.ui

import java.util.{ArrayList, List => JList}
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, GeneArrayIOService}
import org.mbari.esp.ia.geometry.GALPoint
import scala.collection.mutable
import scala.collection.JavaConverters._
import org.mbari.esp.ia.awt.SourceWell
import java.awt.FileDialog
import java.io.File
import org.mbari.awt.image.ImageUtilities
import java.awt.image.BufferedImage
import java.util

/**
 *
 * @author Brian Schlining
 * @since 2013-04-30
 */
class GALPointViewerController(viewer: GALPointViewer, geneArrayIOService: GeneArrayIOService) {

  private[this] val exportFileChooser = {
    val userHome = System.getProperty("user.home")
    val c = new FileDialog(viewer, "ESP - Export Controls to ...", FileDialog.SAVE)
    c.setDirectory(userHome)
    c
  }

  private[this] val imageChooser = {
    val userHome = System.getProperty("user.home")
    val c = new FileDialog(viewer, "ESP - Export Controls to ...", FileDialog.SAVE)
    c.setDirectory(userHome)
    c
  }

  def toSourceWells(points: JList[GALPoint]) = {
    val ps = new util.ArrayList[SourceWell]
    ps.addAll(GeneArrayAnalysisService.toSourceWells(points.asScala).toList.asJava)
    ps
  }


  def generateControlFile(points: JList[GALPoint], sourceWells: JList[SourceWell]) {
    exportFileChooser.setVisible(true)
    if (exportFileChooser.getFile != null) {
      val exportFile = new File(exportFileChooser.getDirectory, exportFileChooser.getFile)
      // Filter for sourceWells that are control points
      val controlNameMap = sourceWells.asScala.filter(_.control).map(s => s.name -> s).toMap
      // Filter for GAL control points
      val controls = new mutable.ListBuffer[GALPoint]
      for (p <- points.asScala) {
        val well = controlNameMap(p.sourceWell)
        if (well != null) {
          controls += GALPoint(p, well.description)
        }
      }
      geneArrayIOService.write(exportFile, controls.toList)
    }
  }

  def generateImage(points: JList[GALPoint], sourceWells: JList[SourceWell], scale: Int) {
    imageChooser.setVisible(true)
    if (imageChooser.getFile != null) {
      val imageFile = new File(imageChooser.getDirectory, imageChooser.getFile)
      val squareSize = PiezzoUtilities.estimateGridSize(points, scale)
      val image = GeneArrayAnalysisService.toBufferedImage(points.asScala, true, scale, squareSize,
        Option(sourceWells.asScala.toSet))
      ImageUtilities.saveImage(image, imageFile)
    }
  }

  def toBufferedImage(points: JList[GALPoint], showLegend: Boolean, scale: Int, sourceWells: JList[SourceWell]): BufferedImage =
    GeneArrayAnalysisService.toBufferedImage(points.asScala, showLegend, scale, PiezzoUtilities.estimateGridSize(points, scale))


}
