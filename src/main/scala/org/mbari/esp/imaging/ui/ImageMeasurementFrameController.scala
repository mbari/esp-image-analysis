package org.mbari.esp.imaging.ui

import ij.ImagePlus
import java.awt.image.BufferedImage
import java.awt.{Cursor, FileDialog, Image}
import java.io._
import java.net.{URL, MalformedURLException}
import java.text.SimpleDateFormat
import java.util
import java.util.prefs.Preferences
import java.util.{Date, TimeZone}
import javax.imageio.ImageIO
import javax.swing._
import org.bushe.swing.event.EventBus
import org.mbari.awt.image.ImageUtilities
import org.mbari.esp.ia.geometry.{GALPoint, Points, DoublePoint2D}
import org.mbari.esp.ia.services.{ImageIOService, GeneArrayIOService, GeneArrayAnalysisService}
import org.mbari.esp.{ImagingServiceException}
import org.mbari.swing.JImageFrame
import org.mbari.util.SystemUtilities
import org.slf4j.LoggerFactory
import scala.collection.JavaConverters._
import scala.collection.mutable


/**
 *
 * @author Brian Schlining
 * @since 2013-04-30
 */
class ImageMeasurementFrameController(frame: ImageMeasurementFrame,
    val geneArrayIOService: GeneArrayIOService,
    val imageIOService: ImageIOService) {


  val IMAGE_DIR_PREFERENCE = "image-directory"
  val GAL_DIR_PREFERENCE = "gal-directory"
  val SPOT_SIZE_PREFERENCE = "spot-size"
  val BLOCK_SIZE_PREFERENCE = "block-size"
  val SAVE_DIRECTORY_PREFERENCE = "save-directory"
  private[this] val preferences = Preferences.userNodeForPackage(classOf[ImageMeasurementFrameController]);

  val REAL_SCALE_MAX = 1.0
  val REAL_SCALE_MIN = 0.0001
  val REAL_SCALE_INTERVAL = 0.0001
  val REAL_SCALE_INITIAL = 0.55D

  private[this] val dateFormat = {
    val d = new SimpleDateFormat("yyyyMMddHHmmss")
    d.setTimeZone(TimeZone.getTimeZone("UTC"))
    d
  }
  
  private[this] val imageFileChooser = {
    val openDirectory = preferences.get(IMAGE_DIR_PREFERENCE, System.getProperty("user.home"))
    val c = new FileDialog(frame, "ESP - Add an Image File", FileDialog.LOAD)
  }

  private[this] val imageDirChooser = {
    val openDirectory = preferences.get(IMAGE_DIR_PREFERENCE, System.getProperty("user.home"))
    val c = new JFileChooser(openDirectory)
    c.setDialogTitle("ESP - Select directory containing filter images")
    c.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
    c
  }

  private[this] val saveDirFileChooser = {
    val saveDirectory = new File(preferences.get(SAVE_DIRECTORY_PREFERENCE, System.getProperty("user.home")))
    val s = new JFileChooser()
    s.setDialogTitle("ESP - Select directory to save results to")
    s.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
    s.setCurrentDirectory(saveDirectory)
    s
  }



  private[this] val log = LoggerFactory.getLogger(getClass)

  private[this] var saveDirectory: File = _

  {
    /*
     * Load preference settings on startup
     */
    val spotSize = preferences.getInt(SPOT_SIZE_PREFERENCE, frame.getSpotSizeSpinner.getValue.asInstanceOf[Integer])
    frame.getSpotSizeSpinner.setValue(spotSize)
    val blockSize = preferences.getInt(BLOCK_SIZE_PREFERENCE, frame.getBlockSizeSpinner.getValue.asInstanceOf[Integer])
    frame.getBlockSizeSpinner.setValue(blockSize)
    val saveDirName = preferences.get(SAVE_DIRECTORY_PREFERENCE, System.getProperty("user.home"))
    saveDirectory = new File(saveDirName)
    log.debug("Saving data to " + saveDirectory.getAbsolutePath)
  }

  /**
   * Save preference settings on shutdown.
   */
  Runtime.getRuntime.addShutdownHook(new Thread(new Runnable() {
    override def run() {
      preferences.putInt(SPOT_SIZE_PREFERENCE, frame.getSpotSizeSpinner.getValue.asInstanceOf[Integer])
      preferences.putInt(BLOCK_SIZE_PREFERENCE, frame.getBlockSizeSpinner.getValue.asInstanceOf[Integer]);
    }
  }))


  /**
   * Handles actions need to add a single image to the filter list in the view
   */
  def addOneImage() {
    // Use a swing Dialog to select all image files in a directory
    /*
     * Read save directory from preferences
     */
    val openDirectory = preferences.get(IMAGE_DIR_PREFERENCE, System.getProperty("user.home"))
    val fileDialog = new FileDialog(frame, "ESP - Add Image File", FileDialog.LOAD)
    fileDialog.setDirectory(openDirectory)
    fileDialog.setFilenameFilter(new FilenameFilter {
      override def accept(dir: File, p: String): Boolean = p.endsWith(".png") || p.endsWith(".jpg") ||
          p.endsWith(".gif") || p.endsWith(".tif") || p.endsWith(".tiff")
    })
    fileDialog.setVisible(true)

    if (fileDialog.getFile != null) {
      val directory = fileDialog.getDirectory
      preferences.put(IMAGE_DIR_PREFERENCE, directory)
      val file = fileDialog.getFile
      val imageFile = new File(directory, file)
      try {
        val imageUrl = imageFile.toURI.toURL
        frame.getImageListModel.addElement(imageUrl)
      }
      catch {
        case e: MalformedURLException => {
          log.info("Bad URL!! " + imageFile.getAbsolutePath)
          EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, e)
        }
      }
    }
  }

  def addGalFile2() {
    /*
     * Read save directory from preferences
     */
    val openDirectory = preferences.get(GAL_DIR_PREFERENCE, System.getProperty("user.home"))

    // Show open URL panel
    val fileDialog = new FileDialog(frame, "ESP - Add Gene Array Layout File", FileDialog.LOAD)
    fileDialog.setDirectory(openDirectory)
    fileDialog.setVisible(true)

    if (fileDialog.getFile != null) {
      val directory = fileDialog.getDirectory
      preferences.put(GAL_DIR_PREFERENCE, directory)
      val file = fileDialog.getFile
      val galFile = new File(directory, file)

      // Get url from panel and add it to the list
      try {
        val galUrl = galFile.toURI.toURL
        frame.getGalListModel.addElement(galUrl)
      }
      catch {
        case ex: MalformedURLException => {
          log.info("Bad URL!! " + galFile.getAbsolutePath)
          EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex)
        }
      }

    }
    fileDialog.dispose()
  }

  /**
   * Handles actions needed to add a directory full of filter images to the
   * view.
   */
  def addManyImages() {

    // Use a swing Dialog to select all image files in a directory
    val status = imageDirChooser.showOpenDialog(frame);
    if (JFileChooser.APPROVE_OPTION == status) {
      // TODO open all image files in the directory .png, .tif, .jpg, .gif
      val selectedFile = imageDirChooser.getSelectedFile
      val currentDirectory = imageDirChooser.getCurrentDirectory
      preferences.put(IMAGE_DIR_PREFERENCE, currentDirectory.getAbsolutePath)

      val allFiles = selectedFile.listFiles()
      for (f <- allFiles) {
        val p = f.getAbsolutePath
        if (p.endsWith(".png") || p.endsWith(".jpg") || p.endsWith(".gif") || p.endsWith(".tif") || p.endsWith(".tiff")) {
          try {
            frame.getImageListModel.addElement(f.toURI.toURL)
          }
          catch {
            case e: MalformedURLException => {
              log.info("Bad URL!! " + p);
              EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, e);
            }
          }
        }
      }
    }
  }

  /**
   * Renders (i.e. Draws) a newly selected GAL file onto the Glasspane that
   * sits on top of the image frame.
   *
   * @param galFile
   */
  def renderGalFile(galFile: URL) {
    log.debug("Rendering GAL file: " + galFile.toExternalForm)
    var points = geneArrayIOService.read(galFile)
    points = GeneArrayAnalysisService.filterSingleSpot(points, 12.5 * 1000)
    val currentImage: BufferedImage = ImageUtilities.toBufferedImage(frame.getImageFrame.getImage.asInstanceOf[Image])
    if (currentImage == null) {
      return
    }
    val imageWidth: Int = currentImage.getWidth
    val imageHeight: Int = currentImage.getHeight
    val maxDiameter: Double = imageHeight * REAL_SCALE_INITIAL
    val imageCenterX: Int = imageWidth / 2
    val imageCenterY: Int = imageHeight / 2
    val min: DoublePoint2D = Points.minimum(points.asJava)
    val max: DoublePoint2D = Points.maximum(points.asJava)
    var scale: Double = REAL_SCALE_MAX
    log.debug("Scaling Galpoints to fit into image")
    val centroid: DoublePoint2D = Points.centroid(points)
    val galDiameter: Double = min.distance(max)
    scale = maxDiameter / galDiameter
    val adjustedPoints = new mutable.ListBuffer[GALPoint]
    for (point <- points) {
      val newX: Double = (point.getX - centroid.getX)
      val newY: Double = (point.getY - centroid.getY)
      adjustedPoints += new GALPoint(point, newX, newY)
    }

    val gp = frame.getPointGlassPane
    gp.setPoints(adjustedPoints.asJava)
    gp.translate(imageCenterX, imageCenterY)
    gp.scale(scale)
    val xSlider: JSlider = frame.getMoveXSlider
    xSlider.setMinimum(0)
    xSlider.setMaximum(imageWidth)
    xSlider.setValue(imageCenterX)
    val ySlider: JSlider = frame.getMoveYSlider
    ySlider.setMinimum(0)
    ySlider.setMaximum(imageHeight)
    ySlider.setValue(imageCenterY)
    val scaleSlider: JSlider = frame.getScaleSlider
    scaleSlider.setValue(realScaleToUiScale(scale))
  }

  /**
   * Convert the tick values from the scale slider to the real change in scale
   *
   * @param uiScale
   * @return
   */
  def uiScaleToRealScale(uiScale: Int): Double = {
    uiScale / (REAL_SCALE_MAX - REAL_SCALE_MIN) * REAL_SCALE_INTERVAL;
  }

  /**
   * Convert a scale value to the correct tick value used by the scale slider
   *
   * @param realScale
   * @return
   */
  def realScaleToUiScale(realScale: Double): Int = {
    math.round(realScale / REAL_SCALE_INTERVAL * (REAL_SCALE_MAX - REAL_SCALE_MIN)).toInt;
  }


  /**
   * Display the select filter image
   * @param imageUrl
   */
  def renderImageFile(imageUrl: URL) {
    val imageFrame = frame.getImageFrame
    imageFrame.setImageUrl(imageUrl)
    imageFrame.setVisible(true)
  }

  /**
   * When a new filter image is selected we need to preserve the relative
   * position and scale of the GAL points and translate them to the new image.
   * This is useful since sometimes the ESP camera returns different size
   * images for the same filter when bracketing exposures. This helps the
   * use avoid having to realign the gal points if the image size changes.
   *
   * @param oldImage
   * @param newImage
   */
  def updateGalOnNewImage(oldImage: BufferedImage, newImage: BufferedImage ) {

    log.debug("Updating GAL position")

    if (newImage == null) {
      return
    }

    /*
     * UPdate UI controls for the new iamge
     */
    val newWidth = newImage.getWidth
    val newHeight = newImage.getHeight


    /*
     * Gets statistics from the old image. If they're the same as the new
     * image we won't adjust the GAL points
     */
    var oldWidth = Integer.MAX_VALUE
    var oldHeight = Integer.MAX_VALUE
    if (oldImage != null) {
      oldWidth = oldImage.getWidth
      oldHeight = oldImage.getHeight
    }

    if (oldWidth != newWidth && oldHeight != newHeight) {

      // Move GAL points to center of the new image
      val gp = frame.getPointGlassPane
      var cx = newWidth / 2
      var cy = newHeight / 2

      // Instead of centering it in the image. Try to position the gal center at the same relative position in the image
      if (oldImage != null) {
        val tranlation = gp.getTranslation
        cx = tranlation.getX() * newWidth / oldWidth
        cy = tranlation.getY() * newHeight / oldHeight
      }

      val xSlider = frame.getMoveXSlider
      xSlider.setMinimum(0)
      xSlider.setMaximum(newWidth)
      xSlider.setValue(cx)

      val ySlider = frame.getMoveYSlider
      ySlider.setMinimum(0)
      ySlider.setMaximum(newHeight)
      ySlider.setValue(cy)


      /*
       * Calculate Scale  based on current image size.
       */
      val points = gp.getPoints
      if (points != null && points.size() > 0) {

        val scale = gp.getScale * newHeight / oldHeight

        log.debug("\nMoving GAL: \n\tTranslation = [" + cx + ", " + cy + "]\n\tScale = " + scale)
        gp.translate(cx, cy)
        gp.scale(scale)

        frame.getScaleSlider.setValue(realScaleToUiScale(scale))
      }

    }
    else {
      // TODO pick some sensible fall back values
    }
  }

  /**
   * Handles messy code for adding a GAL File to the selection view
   */
  def addGalFile() {
    /*
     * Read save directory from preferences
     */
    val openDirectory = preferences.get(GAL_DIR_PREFERENCE, System.getProperty("user.home"))

    // Show open URL panel
    val fileDialog = new FileDialog(frame, "ESP - Add Gene Array Layout File", FileDialog.LOAD)
    fileDialog.setDirectory(openDirectory)
    fileDialog.setVisible(true)

    if (fileDialog.getFile != null) {
      val directory = fileDialog.getDirectory
      preferences.put(GAL_DIR_PREFERENCE, directory)
      val file = fileDialog.getFile
      val galFile = new File(directory, file)

      // Get url from panel and add it to the list
      try {
        val galUrl = galFile.toURI.toURL
        frame.getGalListModel.addElement(galUrl)
      }
      catch {
        case ex: MalformedURLException => {
          log.info("Bad URL!! " + galFile.getAbsolutePath)
          EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, ex)
        }
      }

    }
    fileDialog.dispose()
  }

  /**
   * This method does the analysis for the image when the user clicks the
   * <i>Analyze Image</i> button
   *
   * @param imageURL The URL of the filter image
   * @param image The actual filter image
   * @param points The correctly aligned GALpoints
   * @param spotDiameter Spot Diameter
   * @param blockSize Block size to average across
   */
  def accept(imageURL: URL, image: BufferedImage,  points : util.List[GALPoint],
      spotDiameter: Int, blockSize: Int) {

    if (!saveDirectory.exists()) {
      throw new ImagingServiceException("The directory " + saveDirectory.getAbsolutePath() + " does not exist. Unable to save the results!");
    }

    val radius = spotDiameter / 2F
    val radiusInt = math.ceil(radius).toInt

    frame.getAcceptButton.setEnabled(false)

    frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR))

    /*
     * ---- Step 1: Convert Java image to IJ so it's easier to work with
     */
    val filterImagePlus = new ImagePlus("Filter", image) // Intensity source

    /*
     * ---- Step 2: Extract the intensities for each spot. Due to interactions
     * with the Image class and the Swing classes. We'll do this on the
     * SwingEventDispatch thread to avoid any sideeffects. This isn't
     * ideal since it hangs the UI but oh well.
     */
    val galPoints = if (SystemUtilities.isWindowsOS) {
      // HACK: Windows is off-setting the x translation by about half the block size. Don't know why!
      points.asScala.map(gp => new GALPoint(gp, gp.x - spotDiameter / 2D, gp.y))
    }
    else points.asScala
    val map = GeneArrayAnalysisService.extractIntensities(filterImagePlus.getProcessor, galPoints, radius, blockSize);

    /*
     * ---- Step 3: Generate output.
     */
    val parts = imageURL.getFile.split("/")
    val name = parts(parts.length - 1).split("\\.")
    val now = new Date()
    val nowString = dateFormat.format(now)
    val dataFile = new File(saveDirectory, name(0) + "-" + nowString + ".txt")
    val imageFile = new File(saveDirectory, name(0) + "-" + nowString + ".png")
    val galFile = new File(saveDirectory, name(0) + "-GAL-" + nowString + ".txt")


    val dataThread = new Thread(new Runnable() {

      def run() {
        val intensityData = GeneArrayAnalysisService.createIntensityData(filterImagePlus.getProcessor, map, radiusInt,  blockSize);

        // Data output
        try {
          val writer = new BufferedWriter(new FileWriter(dataFile))
          writer.write(intensityData)
          writer.close()
        }
        catch {
          case e: Exception => {
            log.warn("Failed to write data to " + dataFile, e)
            EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, e)
          }

            // GAL output
            try {
              geneArrayIOService.write(galFile, points.asScala.toList)
            }
            catch {
              case e:Exception => {
                log.warn("Failed to write GAL points to " + galFile.getAbsolutePath)
                EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC)
              }
            }
        }
      }
    })
    dataThread.start()

    // Image output
    val intensityImage = GeneArrayAnalysisService.createIntensityImage(filterImagePlus.getProcessor, map, blockSize)
    val imageThread = new Thread(new Runnable() {

      def run() {
        try {
          log.debug("Writing image to " + imageFile.getAbsolutePath)
          ImageIO.write(intensityImage, "png", imageFile)
        }
        catch {
          case e: IOException => {
            log.warn("Failed to write image to " + imageFile, e)
            EventBus.publish(Lookup.NONFATAL_ERROR_TOPIC, e)
          }
        }
      }
    });
    imageThread.start()

    // Display image
    SwingUtilities.invokeLater(new Runnable() {
      def run() {
        val imageFrame = new JImageFrame(intensityImage)
        imageFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE)
        imageFrame.setVisible(true)
      }
    })


    frame.getAcceptButton.setEnabled(true)
    frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR))

  }

  def removeGalFiles() {
    val selectedGalFiles = frame.getGalList.getSelectedValues
    for (gal <- selectedGalFiles) {
      frame.getGalListModel().removeElement(gal);
    }
  }

  def removeImageFiles() {
    val selectedImageFiles = frame.getImageList.getSelectedValues
    for (image <- selectedImageFiles) {
      frame.getImageListModel.removeElement(image)
    }
  }


  def selectSaveDirectory {
    val status = saveDirFileChooser.showSaveDialog(frame)
    if (JFileChooser.APPROVE_OPTION == status) {
      saveDirectory = saveDirFileChooser.getSelectedFile
      preferences.put(SAVE_DIRECTORY_PREFERENCE, saveDirectory.getAbsolutePath)
    }
  }




}
