package org.mbari.esp.imaging.ui

import java.io.File
import javax.imageio.ImageIO
import javax.inject.Inject
import javax.swing.{WindowConstants, JFrame}
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, GeneArrayIOService}
import scala.collection.JavaConverters._

/**
 *
 * @author Brian Schlining
 * @since 2013-04-30
 */
class LayoutViewerController(@Inject geneArrayIOService: GeneArrayIOService) {

  def generateImage(inputFile: File, outputImage: File) {
    val mapPoints = GeneArrayAnalysisService.invertX(geneArrayIOService.read(inputFile.toURI.toURL))

    val image = GeneArrayAnalysisService.toBufferedImage(mapPoints, showLegend = true, 100, 20, None)

    /*
     * Get the file extension to determine the image type. Split on '.'
     * Remember Java Regex is an escape nightmare so we to double escape
     * it
     */
    val fileParts = outputImage.getName().split("\\.")
    val ext = fileParts(fileParts.length - 1)
    ImageIO.write(image, ext, outputImage);
  }

  def showLayout(layoutFile: File, showSingle: Boolean) {
    val points = {
      val ps = geneArrayIOService.read(layoutFile.toURI.toURL)
      val ps2 = if (showSingle) GeneArrayAnalysisService.filterSingleSpot(ps, 12.5 * 1000) else ps
      ps2.sortBy(_.sourceWell)
    }

    val scale = if (showSingle) 25 else 100

    val viewer = new GALPointViewer(points.asJava, scale, geneArrayIOService)
    viewer.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    viewer.pack();
    viewer.setVisible(true);

  }

}
