package org.mbari.esp.ia

import java.io.File
import org.junit.{Ignore, Test}
import java.awt.{Color, Graphics2D}
import ij.process.{ImageProcessor}
import org.mbari.esp.ia.services.{ToolBox, GeneArrayAnalysisService}
import org.mbari.esp.ia.imglib._

/**
 *
 * @author Brian Schlining
 * @since 2012-03-08
 */

class AnalysisWalkthrough {

    private[this] val filebase = getClass.getSimpleName


    @Test
    def walkthrough() {
        // !!! GAL ---------------------------------------------------------------------------------
        // --- Read the GAL file
        val galIO = ToolBox.geneArrayIOService
        val galUrl = getClass.getResource("/test06/101215 hab5 wcomp_2011-02-04_193203_Run.txt")
        val galPoints = galIO.read(galUrl)
        // TODO draw image

        // --- Isolate a single filter from the GAL points
        val oneFilterPoints = GeneArrayAnalysisService.filterSingleSpot(galPoints, 12500)
        // TODO draw image

        // --- Isolate the points that define the fiducials
        val fiducials = oneFilterPoints.filter( _.sourceWell.equalsIgnoreCase("K-1"))

        // !!! Spot Segmentation -------------------------------------------------------------------
        // --- Read the image
        val imageIO = ToolBox.imageIOService
        val imageUrl = getClass.getResource("/test06/hab11feb0920h40s.tif")
        val imagePlus = imageIO.read(imageUrl)
        imageIO.write(imagePlus, new File("target", filebase + "1-original.png"))

        // --- Reusable functions
        val thresholdFn = To8BitGrayFn andThen MaximumEntropyThresholdFn
        val floodFillWMinFn = FloodFillWithMinimumFn(5 , _: ImageProcessor)
        val removeFilterFn = RemoveNonFilterAreaFn(0.75, _: ImageProcessor)
        val floodFillFn = To16BitGrayFn andThen removeFilterFn andThen floodFillWMinFn

        // --- Process raw image
        // Threshold the image to remove noise an disolate spots.
        val thresheldRawProcessor = thresholdFn(imagePlus.getProcessor)
        imageIO.write(thresheldRawProcessor, new File("target", filebase + "2-raw-thresheld.png"))

        // Flood fill the image to ID each spot
        val floodFilledRawProcessor = floodFillFn(thresheldRawProcessor)
        imageIO.write(floodFilledRawProcessor, new File("target", filebase + "3-raw-floodfilled.png"))

        // Extract the spot regions
        val regionsRaw = SpotRegionsFn(imagePlus.getProcessor, floodFilledRawProcessor)

        // --- Autocontrast image
        // Threshold the image to remove noise an disolate spots.
        val autoThresheldFn = new AutoContrastFn(0.90) andThen thresholdFn
        val thresheldACProcessor = autoThresheldFn(imagePlus.getProcessor)
        imageIO.write(thresheldACProcessor, new File("target", filebase + "4-ac-thresheld.png"))

        // Flood fill the image to ID each spot
        val floodFilledACProcessor = floodFillFn(thresheldACProcessor)
        imageIO.write(floodFilledACProcessor, new File("target", filebase + "5-ac-floodfilled.png"))
        val regionsAC = SpotRegionsFn(imagePlus.getProcessor, floodFilledACProcessor)
        
        // --- Dynamic contrast
        val dynThresholdFn = new AutoContrastFn(0.90) andThen DynamicFuzzyContrastFn andThen thresholdFn
        val thresheldDynProcessor = dynThresholdFn(imagePlus.getProcessor)
        imageIO.write(thresheldDynProcessor, new File("target", filebase + "6-dyn-thresheld.png"))

        val floodFilledDynProcessor = floodFillFn(thresheldDynProcessor)
        imageIO.write(floodFilledDynProcessor, new File("target", filebase + "7-dyn-floodfilled.png"))
        val regionsDyn = SpotRegionsFn(imagePlus.getProcessor, floodFilledACProcessor)


        // --- Draw regions on final image
        val regions = regionsRaw ++ regionsAC ++ regionsDyn
        regions.toSeq.sortBy(_.label).foreach { r =>
            val (e, a0, a1) = r.eccentricity()
            println("Label:" + r.label + ", n:" + r.points.size + ", centroid:" + r.centroid +
                    ", eccentricity:" + e + ", majoraxis:" + a0 + ", minoraxis:" + a1 +
                    ", orientation:" + r.orientation.toDegrees)
        }

        val colorImage = ToRGBFn(imagePlus.getProcessor).getBufferedImage
        val ellipses = regions.filter(_.eccentricity()._1 < 3).map(_.ellipse())
        val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]
        g2.setPaint(Color.RED)
        ellipses.foreach(g2.draw(_))
        imageIO.write(colorImage, new File("target", filebase + "8-regions.png"))
    }


}
