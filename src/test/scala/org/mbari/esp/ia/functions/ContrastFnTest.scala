package org.mbari.esp.ia.functions

import org.junit.{Ignore, Test}
import org.mbari.esp.ia._
import java.awt.{Color, Graphics2D}
import java.io.File
import org.mbari.esp.ia.services.ToolBox
import org.mbari.esp.ia.imglib._

/**
 *
 * @author Brian Schlining
 * @since 2012-03-14
 */

class ContrastFnTest {

    private[this] val imageIO = ToolBox.imageIOService
    private[this] val filebase = getClass.getSimpleName

    val images = Iterable("/test06/hab11feb0914h40s.tif",
        "/test06/hab11feb0920h40s.tif",
        "/test07/hab11may0415h2000ml40s.tif",
        "/test07/hab11may0514h2000ml40s.tif",
        "/test08/bac11jun0517h2000ml40s.tif",
        "/test08/pcr11jun0422h2000ml40s.tif",
        "/test08/pcr11jun2520h2000ml160s.tif",
        "/test08/pcr11jun2520h2000ml40s.tif")

    val fns = Map("ac" -> new AutoContrastFn(0.9),
        "norm" -> new AutoContrastFn(0.9, true),
        "dyn" -> DynamicFuzzyContrastFn,
        "fuzz" -> FuzzyContrastFn,
        "g1" -> new GammaContrastFn(0.3),
        "g2" -> new GammaContrastFn(0.5),
        "log" -> LogContrastFn,
        "s" -> new StretchContrastFn(50, 1, 200, ProcessorType.ByteType.maxValue.toInt))

    @Ignore
    @Test
    def test01() {
        images.foreach { img =>
            fns.foreach { case (prefix, fn) =>
                val url = getClass.getResource(img)
                val imagePlus = imageIO.read(url)
                val newImage = fn(To8BitGrayFn(imagePlus.getProcessor))
                imageIO.write(newImage, new File("target", filebase + "-" + prefix +
                        img.replace('/', '_').replace('.', '_') + ".png"))
            }
        }
    }

}
