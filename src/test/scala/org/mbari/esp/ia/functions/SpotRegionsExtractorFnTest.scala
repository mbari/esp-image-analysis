package org.mbari.esp.ia.functions

import org.junit.Test
import org.mbari.esp.ia.services.ToolBox
import java.io.File
import java.awt.{Graphics2D, Color}
import org.mbari.esp.ia.imglib.{ToRGBFn, SpotRegionsExtractorFn}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-13
 */

class SpotRegionsExtractorFnTest {
    
    private[this] val imageIO = ToolBox.imageIOService
    private[this] val filebase = getClass.getSimpleName

    @Test
    def testWithTonsOfImages() {
        val images = Iterable("/test06/hab11feb0914h40s.tif",
            "/test06/hab11feb0920h40s.tif",
            "/test07/hab11may0415h2000ml40s.tif",
            "/test07/hab11may0514h2000ml40s.tif",
            "/test08/bac11jun0517h2000ml40s.tif",
            "/test08/pcr11jun0422h2000ml40s.tif",
            "/test08/pcr11jun2520h2000ml160s.tif",
            "/test08/pcr11jun2520h2000ml40s.tif")
        
        images.foreach { img =>
            val url = getClass.getResource(img)
            val imagePlus = imageIO.read(url)
            val regions = SpotRegionsExtractorFn(imagePlus.getProcessor).filter(_.eccentricity()._1 < 3)
            val colorImage = ToRGBFn(imagePlus.getProcessor).getBufferedImage
            val ellipses = regions.map(_.ellipse())
            val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]
            g2.setPaint(Color.RED)
            ellipses.foreach(g2.draw(_))
            imageIO.write(colorImage, new File("target", filebase + img.replace('/', '_').replace('.', '_') + ".png"))
        }

    }

}
