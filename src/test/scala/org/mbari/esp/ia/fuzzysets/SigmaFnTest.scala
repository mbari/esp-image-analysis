package org.mbari.esp.ia.fuzzysets

import org.junit.Assert._
import org.junit.Test

/**
 *
 * @author Brian Schlining
 * @since 2012-03-13
 */

class SigmaFnTest {

    val tolerance = 0.000001

    @Test
    def test01() {
        val fn = new SigmaFn(1, 3)
        assertEquals(0, fn(0), tolerance)
        assertEquals(0, fn(1), tolerance)
        assertEquals(0.5, fn(2), tolerance)
        assertEquals(1, fn(3), tolerance)
        assertEquals(1, fn(4), tolerance)
    }

    @Test
    def test02() {
        val fn = new SigmaFn(3, 1)
        assertEquals(1, fn(0), tolerance)
        assertEquals(1, fn(1), tolerance)
        assertEquals(0.5, fn(2), tolerance)
        assertEquals(0, fn(3), tolerance)
        assertEquals(0, fn(4), tolerance)
    }

}
