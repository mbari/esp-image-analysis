package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D
import PointChromosomeExt.extendChromosome // implicit extension
import scala.math._
/**
 *
 * @author Brian Schlining
 * @since 2012-03-27
 */

class AddNoiseMutationTest {
    
    val noiseRange = 100


    @Test
    def test() {
        val c = PointChromosome(Point2D(-200, 0), Point2D(10, 100), Point2D(100, 10))
        val addNoise = new AddNoiseMutation(noiseRange)
        val nc = addNoise(c)
        assertNotSame("Could be a freak random chance. But the chromosomes were still the same " +
                "after noise was added", c, nc)

        // Points should be within the expected noise range
        val cp = c.points
        val ncp = nc.points
        for (i <- 0 until cp.size) {
            val dx = abs(cp(i).x - ncp(i).x)
            assertTrue(dx <= noiseRange)
            val dy = abs(cp(i).y - ncp(i).y)
            assertTrue(dy <= noiseRange)
        }
    }
}
