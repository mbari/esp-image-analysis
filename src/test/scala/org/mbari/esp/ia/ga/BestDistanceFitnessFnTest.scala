package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D

/**
 *
 * @author Brian Schlining
 * @since 2012-03-27
 */

class BestDistanceFitnessFnTest {

    val tolerance = 0.00000001


    @Test
    def test() {
        val fitnessFn = new BestDistanceFitnessFn(3)
        val target = Array.tabulate(10) { i => Point2D(i * 10D, 0D, i)}.toSeq
        assertEquals("Should have been fit..but wasn't", 1D, fitnessFn(target, target), tolerance)
        val badSourceBig = Array.tabulate(10) { i => Point2D(i * 100D, i * 20D, i) }.toSeq
        assertEquals("Should have been dead (unfit) ..but wasn't", 0D, fitnessFn(badSourceBig, target), tolerance)
        val badSourceSmall = Array.tabulate(10) { i => Point2D(i * 0.5, 0D, i) }.toSeq
        assertEquals("Should have been dead (unfit) ..but wasn't", 0D, fitnessFn(badSourceSmall, target), tolerance)
        val goodSource = Array.tabulate(100) { i => Point2D(i * i.toDouble,  i * i.toDouble,  i)}.toSeq
        assertEquals("Should have been fit..but wasn't", 1D, fitnessFn(goodSource, target), tolerance)
    }

}
