package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import java.math.BigInteger

/**
 *
 * @author Brian Schlining
 * @since 2012-03-27
 */

class ChromosomeTest {

    @Test
    def testConstructor() {
        val c = Chromosome(BigInt(0), 16)
        for (i <- 0 until 16) {
            assertTrue("Bit no." + i + "was not 0...whoops! Chromosome was " + c, !c.test(i))
        }


        val c4 = Chromosome(BigInt(4), 16)
        assertTrue("Bit was not set as expected. Chromosome was " + c4, c4.test(2))

    }

    @Test
    def testOperators() {
        val c = Chromosome(BigInt(0), 16)
        val c1 = c.flip(0)
        assertTrue("Gene flip did not work", c1.test(0))
        assertTrue("Gene was expected to be 1 but it wasn't. Gene = " + c1.genes, c1.genes == BigInt(1))

        val c2 = c1.clear(0)
        assertTrue("Clear gene did not work", !c2.test(0))
        assertTrue("Gene was expected to be 0 but it wasn't. Gene = " + c2.genes, c2.genes == BigInt(0))

        val c3 = c.set(1)
        assertTrue("Set gene did not work", c3.test(1))
        assertTrue("Gene was expected to be 2 but it wasn't. Gene = " + c3.genes, c3.genes == BigInt(2))

    }

    @Test
    def testToString() {
        val n = 36
        val c = Chromosome(BigInt(10), n)
        val s = c.toString
        assertEquals("String was the wrong length", n, s.size)
    }

    @Test
    def testEquals() {
        val c1 = Chromosome(BigInt(10), 16)
        val c2 = Chromosome(BigInt(10), 16)
        assertEquals(c1, c2)

        val c3 = Chromosome(BigInt(11), 16)
        assertFalse(c1.equals(c3))
    }

}
