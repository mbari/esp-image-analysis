package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.{Point2D, LabeledDoublePoint2D}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-28
 */

class FitnessFnChainTest {

    private[this] val tolerance = 0.00000001

    @Test
    def test() {

        // --- Setup
        val imageDimension = 100D
        
        val alwaysFailFn = new FitnessFn {
            def apply(v1: Iterable[LabeledDoublePoint2D], v2: Iterable[LabeledDoublePoint2D]): Double = 0
        }
        
        val alwaysPassFn = new FitnessFn {
            def apply(v1: Iterable[LabeledDoublePoint2D], v2: Iterable[LabeledDoublePoint2D]): Double = 1
        }
        
        val imageBoundsFn = new ImageBoundsFitnessFn(imageDimension, imageDimension)
        val shouldBeZero = new FitnessFnChain(alwaysFailFn, imageBoundsFn)
        val passToImageBounds = new FitnessFnChain(alwaysPassFn, imageBoundsFn)
        
        val goodPoints = Array.tabulate(9) { i => 
            val j = i * 10D
            Point2D(j, j, i)
        }

        val badPoints = goodPoints :+ Point2D(imageDimension + 10, imageDimension / 2, 100)
        
        // --- Test. Remember for this test the 2nd set of points is not used by the fitness fn.

        // check our anonymous fns first
        assertEquals(0D, alwaysFailFn(goodPoints, Nil), tolerance)
        assertEquals(1D, alwaysPassFn(goodPoints, Nil), tolerance)

        // Test the FitnessFnChain
        assertEquals(0D, shouldBeZero(goodPoints, Nil), tolerance)
        assertEquals(0D, shouldBeZero(badPoints, Nil), tolerance)

        assertEquals(1D, passToImageBounds(goodPoints, Nil), tolerance)
        assertEquals(0D, passToImageBounds(badPoints, Nil), tolerance)

    }

}
