package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test



/**
 *
 * @author Brian Schlining
 * @since 2012-03-28
 */

class FitnessTest {

    @Test
    def testEquals() {
        val c1 = Chromosome(BigInt(10), 16)
        val c2 = Chromosome(BigInt(11), 16)
        
        val f1 = Fitness(c1, 10)
        val f2 = Fitness(c1, 10)
        assertTrue(f1 == f2)
        
        val f3 = Fitness(c2, 10)
        assertFalse(f1 == f3)
        
        val f4 = Fitness(c1, 11)
        assertFalse(f1 == f4)

    }

}
