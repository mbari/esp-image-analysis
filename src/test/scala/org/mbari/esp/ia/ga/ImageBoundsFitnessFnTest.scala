package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D

/**
 *
 * @author Brian Schlining
 * @since 2012-03-28
 */

class ImageBoundsFitnessFnTest {

    private[this] val tolerance = 0.00000001
    
    @Test
    def test() {
        
        val goodArray = Array.tabulate(12) { i => Point2D(i, i, i).toDouble }
        val badArray = goodArray :+ Point2D(100D, 1000D, 1000)

        val fitnessFn = new ImageBoundsFitnessFn(400, 400)
        assertEquals(1D, fitnessFn(goodArray, Nil), tolerance) // only checks 1st array arg

        assertEquals(0D, fitnessFn(badArray, Nil), tolerance)   // only checks 1st array arg

    }

}
