package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D

/**
 *
 * @author Brian Schlining
 * @since 2012-03-28
 */

class PointChromosomeExtTest {

    @Test
    def testRoundTripConstruction() {

        import PointChromosomeExt.extendChromosome

        val p0 = Point2D(0, 0)
        val p1 = Point2D(10, 0)
        val p2 = Point2D(0, 10)

        val c = PointChromosome(p0, p1, p2)

        val points = c.points

        assertEquals(p0, points(0))
        assertEquals(p1, points(1))
        assertEquals(p2, points(2))


    }

}
