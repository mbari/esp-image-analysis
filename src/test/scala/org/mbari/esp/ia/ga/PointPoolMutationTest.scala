package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D

/**
 *
 * @author Brian Schlining
 * @since 2012-03-28
 */

class PointPoolMutationTest {

    @Test
    def test() {

        import PointChromosomeExt.extendChromosome

        val point = Point2D(100, 100)
        val pointPool = Seq(point)
        val mutate = new PointPoolMutation(pointPool)
        val p = Point2D(0, 0)
        val chromosome = PointChromosomeExt.toChromosome(p, p, p)
        val mutant = mutate(chromosome)

        val matchingPoint = mutant.points.filter( _ == point)
        assertEquals(1, matchingPoint.size)

    }

}
