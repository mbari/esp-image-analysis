package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.imglib.{ToRGBFn, SpotRegionsExtractorFn}
import java.awt.geom.Ellipse2D
import java.awt.{Color, Graphics2D}
import java.io.File
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}
import org.mbari.esp.ia.geometry.Point2D
import org.mbari.esp.ia.awt.RegionInfo

/**
 *
 * @author Brian Schlining
 * @since 2012-03-26
 */

class RigidTransformTest {

    private[this] val filebase = getClass.getSimpleName

    @Test
    def test() {

        import org.mbari.esp.ia.imglib.extendImageProcessor // implicit

        // --- Extract Fiducials from gals
        val galUrl = getClass.getResource("/test06/101215 hab5 wcomp_2011-02-04_193203_Run.txt")
        val points = GeneArrayAnalysisService.filterSingleSpot(ToolBox.geneArrayIOService.read(galUrl), 12500)
        val fiducials = points.filter(_.sourceWell == "K-1").sortWith { (a, b) =>
            val aby = a.y.compareTo(b.y)
            if (aby == 0) {
                a.x.compareTo(b.x) <= 0
            }
            else {
                aby < 0
            }
        } // Order from top
        assertEquals("Whoops, expected to find 3 fiducial markers", 3, fiducials.size)

        // --- Extract spots from image
        val imageUrl = getClass.getResource("/test06/hab11feb0914h40s.tif")
        val imagePlus = ToolBox.imageIOService.read(imageUrl)
        //        val spots = SpotRegionsExtractorFn(imagePlus.getProcessor).map(_.centroid())
        //        val spotFiducials = spots.filter(p => p.label >= 24 && p.label <= 26).toSeq.sortWith { (a, b) =>
        //            val aby = a.y.compareTo(b.y)
        //            if (aby == 0) {
        //                a.x.compareTo(b.x) <= 0
        //            }
        //            else {
        //                aby < 0
        //            }
        //        }.map(_.toInt)
        val spotFiducials = Seq(Point2D(347, 262, 1), Point2D(334, 385, 2), Point2D(300, 377, 3)).sortWith { (a, b) =>
            val aby = a.y.compareTo(b.y)
            if (aby == 0) {
                a.x.compareTo(b.x) <= 0
            }
            else {
                aby < 0
            }
        }

        // --- Calculate transform
        val galChromosome = PointChromosomeExt.toChromosome(fiducials.map(_.toLabeledPoint2D.toInt))
        val imgChromsome = PointChromosomeExt.toChromosome(spotFiducials)
        val transformedGalPoints = RigidTransform(galChromosome, imgChromsome, points.map(_.toLabeledPoint2D))

        // --- Draw results
        val colorImage = ToRGBFn(imagePlus.getProcessor).bufferedImage
        val results = RegionInfo(transformedGalPoints, Color.RED, 7)
        val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]
        g2.setPaint(Color.RED)
        results.foreach(r => g2.draw(r.shape))
        ToolBox.imageIOService.write(colorImage, new File("target", filebase + "-test01.png" ))

    }

}
