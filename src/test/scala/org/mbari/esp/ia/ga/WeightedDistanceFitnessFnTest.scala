package org.mbari.esp.ia.ga

import org.junit.Assert._
import org.junit.Test
import org.mbari.esp.ia.geometry.Point2D

/**
 *
 * @author Brian Schlining
 * @since 2012-03-29
 */

class WeightedDistanceFitnessFnTest {

    private[this] val tolerance = 0.000000001

    @Test
    def test() {
        val fn = new WeightedDistanceFitnessFn(10)
        val p0 = Array.tabulate(10) { i => Point2D(0D, i * 10D, i)}
        assertEquals(1D, fn(p0, p0), tolerance)

        // This point is more than 10 from it's nearest neighbor. fn should return 0
        val p1 = Seq(Point2D(0D, 111D, 111))
        assertEquals(0D, fn(p0, p1), tolerance)

        val p2 = p0 :+ Point2D(0D, 101D, 101)
        assertTrue(fn(p0, p2) > 0D)
    }

}
