package org.mbari.esp.ia.ga.impl1


import org.mbari.esp.ia.imglib.{ToRGBFn, SpotRegionsExtractorFn}
import java.awt.geom.{Ellipse2D}
import java.awt.image.BufferedImage
import java.awt.{Color, Graphics2D}
import java.io.File
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}
import org.junit.{Ignore, Test}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-26
 */

class ESPAutoAlignerTest {

  private[this] val filebase = getClass.getSimpleName

  @Ignore
  @Test
  def test() {


    val galName = "/test08/11may06_BAC_JDF_2011-05-19_153819_Run.txt"
    val imageName = "/test08/bac11may1611h40s.tif"
    val fiducialKey = "I-3"

    //        val galName = "/test06/101215 hab5 wcomp_2011-02-04_193203_Run.txt"
    //        val imageName = "/test06/hab11feb0920h40s.tif"
    //        val fiducialKey = "K-1"

    //        val galName = "/test07/110220hab5wcomp_2011-03-07_150545_Run.txt"
    //        val imageName = "/test07/hab11may0514h2000ml40s.tif"
    //        val fiducialKey = "K-1"

    val imageIO = ToolBox.imageIOService

    // --- Read GAL, extract fiducials
    val galUrl = getClass.getResource(galName)
    val galPoints = GeneArrayAnalysisService.filterSingleSpot(ToolBox.geneArrayIOService.read(galUrl), 12500)
    val fiducialPoints = galPoints.filter(_.sourceWell == fiducialKey)
    val galImage = GeneArrayAnalysisService.toBufferedImage(galPoints, true, 10, 10)
    imageIO.write(galImage, new File("target", filebase + "-test01-gal.png"))


    // --- Read the image. Extract array points
    val imageUrl = getClass.getResource(imageName)
    val imagePlus = imageIO.read(imageUrl)
    val spotRegions = SpotRegionsExtractorFn(imagePlus.getProcessor).filter(_.eccentricity()._1 < 3)
    val imagePoints = spotRegions.map(_.centroid.toInt).toSeq


    val gaStrategy = RigidGAStrategyFactory(100, 40, 0.25, imagePlus.getWidth,
      imagePlus.getHeight, imagePoints)

    val aligner = new ESPAutoAligner(gaStrategy, fiducialPoints, galPoints)
    val alignedGalPoints = aligner(imagePoints.map(_.toDouble))

    // --- Draw results
    val colorImage: BufferedImage = ToRGBFn(imagePlus.getProcessor).getBufferedImage
    val g2 = colorImage.getGraphics.asInstanceOf[Graphics2D]

    val extractedPoints = imagePoints.map(p =>
      new Ellipse2D.Double(p.x - 2, p.y - 2, 4, 4))
    g2.setPaint(Color.GREEN)
    extractedPoints.foreach(g2.draw(_))

    val results = alignedGalPoints.map(p =>
      new Ellipse2D.Double(p.x - 2, p.y - 2, 4, 4))
    g2.setPaint(Color.RED)
    results.foreach(g2.draw(_))

    imageIO.write(colorImage, new File("target", filebase + "-test01.png"))

  }

}
