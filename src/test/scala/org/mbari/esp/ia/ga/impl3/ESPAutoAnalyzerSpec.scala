package org.mbari.esp.ia.ga.impl3

import java.io.{FileWriter, BufferedWriter, File}

import org.junit.runner.RunWith
import org.mbari.esp.ia.services.{ToolBox, GeneArrayAnalysisService}
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

/**
 *
 *
 * @author Brian Schlining
 * @since 2015-08-06T11:26:00
 */
@RunWith(classOf[JUnitRunner])
class ESPAutoAnalyzerSpec extends FunSpec with Matchers {

  private[this] val galName = "/test13/HAB_Array_MBARI_15_Jul_09.gal"
  private[this] val imageName = "/test13/hab15jul1914h40s.tiff"
  private[this] val fiducialKey = "Alex comp"

  private[this] val filebase = getClass.getSimpleName


  describe(getClass.getSimpleName) {

    it ("should align a GAL to an image correctly") {

      val galUrl = getClass.getResource(galName)
      val imageUrl = getClass.getResource(imageName)

      val analyzer = new ESPImageAnalyzer(5, 3)
      val results = analyzer(galUrl, imageUrl, fiducialKey)
      val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
        results.intensities, results.blockSize)
      ToolBox.imageIOService.write(resultImage, new File("target", filebase + "-test.png"))

      val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
        results.intensities, results.spotRadius, results.blockSize)
      val writer = new BufferedWriter(new FileWriter(new File("target", filebase + "-test.txt")))
      writer.write(resultText)
      writer.close()

      val fiducialSpots = results.intensities.filter({ case (gp, p) =>  gp.sourceWell.equalsIgnoreCase(fiducialKey) })

      fiducialSpots.size should be (3)

      for ((gp, p) <- fiducialSpots) {
        p.z should be (65535D +- 100D)
      }


    }

  }

}
