package org.mbari.esp.ia.ga.impl3

import org.junit.Test
import org.mbari.esp.ia.services.{GeneArrayAnalysisService, ToolBox}

import java.io.{FileWriter, BufferedWriter, File}

/**
 *
 * @author Brian Schlining
 * @since 2012-05-16
 */

class ESPAutoAnalyzerTest {

    private[this] val filebase = getClass.getSimpleName

    @Test
    def test() {

        //                val galName = "/test08/11may06_BAC_JDF_2011-05-19_153819_Run.txt"
        //                val imageName = "/test08/bac11may1611h40s.tif"
        //                val fiducialKey = "I-3"

        //                val galName = "/test06/101215 hab5 wcomp_2011-02-04_193203_Run.txt"
        //                val imageName = "/test06/hab11feb0920h40s.tif"
        //                val fiducialKey = "K-1"

        //        val galName = "/test07/110220hab5wcomp_2011-03-07_150545_Run.txt"
        //        val imageName = "/test07/hab11may0514h2000ml40s.tif"
        //        val fiducialKey = "K-1"

//        val galName = "/test08/11may06_BAC_JDF_2011-05-19_153819_Run.txt"
//        val imageName = "/test08/pcr11jun2520h2000ml40s.tif"
//        val fiducialKey = "I-3"

        val galName = "/test08/11may06_BAC_JDF_2011-05-19_153819_Run.txt"
        val imageName = "/test08/pcr11jun0700h2000ml40s.tif"
        val fiducialKey = "I-3"

        val galUrl = getClass.getResource(galName)
        val imageUrl = getClass.getResource(imageName)

        val analyzer = new ESPImageAnalyzer(5, 3)
        val results = analyzer(galUrl, imageUrl, fiducialKey)
        val resultImage = GeneArrayAnalysisService.createIntensityImage(results.imageProcessor,
            results.intensities, results.blockSize)
        ToolBox.imageIOService.write(resultImage, new File("target", filebase + "-test.png"))

        val resultText = GeneArrayAnalysisService.createIntensityData(results.imageProcessor,
            results.intensities, results.spotRadius, results.blockSize)
        val writer = new BufferedWriter(new FileWriter(new File("target", filebase + "-test.txt")))
        writer.write(resultText)
        writer.close()

    }
}
