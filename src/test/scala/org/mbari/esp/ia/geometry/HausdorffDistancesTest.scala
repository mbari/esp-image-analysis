package org.mbari.esp.ia.geometry

import org.junit.Assert._
import org.junit.{Test, Before}
import scala.math.random

/**
 *
 * @author Brian Schlining
 * @since 2012-03-27
 */

class HausdorffDistancesTest {

    var xPoints: Seq[LabeledDoublePoint2D] = _
    var yPoints: Seq[LabeledDoublePoint2D] = _
    val range = 1000
    val noise = 10D
    val diameter = 3
    val distanceFn = new HausdorffDistanceFn[Double](new EuclideanDistanceFn[Double], 1D, 1D)

    private def generateValue() = random * range

    private def generateNoise() = random * noise * 2 - noise

    @Before
    def init() {
        // offset 2 points by enough that they will always represent the largest hausdorff distance
        val x = Point2D(range + 2 * noise, range + 2 * noise, 50)
        val y = Point2D(range + 4 * noise, range + 4 * noise, 51)
        xPoints = rand(40) :+ x
        yPoints = addNoise(xPoints) :+ y

    }

    private def rand(n: Int): Seq[LabeledDoublePoint2D] = {
        Array.tabulate(n) { i =>
            Point2D(generateValue(), generateValue(), i)
        }.toSeq
    }

    private def addNoise(points: Seq[LabeledDoublePoint2D]): Seq[LabeledDoublePoint2D] =
        points.map { p => Point2D(p.x + generateNoise(), p.y + generateNoise(), p.label) }

    @Test
    def test01() {
        val hd = distanceFn(xPoints, yPoints)
        assertEquals("Wrong point was selected", 50, hd.p0.label )
        assertEquals("Wrong point was selected", 51, hd.p1.label)
    }

    @Test
    def test02() {
        // Add 2x as many points as test01
        xPoints = xPoints ++ rand(40)
        val hd = distanceFn(xPoints, yPoints)
        assertEquals("Wrong point was selected", 50, hd.p0.label)
        assertEquals("Wrong point was selected", 51, hd.p1.label)

    }


}
