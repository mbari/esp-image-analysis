package org.mbari.esp.ia.geometry

import org.junit.Assert._
import org.junit.Test

/**
 *
 * @author Brian Schlining
 * @since 2012-03-30
 */

class ToPointPairsFnTest {

    @Test
    def test() {

        // -- Create partially applied function for testing
        val calculation = (a: LabeledDoublePoint2D, b: LabeledDoublePoint2D) => 0D
        val fn = new ToPointPairsFn[Double].apply(calculation, _ : Iterable[LabeledDoublePoint2D])


        val p0 = Seq(Point2D(0D, 0D, 1), Point2D(0D, 1D, 2))
        val p0p0 = fn(p0)
        assertEquals(1, p0p0.size)

    }

}
