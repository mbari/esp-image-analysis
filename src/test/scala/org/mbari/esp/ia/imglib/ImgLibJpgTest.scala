package org.mbari.esp.ia.imglib

import io.scif.img.ImgOpener
import net.imglib2.converter.RealARGBConverter
import org.junit.Test
import org.junit.Assert._
import net.imglib2.img.display.imagej.ImageJFunctions
import org.slf4j.LoggerFactory
import org.mbari.net.URLUtilities
import net.imglib2.img.array.ArrayImgFactory
import net.imglib2.`type`.numeric.ARGBType
import net.imglib2.`type`.numeric.integer.{UnsignedByteType}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-19
 */

class ImgLibJpgTest {

    private[this] val image = "/brian-schlining.jpeg"
    private[this] val log = LoggerFactory.getLogger(getClass)

    @Test
    def test() {
        // Read image
        val io = new ImgOpener
        val imagePath = URLUtilities.toFile(getClass.getResource(image)).getAbsolutePath
        val imgPlus = io.openImgs(imagePath, new ArrayImgFactory[UnsignedByteType])
        log.debug("ImgPlus:\n" + DebugInspector.toString(imgPlus))
        //assertEquals(3, imgPlus.size())


        // --- Convert o ImgPlus[ShortType] => RGB
//        val width: Long = imgPlus.max(0).toInt + 1
//        val height: Long = imgPlus.max(1).toInt + 1
//        val colorImgFactory = new ArrayImgFactory[ARGBType]
//        val colorImg = colorImgFactory.create(Array(width, height, 3L), new ARGBType())
//        val projector = new XYProjector[UnsignedByteType, ARGBType](imgPlus, colorImg,
//            new RealARGBConverter[UnsignedByteType])
//        projector.map() // Side-effect!! Yuck.
//        log.debug(DebugInspector.toString(colorImg))
//
//        // --- Convert ImgPlus => ImagePlus
//        val imagePlus = ImageJFunctions.wrapUnsignedByte(imgPlus, "")
//        log.debug("ImagePlus:\n" + DebugInspector.toString(imagePlus))
//
//        // --- Convert ImagePlus => BufferedImage
//        val bufferedImage = imagePlus.getProcessor.getBufferedImage
//        log.debug("BufferedImage:\n" + DebugInspector.toString(bufferedImage))
    }


}
