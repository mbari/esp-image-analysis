package org.mbari.esp.ia.imglib

import org.junit.Test
import org.junit.Assert._
import org.mbari.net.URLUtilities
import net.imglib2.img.array.ArrayImgFactory
import net.imglib2.`type`.numeric.integer.ShortType
import org.slf4j.{LoggerFactory}
import net.imglib2.`type`.numeric.ARGBType
import net.imglib2.img.display.imagej.ImageJFunctions

/**
 *
 * @author Brian Schlining
 * @since 2012-03-19
 */

class ImgLibTiffTest {

    private[this] val image = "/test06/hab11feb0920h40s.tif"
    private[this] val log = LoggerFactory.getLogger(getClass)

    @Test
    def test() {
        // Read image
//        val io = new ImgOpener
//        val imagePath = URLUtilities.toFile(getClass.getResource(image)).getAbsolutePath
//        val imgPlus: ImgPlus[ShortType] = io.openImg[ShortType](imagePath, new ArrayImgFactory[ShortType])
//        log.debug("ImgPlus:\n" + DebugInspector.toString(imgPlus))
//        assertEquals(2, imgPlus.numDimensions())
//
//        // --- Convert o ImgPlus[ShortType] => RGB
//        val width: Long = imgPlus.max(0).toInt + 1
//        val height: Long = imgPlus.max(1).toInt + 1
//        val colorImgFactory = new ArrayImgFactory[ARGBType]
//        val colorImg = colorImgFactory.create(Array(width, height, 3L), new ARGBType())
//        val projector = new XYProjector[ShortType, ARGBType](imgPlus, colorImg,
//            new RealARGBConverter[ShortType])
//        projector.map() // Side-effect!! Yuck.
//        log.debug(DebugInspector.toString(colorImg))
//
//        // --- Convert ImgPlus => ImagePlus
//        val imagePlus = ImageJFunctions.wrapUnsignedShort(imgPlus, "")
//        log.debug("ImagePlus:\n" + DebugInspector.toString(imagePlus))
//
//        // --- Convert ImagePlus => BufferedImage
//        val bufferedImage = imagePlus.getProcessor.getBufferedImage
//        log.debug("BufferedImage:\n" + DebugInspector.toString(bufferedImage))


    }

}
