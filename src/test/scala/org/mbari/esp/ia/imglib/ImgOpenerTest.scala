package org.mbari.esp.ia.imglib

import io.scif.img.ImgOpener
import org.mbari.net.URLUtilities
import net.imglib2.img.array.ArrayImgFactory
import org.junit.{Test}
import org.slf4j.LoggerFactory
import net.imglib2.`type`.numeric.integer.{UnsignedByteType, ShortType}

/**
 *
 * @author Brian Schlining
 * @since 2012-03-15
 */

class ImgOpenerTest {

    val images = Iterable("/test06/hab11feb0920h40s.tif", "/brian-schlining.jpeg")
    val io = new ImgOpener
    private[this] val log = LoggerFactory.getLogger(getClass)


    @Test
    def testRead() {
        images.foreach {
            filename =>
                val imagePath = URLUtilities.toFile(getClass.getResource(filename))
                val imgPlus = io.openImg(imagePath.getAbsolutePath)
                log.debug("Read without specifying type:\n" + DebugInspector.toString(imgPlus))
        }
    }

    @Test
    def testReadAsShortType() {
        images.foreach {
            filename =>
                val imagePath = URLUtilities.toFile(getClass.getResource(filename))
                val imgPlus = io.openImg(imagePath.getAbsolutePath, new ArrayImgFactory[ShortType],
                    new ShortType)
                log.debug("Read as ShortType:\n" + DebugInspector.toString(imgPlus))
        }
    }

    @Test
    def testReadAsByteType() {
        images.foreach {
            filename =>
                val imagePath = URLUtilities.toFile(getClass.getResource(filename))
                val imgPlus = io.openImg(imagePath.getAbsolutePath, new ArrayImgFactory[UnsignedByteType],
                    new UnsignedByteType)
                log.debug("Read as ByteType:" + DebugInspector.toString(imgPlus))
        }
    }

}
