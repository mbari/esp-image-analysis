package org.mbari.esp.ia.imglib

import org.mbari.math.Statlib
import ij.process.{FloatProcessor, ImageProcessor}
import org.mbari.esp.ia.services.ToolBox
import java.io.File

/**
 *
 * @author Brian Schlining
 * @since 2012-05-15
 */

object LTFThrowaway {

    private[this] val name = getClass.getSimpleName

    def main(args: Array[String]) {
        val imageName = "/test08/pcr11jun2520h2000ml160s.tif"
        val imageUrl = getClass.getResource(imageName)
        val io = ToolBox.imageIOService
        val image = io.read(imageUrl).getProcessor
        val (meanP, stdP) = statistics(image, 1)
        io.write(meanP, new File(name +  "-mean.png"))
        io.write(stdP, new File(name +  "-std.png"))
        for (i <- 1 to 5; j <- 1 to 8) {
            val t = apply(image, i * 0.25 , j * 0.25, meanP, stdP)
            io.write(t, new File(name + "-" + i + "-" + j + "threshold.png"))
        }

    }


    private def statistics(image: ImageProcessor, k: Int) = {
        val w = image.width
        val h = image.height

        // Create a new image filled with black
        val stdP = new FloatProcessor(w, h)
        val meanP = new FloatProcessor(w, h)

        val length = (k + k + 1) * (k + k + 1)
        for (i <- k until (w - k); j <- k until (h - k)) {

            val buf = Array.ofDim[Double](length)
            for (u <- -k to k; v <- -k to k) {
                val idx = (u + k) + ((u + k) * (v + k))
                buf(idx) = image(i + u, j + v)
            }
            val std = Statlib.standardDeviation(buf)
            val mean = Statlib.mean(buf)
            stdP(i, j) = std.toFloat
            meanP(i, j) = mean.toFloat
        }

        (meanP, stdP)
    }


    private def apply(image: ImageProcessor, a: Double, b: Double, meanP: FloatProcessor,
            stdP: FloatProcessor) = {

        val w = image.width
        val h = image.height

        // Create a new image filled with black
        val to = image.duplicate()
        val t = ProcessorType.get(to)
        val black = t.minValue.toInt
        val white = t.maxValue.toInt
        for (i <- 0 until w; j <- 0 until h) {
            to(i, j) = black
        }

        for (i <- 0 until w; j <- 0 until h) {
            val pixel = image(i, j)
            to(i, j) = if (pixel > a * stdP.getf(i, j) && pixel > b * meanP.getf(i, j)) white else black
        }

        to

    }


}
