package org.mbari.esp.ia.imglib

import org.mbari.esp.ia.services.ToolBox
import java.io.File

/**
 *
 * @author Brian Schlining
 * @since 2012-05-15
 */

object StdFnDemo {

    private[this] val name = getClass.getSimpleName

    def main(args: Array[String]) {
        val imageName = "/test08/pcr11jun2520h2000ml160s.tif"
        val imageUrl = getClass.getResource(imageName)
        val io = ToolBox.imageIOService
        val image = io.read(imageUrl).getProcessor

        for (i <- 1 to 4) {
            val stdImage = StdFn(image, i)
            io.write(stdImage, new File(name + "-std-" + i + ".png"))
        }
    }

}
