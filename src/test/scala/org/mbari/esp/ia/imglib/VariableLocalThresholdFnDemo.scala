package org.mbari.esp.ia.imglib

import org.mbari.esp.ia.services.ToolBox
import org.mbari.esp.ia.awt.Drawing
import java.io.File

/**
 *
 * @author Brian Schlining
 * @since 2012-05-15
 */

object VariableLocalThresholdFnDemo {

    private[this] val name = getClass.getSimpleName

    def main(args: Array[String]) {
        val imageName = "/test08/pcr11jun2520h2000ml160s.tif"
        val imageUrl = getClass.getResource(imageName)

        var n = 0
        for (i <- 1 to 30 by 6; j <- 1 to 4) {
            n = n + 1
            val threshold = new VariableLocalThresholdFn(i, j * 0.5, 3)
            val floodFill = new FloodFillWithMinimumFn(5)
            val image = ToolBox.imageIOService.read(imageUrl).getProcessor
            val newImage = threshold(image)
            ToolBox.imageIOService.write(newImage, new File(name + n + "-threshold.png"))
            val spots = SpotRegionsFn(image, floodFill(newImage)).filter(_.eccentricity()._1 < 3).map(_.centroid())
            val bufferedImage = Drawing.draw(image, spots)
            ToolBox.imageIOService.write(bufferedImage, new File(name + n + ".png"))
        }
    }

}
