package org.mbari.esp.ia.services

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}

/**
 * Created by brian on 3/3/14.
 */
@RunWith(classOf[JUnitRunner])
class GeneArrayIOServiceBSpec extends FunSpec with Matchers {

  describe("A GeneArrayIOServiceB object") {

    val ioService = new GeneArrayIOServiceB
    val galUrl1 = getClass.getResource("/test12/bac_Array_Catalina_2014.gal")
    //val galUrl1 = getClass.getResource("/test14/HAB_Array_WHOIplus_16_jan_12_NF.gal")
    val badGalUrl = getClass.getResource("/test09/110220hab5wcomp_2012-02-22_163334_Run.txt")

    it ("should indicate if it can read a gal file") {
      val good =  ioService.canParse(galUrl1)
      good should be(true)

      val bad = ioService.canParse(badGalUrl)
      bad should be(false)
    }

    it ("should read the correct type of gal") {
      val galPoints = ioService.read(galUrl1)
      galPoints.size should be(224)
    }

    it ("should handle the wrong type of gal by returning an empty Seq") {
      val galPoints = ioService.read(badGalUrl)
      galPoints.size should be(0)
    }

  }



}
