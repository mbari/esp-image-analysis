package org.mbari.esp.ia.services

import org.slf4j.LoggerFactory
import org.mbari.esp.ia.imglib.DebugInspector
import org.junit.Test

/**
 *
 * @author Brian Schlining
 * @since 2012-03-19
 */

class ImageIOServiceBTest {

    val images = Iterable("/test06/hab11feb0920h40s.tif", "/brian-schlining.jpeg")
    val log = LoggerFactory.getLogger(getClass)

    @Test
    def testRead() {
        val ioService = new ImageIOServiceB
        images.foreach {
            imagePath =>
                val imageUrl = getClass.getResource(imagePath)
                val imgPlus = ioService.read(imageUrl)
                log.debug(DebugInspector.toString(imgPlus))
        }
    }

}
